#**********************************************************************
Project: Meta-analyzing the multiverse
File purpose: This document describes the content of the folder 'data'
Author: Anton Olsson Collentine (anton@olssoncollentine.com)

#**********************************************************************
Folder content
#**********************************************************************

00_codebooks [folder]
01_original-summary-data [folder]
02_raw-data [folder]
03_cleaned-raw-data [folder]
04_study_multiverses [folder]
05_study_multiverses_cleaned [folder]
06_meta-analytic-multiverse-samples [folder]
07_supplement-D-meta-analytic-samples [folder]
08.1_supplement-F-study-cleaned [folder]
08.2_supplement-F-meta-samples [folder]
08.3_supplement-F-intermediate-files [folder]
09.1_supplement-G-age_study_multiverses [folder]
09.2_supplement-G-age_study_multiverses_cleaned [folder]
09.3_supplement-G-age_meta [folder]
09.4_supplement-G-age_intermediate-files [folder]

meta_plot_data_n24.RDS [single file]
meta_plot_data_n2.RDS [single file]
single_MA_study_level.RDS [single file]
es_sd.RDS [single file]

#**********************************************************************
00_codebooks
#**********************************************************************
Contains codebooks for the folders 04 to 07. These were placed in a separate folder to not interfere with functions using the data.
For the folder 01_original-summary-data we refer to the original codebook, available in that folder.
For the folders 02-03 we refer to the file code/1.0_extract-raw-data.r


#**********************************************************************
01_original-summary-data
#**********************************************************************
This folder contains the pre-registered effect size estimates for all projects. They were downloaded from https://osf.io/4z3e7/ and were collated in Olsson-Collentine et al., 2020. The folder contains a codebook.

Olsson-Collentine, Anton, Jelte M. Wicherts, and Marcel A. L. M. van Assen. 2020. “Heterogeneity
in Direct Replications in Psychology and Its Association with Effect Size.” Psychological Bulletin, July.
https://doi.org/10.1037/bul0000294.


#**********************************************************************
02_raw-data
#**********************************************************************
Individual level data for RRR1 -RRR10 before any data-cleaning. More information can be found in the file code/1.0_extract-raw-data.r


#**********************************************************************
03_cleaned-raw-data
#**********************************************************************
Individual level data for RRR1 - RRR10 after data cleaning, but before applying any researcher DF. These files results from code/1.0_extract-raw-data.r
See code/1.0_extract-raw-data for a description of the content of each data-file.
This folder also contains a file for tracing back lab indicators to original labs 03_cleaned-raw-data/lab-keys.csv


#**********************************************************************
04_study_multiverses
#**********************************************************************
Effect sizes and specifications across the multiverse for each lab within RRR1 - RRR10, resulting from the file code/2.1_create-study-level-multiverses.r


#**********************************************************************
05_study_multiverses_cleaned
#**********************************************************************
Effect sizes and specifications across the multiverse for each lab within RRR1 - RRR10, after researcher DF combinations that resulted in 100% of participants excluded.
In addition, effect sizes have been standardized, and summary statistics such as p-values, effect size variance (SE^2), and total N have been computed.
These files result from code/3.0_clean-and-add-summary-statistics.r


#**********************************************************************
06_meta-analytic-multiverse-samples
#**********************************************************************
Files in this folder contain 100,000 meta-analyses drawn from the possible combinations of lab multiverses with at least 24 participants per experimental grouo. For each RRR there are 3 files for 3 different conditions.
1) Random draw from the multiverse (e.g., RRR1_MA_multiverse_samples_24.RDS)
2) bounded significance draws (e.g., RRR1_sig_bound_MA_multiverse_samples_24.RDS)
3) random significant draws (e.g., RRR1_sig_MA_multiverse_samples_24.RDS)

These files result from code/4.2_create-meta-analytic-multiverses.r


#**********************************************************************
07_supplement-D-meta-analytic-samples
#**********************************************************************
Files in this folder are identical to the files in 06_meta-analytic-multiverse-samples, except that labs were only required to contain at least 2 participants per experimental group (the minimum possible to compute many statistics). These files are used for a sensitivity analysis in supplement D.

#**********************************************************************
08.1_supplement-F-study-cleaned
08.2_supplement-F-meta-samples
08.3_supplement-F-intermediate-files
#**********************************************************************
The data in folders 08.1 and 08.2 corresponds to the data in folders 05
 and 06, but with the common researcher DF S2 option c (composite score
 using Principal Components Analysis) dropped. 08.1 is not strictly
 necesary as it only duplicates 05, but was created for convenience to
 not have to re-write functions. 08.2 are the meta-analytic multiverses
 when the S2.c option has been removed. Folder 08.3 just contains an
 intermediate file for saving time when computing final output in
 supplements/F_tutorial-drop-DF-S2c.Rmd

#**********************************************************************
09.1_supplement-G-age_study_multiverses
09.2_supplement-G-age_study_multiverses_cleaned
09.3_supplement-G-age_meta
09.4_supplement-G-age_intermediate-files
#**********************************************************************
The data in the folders 09.1, 09.2 and 09.3 correspond to the data in
the folders 04, 05 and 06, but with using an alternative age
specification for the E3 variable. The data in these folders use the
age options [a) No exclusion  b) Not 18-34 c) Not 18-30 d) Not 18-25
e) Not 18-24] instead of [a) No exclusion b) Not 18-24 c) Not 18-23
d) Not 18-22 e) Not 18-21]. The folder 09.4 contains intermediate data
files used to save time in the computation of results in
supplement/G_age-alternative-specification.Rmd.


#**********************************************************************
Single files
#**********************************************************************
These files are intermediate data file created to save time so that relatively time-consuming computations for e.g., creating plots do not need to be repeated constantly. These files are:
meta_plot_data_n24.RDS = intermediate data file created from 5.1_prep-meta-plot with minimum n = 24 that is used to save time for creating plots in 5.2_plots.r
meta_plot_data_n2.RDS = intermediate data file created from 5.1_prep-meta-plot with minimum n = 2 that is used to save time for creating plots in 5.2_plots.r for Supplement S3
single_MA_study_level.RDS = intermediate data file created from 5.1_prep-meta-plot used to create publication bias corrected plots in 5.2_plots.r
es_sd.RDS = intermediate data file created in 5.2_plots.r used to save time for creating DF standard deviation plots in 5.2_plots.r
