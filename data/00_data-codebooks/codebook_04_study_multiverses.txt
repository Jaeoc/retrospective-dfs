Codebook for data files in data/04_study_multiverses
Variable - explanation

For RRR01 and RRR02:
 RRR    - RRR indicator
 lab    - lab indicator            
 cell_A - number of participants in treatment group that were correct on dependent variable        
 cell_B - number of participants in treatment group that were incorrect on dependent variable        
 cell_C - number of participants in control group that were correct on dependent variable
 cell_D - number of participants in treatment group that were incorrect on dependent variable          
 excluded_percent  - what % of participants excluded from total sample size for a given multiverse (row)
 s1:s2, e1:e7, and u1:u5  - indicators of degrees of freedom, number {0,1, 2 , 3 et} indicates choice for each DF in that universe. The degrees of freedom in the dataset differ between RRRs, see DFs/DF-per-project.html   

For RRR03 - RRR10
 RRR    - RRR indicator
 lab    - lab indicator             
 treatment_mean - unstandardized mean on dependent variable for treatment group
 treatment_sd  - standard deviation in treatment group
 treatment_n - sample size in treatment group     
 control_mean - unstandardized mean on dependent variable for control group
 control_sd   - standard deviation in control group
 control_n     - sample size in control group
 excluded_percent  - what % of participants excluded from total sample size for a given multiverse (row)
 s1:s2, e1:e7, and u1:u9  - indicators of degrees of freedom, number {0,1, 2 , 3 et} indicates choice for each DF in that universe. The degrees of freedom in the dataset differ between RRRs, see DFs/DF-per-project.html  
 