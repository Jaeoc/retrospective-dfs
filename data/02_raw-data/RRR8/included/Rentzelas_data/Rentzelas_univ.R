#####TRANSLATION TERMS#####
#If your lab used a translated version of the experiment
#script, please update the terms in single quotes here to reflect the translated
#terms used in the experiment. Changing them here will ensure
#the analysis script runs correctly on your data.
labname <- 'Rentzelas'
non_student <- 'not a student'
male <- 'male'
female <- 'female'
yes <- 'yes'
no <- 'no'
###########################

#A script to analyze an idividual lab's replication results.
#This script binds all individual subject data into a single
#file and saves it out in raw form, and also saves out a results
#file that includes effects, confidence intervals, and summary statistics.

#Vers. 1.5
#Created by Katherine Wood 
#June 2016
#Updated February 2017

#First read in all data files and bind into a single data frame.
all_subjects <- as.data.frame(rbindlist(lapply(list.files(path=getwd(), 
                        pattern = '[[:digit:]]_[[:alpha:]]*.*.csv'), read.csv), use.names=TRUE, fill=TRUE))

all_subjects <- mutate(all_subjects, sex_r = case_when(
  sex_r == female & !is.na(sex_r) ~ 1,
  sex_r == male & !is.na(sex_r) ~ 0
),
link_yn = case_when(
  link_yn == yes & !is.na(link_yn) ~ 1,
  link_yn == no & !is.na(link_yn) ~ 0
),
thinking_yn = case_when(
  thinking_yn == yes & !is.na(thinking_yn) ~ 1,
  thinking_yn == no & !is.na(thinking_yn) ~ 0
),
subjID = 1:nrow(all_subjects)
)


#Process the exclusions we can detect automatically from the data
excluded_subjs <- all_subjects$subjID[all_subjects$age < 18 | all_subjects$age > 24 |
                                      as.character(all_subjects$year) == non_student |
                                      is.na(all_subjects$prime_response)]

#Determine number of trivia questions correct, number of trivia questions skipped,
#percent correct, and percent correct calculated relative only to questions answered.
dat <- all_subjects[!(all_subjects$subjID %in% excluded_subjs),]
dat$prime_code <- as.numeric(dat$prime_code)
dat$sex_r <- gsub("[\r\\r\n\\n]", "", as.character(dat$sex_r))
corr_cols <- paste(rep('triv', 30), seq(1:30), rep('_correct', 30), sep='')
raw_cols <- paste(rep('triv', 30), seq(1:30), rep('_raw', 30), sep='')
dat$correct_raw <- rowSums(dat[, corr_cols] == 'TRUE' | dat[, corr_cols] == 'True')
dat$skipped_raw <- rowSums(dat[,raw_cols] == 0)
dat$correct_pct <- (dat$correct_raw/30) * 100
dat$correct_skip_pct <- (dat$correct_raw/rowSums(dat[,raw_cols] != 0)) * 100
dat_gender <- dat[!is.na(dat$sex_r),]

skip_prof <- dat$skipped_raw[dat$prime_code == 1]
skip_hool <- dat$skipped_raw[dat$prime_code == 0]

dat$country <- 'United Kingdom'
dat$test_cond <- 0
dat$enough_males <- 0

#Save out this data frame as a .csv
write.csv(dat, paste(labname, '_data_complete.csv', sep=''), row.names = FALSE)
