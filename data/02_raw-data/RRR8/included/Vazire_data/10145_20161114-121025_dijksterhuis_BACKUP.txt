Prime condition:professor
Prime essay: I imagine myself working in a cubicle writing grants to send to the IRB so that more studies can be done. A lot of the work seems like it would constitute of managing different studies, talking with the project managers to see how the research is being conducted, having daily phone calls with the IRB about some consent form issue, and speaking with the graduate students about their various projects/theses/dissertations (depending on if they have decided to accept a graduate student for that academic year). As a university professor, I imagine that my personality would be easy-going and flexible, considering the field of research is not a field that has information set in stone. It would be probably require a lot of strenuous work and tension, which would probably take a toll on my easy-going personality. Thus, there would probably be moments where I'm not in a great mood or come off as stand-offish. Passions and hobbies would probably vary from person to person. For some reason, I imagine professors would not want to read all day since they ready scientific papers all the time so they would probably do something active like hiking or jogging since they're at a computer all day. On the plus side, I would probably be getting a lot of emails so my writing would probably not be too bad.
01. Gaborone is the capital of which African country?:Zaire
02. What is a character encoding scheme used by many computers called?:HTML
03. Which is the world's largest living rodent?:Capybara
04. Which planet is 92,897,000 miles from the sun?:Uranus
05. How many pairs of ribs are in a typical human body?:15
06. What country did Jawaharlal Nehru lead?:India
07. The ulna is a long bone in which part of the body?:Arm
08. Which of these people is particularly associated with November 5?:Guy Fawkes
09. What alloy is formed from copper and tin?:Brass
10. What breaks when its height is 3/4 of its depth?:Wave
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isotherms
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Ford
14. What do supplementary angles add up to in degrees?:90
15. What is the second planet in distance from the sun?:Venus
16. Which leader of the Soviet Union resigned on December 25, 1991?:Nikita Krushchev
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Radiation
18. Which computer was the first to beat a reigning chess champion?:Blue Knight
19. What is the name of the submerged fringe of a continent?:Continental Slope
20. What type of government ruled Italy during World War II?:Fascist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Salvador Dali
23. Which river is called 'Rio Bravo' by Mexicans?:Colorado River
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:France
What is your age?:20
What is your major?:Human Development

What is your native language?:English
What is your sex?:male
What is your year of study?:3rd year
Task 1 (the writing task),Task 2 (the trivia/knowledge task):Perception,Memory
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":I think the purpose of the task was to see how prior knowledge or information can elicit the brain to think about certain modes of information in a certain light, biasing the way that we think about certain things. 
1. Do you believe that there could be a link between thinking about a professor and the general knowledge questions?,2. What kind of link? If you have no idea, you may answer 'no idea.':yes,The way that the question asked us to think about a professor's life may have gotten us to think about the general knowledge questions in a more "sophisticated" way. By getting our brain more active in the first task, perhaps it primes the brain to be even more ready for new information that is asked in the 2nd task.
1. Do you believe that thinking about a professor affected your performance on the general knowledge questions?,2. How do you think that thinking about a professor affected your performance on the general knowledge questions? If you have no idea, you can answer 'no idea.':yes,I had to think creatively in the first task so I was more active and ready to take on the task of thinking about information that's obscure. 
Do you have any further thoughts or comments about the tasks so far?:Nope
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 44
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 12
Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 75
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? Assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 12
Before participating in this study, were you familiar with the term 'soccer hooligan?':no
