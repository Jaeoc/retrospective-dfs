Prime condition:professor
Prime essay: Maybe just spend 70% of my day doing research and studying and looking for materials. And spend the rest of my day relaxing and doing things I like other than my job. Grading papers, preparing for classes, having meeting with my collegues. I am passionate about my job and being nice on grading. I also would be really close to my students. As a univresity professor, I like to listen to music, to draw things and to hang out with friends! Also I like to cook and try new kinds of food.
01. Gaborone is the capital of which African country?:Zimbabwe
02. What is a character encoding scheme used by many computers called?:HTML
03. Which is the world's largest living rodent?:Muskrat
04. Which planet is 92,897,000 miles from the sun?:Earth
05. How many pairs of ribs are in a typical human body?:8
06. What country did Jawaharlal Nehru lead?:Phillipines
07. The ulna is a long bone in which part of the body?:Leg
08. Which of these people is particularly associated with November 5?:Guy Fawkes
09. What alloy is formed from copper and tin?:Brass
10. What breaks when its height is 3/4 of its depth?:Wave
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isotherms
12. What is the total number of spots on a die?:20
13. Which auto manufacturer established Saturn in 1985?:Ford
14. What do supplementary angles add up to in degrees?:360
15. What is the second planet in distance from the sun?:Venus
16. Which leader of the Soviet Union resigned on December 25, 1991?:0
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Temperature
18. Which computer was the first to beat a reigning chess champion?:Deep Blue
19. What is the name of the submerged fringe of a continent?:Continental Shelf
20. What type of government ruled Italy during World War II?:Fascist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Salvador Dali
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Rojo
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Saturn
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Wind speed
29. What is the main structural molecule in hair and nails?:Elastin
30. Joan of Arc is a national heroine of which country?:Greece
What is your age?:19
What is your major?:Undeclared-Social Science
What is your native language?:Mandarine
What is your sex?:female
What is your year of study?:2nd year
Task 1 (the writing task),Task 2 (the trivia/knowledge task):Geology,History
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":no idea
1. Do you believe that there could be a link between thinking about a professor and the general knowledge questions?,2. What kind of link? If you have no idea, you may answer 'no idea.':yes,A professor might know about these general knowledge?
1. Do you believe that thinking about a professor affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:It's a fun task
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 34
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 8
Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 65
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? Assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 16
Before participating in this study, were you familiar with the term 'soccer hooligan?':no
