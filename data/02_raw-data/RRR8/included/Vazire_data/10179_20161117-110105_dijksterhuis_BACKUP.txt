Prime condition:soccer hooligan
Prime essay: I spend much of my daily life at working. I cna be a bit crass to other people when it's uncalled for and I don't really make an effort to apologize for it since that's just the way I am. I spend alot of my evening watching soccer games, if not hanging out at the local bar with my soccer buddies. Over the weekends, my friends and I may go for a game of soccer at some nearby park. Outside of my soccer buddies, I don't spend much time with anyone else. I tend to be very dominating and also feel like my masculinity is threatened if anyone questions what I'm saying, and few do so. 



(sorry for spelling mistakes, i couldnt find a way to go back and change that)
01. Gaborone is the capital of which African country?:Uganda
02. What is a character encoding scheme used by many computers called?:HTML
03. Which is the world's largest living rodent?:Beaver
04. Which planet is 92,897,000 miles from the sun?:Mars
05. How many pairs of ribs are in a typical human body?:6
06. What country did Jawaharlal Nehru lead?:Pakistan
07. The ulna is a long bone in which part of the body?:Arm
08. Which of these people is particularly associated with November 5?:Guy Fawkes
09. What alloy is formed from copper and tin?:Bronze
10. What breaks when its height is 3/4 of its depth?:Iceberg
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isobars
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Chrysler
14. What do supplementary angles add up to in degrees?:180
15. What is the second planet in distance from the sun?:Venus
16. Which leader of the Soviet Union resigned on December 25, 1991?:Mikhail Gorbachev
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Pressure
18. Which computer was the first to beat a reigning chess champion?:Blue Knight
19. What is the name of the submerged fringe of a continent?:Continental Shelf
20. What type of government ruled Italy during World War II?:Fascist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Pablo Picasso
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Grande
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:France
What is your age?:20
What is your major?:20
What is your native language?:English
What is your sex?:female
What is your year of study?:4th year or higher
Task 1 (the writing task),Task 2 (the trivia/knowledge task):writing task,knowledge task
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":no idea

1. Do you believe that there could be a link between thinking about a soccer hooligan and the general knowledge questions?:no
1. Do you believe that thinking about a soccer hooligan affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:no
Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 85
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? Assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 23
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 50.0
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 15.0
Before participating in this study, were you familiar with the term 'soccer hooligan?':yes
