Prime condition:soccer hooligan
Prime essay: As a daily day in my life i'm watching Soccer most of the time, as well as keeping on top of the soccer news and the new information that is out. I like to drinkn beer and wear the jersey of my team, as well acheer for my team when there's a game. My hobbies are just staying home or if I go out watch the game on my phone or listen to the broadcastof it. Most of them time I spend it thinking about my soccer team which becomes my main focus. My passion is my team and soccer games, wether i get to see them live or not. But if I do get the chance, i get all excited because i'm seeing my team and someone who tries to say something about my team that is offensive I would go crazy on them. My team is everything and there's no way I would let someone else disrespect the team. 
01. Gaborone is the capital of which African country?:Zimbabwe
02. What is a character encoding scheme used by many computers called?:ASCII
03. Which is the world's largest living rodent?:Beaver
04. Which planet is 92,897,000 miles from the sun?:Uranus
05. How many pairs of ribs are in a typical human body?:12
06. What country did Jawaharlal Nehru lead?:India
07. The ulna is a long bone in which part of the body?:Neck
08. Which of these people is particularly associated with November 5?:Robin Hood
09. What alloy is formed from copper and tin?:Solder
10. What breaks when its height is 3/4 of its depth?:Wave
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isobars
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Ford
14. What do supplementary angles add up to in degrees?:180
15. What is the second planet in distance from the sun?:Mars
16. Which leader of the Soviet Union resigned on December 25, 1991?:Alexander Popov
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Pressure
18. Which computer was the first to beat a reigning chess champion?:Deep Blue
19. What is the name of the submerged fringe of a continent?:Continental Rise
20. What type of government ruled Italy during World War II?:Communist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:X Rays
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Salvador Dali
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Grande
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Ukraine
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Beethoven
28. What does a barometer measure?:Wind speed
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:Spain
What is your age?:19
What is your major?:Psychology
What is your native language?:Spanish
What is your sex?:female
What is your year of study?:2nd year
Task 1 (the writing task),Task 2 (the trivia/knowledge task):Social Psychology,Cognitive Tasks
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":The purpose was to see if there was a correlation between cognitive tasks and the social aspect of it
1. Do you believe that there could be a link between thinking about a soccer hooligan and the general knowledge questions?,2. What kind of link? If you have no idea, you may answer 'no idea.':yes,Association 
1. Do you believe that thinking about a soccer hooligan affected your performance on the general knowledge questions?,2. How do you think that thinking about a soccer hooligan affected your performance on the general knowledge questions? If you have no idea, you can answer 'no idea.':yes,If made me focus on an specific subject that when I was asked different questions I wasn't sure.
Do you have any further thoughts or comments about the tasks so far?:No

Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 80
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? Assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 25
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 24
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 7
Before participating in this study, were you familiar with the term 'soccer hooligan?':yes
