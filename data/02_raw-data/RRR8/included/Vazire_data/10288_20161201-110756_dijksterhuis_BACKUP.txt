Prime condition:professor
Prime essay: My name is Dr. Rami Rashmawi. I have a multiple degrees in Chemical Biology from the University of California, Davis and from Northwestern University. I recieved my Doctorate from Massachusetts Institute of Technology. I currently teach at Georgetown University as well as conduct research in my lab there. Due to my inquisitive and curious nature, I spend a majority of my free time reading journals in order to stay current. I also teach a Freshman Lecture series on the Chemistry of Love every Fall Semester. I also enjoy watching sports with my family and spending time with them.
01. Gaborone is the capital of which African country?:Botswana
02. What is a character encoding scheme used by many computers called?:ASCII
03. Which is the world's largest living rodent?:Capybara
04. Which planet is 92,897,000 miles from the sun?:Mars
05. How many pairs of ribs are in a typical human body?:8
06. What country did Jawaharlal Nehru lead?:Bangledesh
07. The ulna is a long bone in which part of the body?:Arm
08. Which of these people is particularly associated with November 5?:Guy Fawkes
09. What alloy is formed from copper and tin?:Bronze
10. What breaks when its height is 3/4 of its depth?:Wave
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isobars
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:General Motors
14. What do supplementary angles add up to in degrees?:180
15. What is the second planet in distance from the sun?:Venus
16. Which leader of the Soviet Union resigned on December 25, 1991?:Nikita Krushchev
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Radiation
18. Which computer was the first to beat a reigning chess champion?:Deep Blue
19. What is the name of the submerged fringe of a continent?:Continental Shelf
20. What type of government ruled Italy during World War II?:Fascist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Pablo Picasso
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Grande
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:France
What is your age?:20
What is your major?:NPB
What is your native language?:English
What is your sex?:male
What is your year of study?:3rd year
Task 1 (the writing task),Task 2 (the trivia/knowledge task):dont remember,dont remember
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":test knowledge
1. Do you believe that there could be a link between thinking about a professor and the general knowledge questions?:no
1. Do you believe that thinking about a professor affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:no
