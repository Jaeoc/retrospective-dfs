Prime condition:soccer-hooligan
Prime essay: As a soccer hooligan, I would spend most of my time either doing nothing, loitering around public places, or most importantly, playing soccer. I imagine that I would wake up around 1pm in the afternoon from my slumber, go out to play soccer for a couple of hours, and then bum around the area. When it becomes evening time, I would get some drinks from a nearby liquor store and publicly drink in the park. If I see anything that is to my disliking in the park, I will scream offensive things to passerbyers, as well as break car windows. My typical personality characteristics are a mix of anger and laziness. I would say that I would be filled with much contempt about the world, about my life, about my current state of being. However, I would be to lazy to change anything about my behavior or my actions. My true passion would be soccer - and I am great at it. Another hobby would be to loiter around pubilc areas such as parks or schools. I would enjoy loitering around because my being is to be a loitering and hooligan. As a soccer hooligan, these are the main things I love to do: soccer, loitering, and distirbuing the peace. 
01. Gaborone is the capital of which African country?:Zaire
02. What is a character encoding scheme used by many computers called?:HTML
03. Which is the world's largest living rodent?:Beaver
04. Which planet is 92,897,000 miles from the sun?:Mars
05. How many pairs of ribs are in a typical human body?:8
06. What country did Jawaharlal Nehru lead?:Bangladesh
07. The ulna is a long bone in which part of the body?:Arm
08. Which of these people is particularly associated with November 5?:Robin Hood
09. What alloy is formed from copper and tin?:Bronze
10. What breaks when its height is 3/4 of its depth?:0
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isobars
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Mitsubishi
14. What do supplementary angles add up to in degrees?:180
15. What is the second planet in distance from the sun?:Mars
16. Which leader of the Soviet Union resigned on December 25, 1991?:Boris Yeltsin
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Pressure
18. Which computer was the first to beat a reigning chess champion?:Blue Knight
19. What is the name of the submerged fringe of a continent?:Continental Abyss
20. What type of government ruled Italy during World War II?:Republic
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Salvador Dali
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Grande
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Ukraine
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:France
What is your age?:21
What is your major?:Business Administration
What is your native language?:English
What is your sex?:female
What is your year of study?:4th year or higher
Task 1 (the writing task),Task 2 (the trivia/knowledge task):small room	,small room
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":seeing the corelation between how emboding the "soccer hooligan" effects trivial knowledge
Do you believe that there could be a link between thinking about a soccer-hooligan and the general knowledge questions?,What kind of link? If you have no idea, you may answer 'no idea.':yes,soccer hooligan decreases accuracy of general knowledge questions
Do you believe that thinking about a soccer-hooligan affected your performance on the general knowledge questions?,How do you think that thinking about a soccer-hooligan affected your performance on the general knowledge questions? If you have no idea, you can answer 'no idea.':yes,made me lazy. I didn't really care to try to get the right answer to the questions.
Do you have any further thoughts or comments about the tasks so far?:n/a
