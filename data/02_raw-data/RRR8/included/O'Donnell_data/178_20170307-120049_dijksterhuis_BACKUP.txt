Prime condition:soccer-hooligan
Prime essay: In my daily life, I am a young, white, male college student. I usually have a full day of class scheduled but I may or may not go to them. In the evenings, I hang out with my friends, generally other young, white males. We go out to bars or hang out and drink at one of our houses. I have a strong personality. I can be loud, especially when I'm with my friends and when we're watching sports. I am really extraverted, sometimes agreeable (people like me unless I've been drinking), very open to new experiences, not super conscientiousness (I tend to do some rash/impulsive things), and can be neurotic. I am so passionate about sports and very loyal to my friends. I get all the updates about games, stay up late to watch games being played in other countries. I love to be around my friends.
01. Gaborone is the capital of which African country?:Botswana
02. What is a character encoding scheme used by many computers called?:HTML
03. Which is the world's largest living rodent?:Capybara
04. Which planet is 92,897,000 miles from the sun?:Earth
05. How many pairs of ribs are in a typical human body?:12
06. What country did Jawaharlal Nehru lead?:India
07. The ulna is a long bone in which part of the body?:Arm
08. Which of these people is particularly associated with November 5?:Guy Fawkes
09. What alloy is formed from copper and tin?:Bronze
10. What breaks when its height is 3/4 of its depth?:Wave
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isotherms
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Mitsubishi
14. What do supplementary angles add up to in degrees?:360
15. What is the second planet in distance from the sun?:Venus
16. Which leader of the Soviet Union resigned on December 25, 1991?:Mikhail Gorbachev
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Pressure
18. Which computer was the first to beat a reigning chess champion?:Blue Knight
19. What is the name of the submerged fringe of a continent?:Continental Shelf
20. What type of government ruled Italy during World War II?:Fascist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Pablo Picasso
23. Which river is called 'Rio Bravo' by Mexicans?:Colorado River
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Handel
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:France
What is your age?:22
What is your major?:Business
What is your native language?:English
What is your sex?:female
What is your year of study?:4th year or higher
Task 1 (the writing task),Task 2 (the trivia/knowledge task):In a room	,In a room
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":no idea
Do you believe that there could be a link between thinking about a soccer-hooligan and the general knowledge questions?:no
Do you believe that thinking about a soccer-hooligan affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:no
