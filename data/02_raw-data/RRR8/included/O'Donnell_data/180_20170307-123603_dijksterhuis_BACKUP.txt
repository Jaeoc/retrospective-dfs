Prime condition:professor
Prime essay: AS A PROFESSOR, I USUALLY GET COFFEE IN THE MORNINGS BEFORE I TEACH ANY LECTURES OR HOLD ANY OFFICE HOURS TO ENSURE THAT I CAN BE AS ALERT AS POSSIBLE TO CONVEY THE INFORMATION THE BEST WAY POSSIBLE. FOLLOWING MY LECTURES OR OFFICE HOURS, I DO MY BEST TO WORK ON MY DAILY ALLOTTED SECTION OF RESEARCH AND MAKE SURE I AM KEEPING UP WITH MY TIMELINE. IN MY SPARE TIME, I TRY TO STAY AS ACTIVE AS POSSIBLE BY TAKING WALKS AROUND THE CAMPUS OR TAKING EXERCISE CLASSES IN STUDIOS NEARBY. I love to teach students and watch them grow with the subject and learn to love the subject, and in that light, I like to approach my classes with lots of energy and humor in order to capture students' attention and keep them engaged with the material. In order to ensure the most learning in the classroom, I enforce a no-technology policy, a policy I found particularly helpful during my time in college. 
01. Gaborone is the capital of which African country?:Botswana
02. What is a character encoding scheme used by many computers called?:ASCII
03. Which is the world's largest living rodent?:Capybara
04. Which planet is 92,897,000 miles from the sun?:Mars
05. How many pairs of ribs are in a typical human body?:6
06. What country did Jawaharlal Nehru lead?:Pakistan
07. The ulna is a long bone in which part of the body?:Foot
08. Which of these people is particularly associated with November 5?:Guy Fawkes
09. What alloy is formed from copper and tin?:Brass
10. What breaks when its height is 3/4 of its depth?:Iceberg
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isotherms
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Chrysler
14. What do supplementary angles add up to in degrees?:180
15. What is the second planet in distance from the sun?:Venus
16. Which leader of the Soviet Union resigned on December 25, 1991?:Mikhail Gorbachev
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Pressure
18. Which computer was the first to beat a reigning chess champion?:Deep Blue
19. What is the name of the submerged fringe of a continent?:Continental Shelf
20. What type of government ruled Italy during World War II?:Fascist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Penicillin
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Pablo Picasso
23. Which river is called 'Rio Bravo' by Mexicans?:Colorado River
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Jupiter
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:France
What is your age?:21
What is your major?:Business Administration, Economics
What is your native language?:English
What is your sex?:female
What is your year of study?:4th year or higher
Task 1 (the writing task),Task 2 (the trivia/knowledge task):Sociology,Psychology
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":Testing memory
Do you believe that there could be a link between thinking about a professor and the general knowledge questions?,What kind of link? If you have no idea, you may answer 'no idea.':yes,By placing yourself in a level of authority or more knowledge, you could subconciously feel smarter/more confident about your answers, which may lead to a person getting more questions correct than if they had pretended they were a student.
Do you believe that thinking about a professor affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:no
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 30
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 10
Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 86
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 25
Before participating in this study, were you familiar with the term 'soccer hooligan?':yes
