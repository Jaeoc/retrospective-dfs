Prime condition:soccer-hooligan
Prime essay: As a soccer hooligan, I would...
1. Watch sports on the weekends with my other soccer hooligan friends.
2. Probably obnoxiously cat-call women on the street.
3. Go to my local soccer team's games with my other soccer hooligan friends.
4. Go to my child's soccer games and teach them how to be the best soccer player ever.
5. Be annoying.
6. Go to the bar with my other soccer hooligan friends, drink, get into fights with other soccer fans, and most of all, watch soccer.
7. Tweet about soccer events and scores.

My personality characteristics or attributes:
1. I would be annoying.
2. I would be obnoxious.
3. I would be loud.
4. I would be rude.
5. I would be stubborn.
6. I would be offensive.
01. Gaborone is the capital of which African country?:Zimbabwe
02. What is a character encoding scheme used by many computers called?:Morse Code
03. Which is the world's largest living rodent?:Capybara
04. Which planet is 92,897,000 miles from the sun?:Uranus
05. How many pairs of ribs are in a typical human body?:12
06. What country did Jawaharlal Nehru lead?:Bangladesh
07. The ulna is a long bone in which part of the body?:Neck
08. Which of these people is particularly associated with November 5?:Dick Turpin
09. What alloy is formed from copper and tin?:Brass
10. What breaks when its height is 3/4 of its depth?:Mountain
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isotherms
12. What is the total number of spots on a die?:21
13. Which auto manufacturer established Saturn in 1985?:Chrysler
14. What do supplementary angles add up to in degrees?:120
15. What is the second planet in distance from the sun?:Mercury
16. Which leader of the Soviet Union resigned on December 25, 1991?:Boris Yeltsin
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Temperature
18. Which computer was the first to beat a reigning chess champion?:Deep Blue
19. What is the name of the submerged fringe of a continent?:Continental Abyss
20. What type of government ruled Italy during World War II?:Communist
21. Alexander Fleming won the Nobel Prize for the discovery of what?:Stethoscope
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Salvador Dali
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Grande
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Kazakhstan
26. Which planet is the largest in the solar system?:Mars
27. Who composed 'The Nutcracker?':Tchaikovsky
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Collagen
30. Joan of Arc is a national heroine of which country?:Germany
What is your age?:20
What is your major?:Business Administration
What is your native language?:English
What is your sex?:male
What is your year of study?:3rd year
Task 1 (the writing task),Task 2 (the trivia/knowledge task):This was the task I was working for.,This was the task I wa working for.
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":No idea.
Do you believe that there could be a link between thinking about a soccer-hooligan and the general knowledge questions?,What kind of link? If you have no idea, you may answer 'no idea.':yes,Soccer hooligans cannot answer those general knowledge questions.
Do you believe that thinking about a soccer-hooligan affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:No
Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 87
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 22
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 35
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 14
Before participating in this study, were you familiar with the term 'soccer hooligan?':no
