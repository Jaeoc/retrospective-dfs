Prime condition:soccer-hooligan
Prime essay: As a soccer hooligan, my daily life consists of blowing off my responsibilities, not really caring to prepare for the future, and always looking for something fun to do.
Whenever I get the chance, and I Always seem to get the chance, I will be out of the house with my bros. We go to malls, baseball games, anywhere that we can get attention from the finest girls in town.
As a soccer hooligan, I am impatient, lazy, irresponsible, and very impulsive. I don't care to listen to what other people have to say before I make my decisions.
My passions include hitting the next max weight at the gym, and my hobbies are finding hot girls with my friends.
I don't give off a good impression to people because I just don't care enough to. Any time is free time to me, and I am always trying to have a good time, even if it is at the expense of others.
01. Gaborone is the capital of which African country?:Botswana
02. What is a character encoding scheme used by many computers called?:HTML
03. Which is the world's largest living rodent?:Beaver
04. Which planet is 92,897,000 miles from the sun?:Earth
05. How many pairs of ribs are in a typical human body?:12
06. What country did Jawaharlal Nehru lead?:Pakistan
07. The ulna is a long bone in which part of the body?:Leg
08. Which of these people is particularly associated with November 5?:Sweeney Todd
09. What alloy is formed from copper and tin?:Bronze
10. What breaks when its height is 3/4 of its depth?:Wave
11. In meteorology, what name is given to lines of equal atmospheric pressure?:Isotherms
12. What is the total number of spots on a die?:20
13. Which auto manufacturer established Saturn in 1985?:Mitsubishi
14. What do supplementary angles add up to in degrees?:180
15. What is the second planet in distance from the sun?:Mars
16. Which leader of the Soviet Union resigned on December 25, 1991?:Boris Yeltsin
17. The German physicist Hans Geiger is best remembered as the co-inventor of a device measuring what?:Radiation
18. Which computer was the first to beat a reigning chess champion?:Blue Knight
19. What is the name of the submerged fringe of a continent?:Continental Abyss
20. What type of government ruled Italy during World War II?:Democracy
21. Alexander Fleming won the Nobel Prize for the discovery of what?:X Rays
22. Which artist painted Guernica, depicting scenes from the Spanish Civil War?:Henri Matisse
23. Which river is called 'Rio Bravo' by Mexicans?:Rio Grande
24. Where is the uvula located?:In the throat
25. What region makes up 75% of Russia?:Siberia
26. Which planet is the largest in the solar system?:Saturn
27. Who composed 'The Nutcracker?':Handel
28. What does a barometer measure?:Air pressure
29. What is the main structural molecule in hair and nails?:Keratin
30. Joan of Arc is a national heroine of which country?:Greece
What is your age?:21
What is your major?:EEP
What is your native language?:English
What is your sex?:male
What is your year of study?:3rd year
Task 1 (the writing task),Task 2 (the trivia/knowledge task):Social Psychology,Cognitive Psychology
In your opinion, what was the purpose of these tasks? If you have no idea, you may answer by typing "no idea.":To prepare for a future study
Do you believe that there could be a link between thinking about a soccer-hooligan and the general knowledge questions?:no
Do you believe that thinking about a soccer-hooligan affected your performance on the general knowledge questions?:no
Do you have any further thoughts or comments about the tasks so far?:No
Imagine a typical university professor. Professors, as a group, tend to have completed a doctorate degree, work in colleges or universities, dedicate their time to teaching and research, and try to publish their research in academic journals.

Use the scale below to rate how intelligent you think a typical university professor is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 60
Using the scale below, how well do you think a typical university professor would perform on the general-knowledge trivia test you just completed? For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical university professor would get correct.: 10
Imagine a typical soccer hooligan. Hooligans, as a group, tend to be young men who are fanatical sports fans, generally drink a lot in public, say offensive things to passersby, and sometimes provoke fights or destroy property.

Use the scale below to rate how intelligent you think a typical soccer hooligan is relative to the average adult. On this scale, 50 would be the average adult, 0 would be the least intelligent, and 100 would be the most intelligent.: 37
Using the scale below, how well do you think a typical soccer hooligan would perform on the general-knowledge trivia test you just completed. For the scale, assume that the average adult would get 15 items correct (50%). Please indicate how many of the 30 questions a typical soccer hooligan would get correct.: 7
Before participating in this study, were you familiar with the term 'soccer hooligan?':no
