#####TRANSLATION TERMS#####
#If your lab used a translated version of the experiment
#script, please update the terms in single quotes here to reflect the translated
#terms used in the experiment. Changing them here will ensure
#the analysis script runs correctly on your data.
labname <- 'Aczel'
non_student <- 'nem hallgató'
professor <- "professzor"
hooligan <- "futballhuligán"
female <- 'nő'
male <- 'férfi'
###########################

#First read in all data files and bind into a single data frame.
all_subjects <- as.data.frame(rbindlist(lapply(list.files(path=getwd(), 
                        pattern = '[[:digit:]]_[[:alpha:]]*.*.csv'), 
                        read.csv, stringsAsFactors=FALSE, fileEncoding = "latin1")))

all_subjects <- rename(all_subjects, 'sex_r'=language, 'language'=sex_r, 'year'=major, 'major'=year) %>%
                select(-File, -X.LocalizedFileNames.)

female <- 1
male <-  0

yes <- 'igen'
no <- 'nem'

all_subjects <- mutate(all_subjects, sex_r = case_when(
  startsWith(as.character(sex_r), 'n') & !is.na(sex_r) ~ 1,
  startsWith(as.character(sex_r), 'f') & !is.na(sex_r) ~ 0
),
prior_hool = case_when(
  prior_hool == yes & !is.na(prior_hool) ~ 1,
  prior_hool == no & !is.na(prior_hool) ~ 0
),
link_yn = case_when(
  link_yn == yes & !is.na(link_yn) ~ 1,
  link_yn == no & !is.na(link_yn) ~ 0
),
thinking_yn = case_when(
  thinking_yn == yes & !is.na(thinking_yn) ~ 1,
  thinking_yn == no & !is.na(thinking_yn) ~ 0
)
)


#Process the exclusions we can detect automatically from the data
excluded_subjs <- all_subjects$subjID[all_subjects$age < 18 | all_subjects$age > 24 |
                                      as.character(all_subjects$year) == non_student |
                                      is.na(all_subjects$prime_response)]

#Determine number of trivia questions correct, number of trivia questions skipped,
#percent correct, and percent correct calculated relative only to questions answered.
dat <- all_subjects[!(all_subjects$subjID %in% excluded_subjs),]
dat$prime_code <- as.numeric(dat$prime_code)
corr_cols <- paste(rep('triv', 30), seq(1:30), rep('_correct', 30), sep='')
raw_cols <- paste(rep('triv', 30), seq(1:30), rep('_raw', 30), sep='')
dat$correct_raw <- rowSums(dat[, corr_cols] == 'TRUE' | dat[, corr_cols] == 'True')
dat$skipped_raw <- rowSums(dat[,raw_cols] == 0)
dat$correct_pct <- (dat$correct_raw/30) * 100
dat$correct_skip_pct <- (dat$correct_raw/rowSums(dat[,raw_cols] != 0)) * 100
dat_gender <- dat[!is.na(dat$sex_r),]

skip_prof <- dat$skipped_raw[dat$prime_code == 1]
skip_hool <- dat$skipped_raw[dat$prime_code == 0]

dat$country <- 'Hungary'
dat$test_cond <- 1
dat$enough_males <- 1

#Save out this data frame as a .csv
write.csv(dat, paste(labname, '_data_complete.csv', sep=''), row.names = FALSE)
