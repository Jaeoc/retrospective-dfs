Prime condition:profesor universitario
Prime essay: Como profesor me dedico a mantenerme actualizado en el campo de mi profesion, tambien comparto tiempo con mi nucleo familiar y para salir un poco de la rutina suelo viajar.
las actividades que realizo como profesor se basan en preparar profesionales en el ambito de la salud, pues como medico es vital que los estudiantes se preparen de una buena manera, otra de las actividades que se ve inmersa en la profesion es evaluar la calidad de estudiantes que dia a dia se comprometen a la carrera
01. Gaborone es la capital de qué país africano:Botsuana
02. ¿Cuál es el nombre de un esquema de codificación de caracteres usado por muchos computadores?:HTML
03. ¿Cuál es el roedor viviente más grande del mundo?:Rata almizclera
04. ¿Qué planeta está a 149.600.000 kilómetros del sol?:Urano
05. ¿Cuántos pares de costillas tiene un cuerpo humano normal?:12
06. ¿Qué país lideró Jawaharlal Nehru?:Filipinas
07. ¿El cúbito es un hueso largo en qué parte del cuerpo?:Brazo
08. ¿Cuál de estas personas está particularmente asociada con el 5 de noviembre?:Guy Fawkes
09. ¿Qué aleación se forma del cobre y el estaño?:Latón
10. ¿Qué se rompe cuando su altura es 3/4 de su profundidad?:Iceberg
11. En meteorología, ¿Qué nombre se le da a líneas de presión atmosférica igual?:Isotermas
12. ¿Cuál es el número total de puntos en un dado?:25
13. ¿Qué fábrica de automóviles creó en 1985 a la compañía 'Saturn'?:Mitsubishi
14. ¿Cuánto suman los ángulos suplementarios en grados?:180
15. ¿Cuál es el segundo planeta más cercano al sol?:Marte
16. ¿Qué lider de la Unión Soviética renunció el 25 de diciembre de 1991?:Nikita Krushchev
17. ¿Al físico Alemán Hans Geiger se le recuerda como el co-inventor de un aparato que medía qué?::Presión
18. ¿Cuál computador fue el primero en vencer a un campeón vigente de ajedrez?:King Blue
19. ¿Cuál es el nombre de la franja sumergida de un continente?:Abismo continental
20. ¿Qué tipo de gobierno había en Italia durante la segunda guerra mundial?:Fascista
21. ¿Qué descubrimiento llevó a Alexander Fleming a ganar el Premio Nobel?:Rayos X
22. ¿Qué artista pintó el Guernica, retratando escenas de la Guerra Civil Española?:Henri Matisse
23. ¿A qué río los mexicanos le llaman 'Rio Bravo'?:Río Grande
24. ¿Dónde se ubica la úvula?:En la garganta
25. ¿Qué región abarca el 75% de Rusia?:Siberia
26. ¿Cuál es el planeta más grande en el sistema solar?:Saturno
27. ¿Quién compuso 'El Cascanueces'?:Tchaikovsky
28. ¿Qué mide un barómetro?:Humedad relativa
29. ¿Cuál es la principal estructura molecular en el cabello y en las uñas?:Queratina
30. ¿Juana de Arco es una heroína nacional de qué país?:Francia
¿Cuál es tu lengua materna?:Español
¿Cuál es tu sexo?:femenino
¿Qué año de la carrera estás cursando?:primer año
¿Qué carrera estás cursando?:terapia ocupacional
¿Qué edad tienes?:17
Tarea 1 (la tarea en la que tenías que hacer una redacción),Tarea 2 (la tarea de preguntas de conocimiento general):descriptiva,cognitiva
En tu opinión, ¿Cuál fue el propósito de estas tareas? Si no sabes,puedes contestar escribiendo “no sé":no se
1. ¿Crees que pueda haber una conexión entre pensar en un profesor universitario y las preguntas de conocimiento general?:no
1. Crees que pensar en un profesor universitario afectó tu rendimiento en las preguntas de conocimiento general?:no
¿Tienes alguna otra opinión o comentario sobre las tareas hasta ahora?:no
Imagina un típico barrista de futbol. Los barristas de fútbol, como grupo, tienden a ser hombres jóvenes fanáticos de los deportes, generalmente beben mucho en público, les dicen cosas ofensivas a las personas que pasan, y algunas veces provocan peleas o destruyen propiedades.

Usa la escala debajo para evaluar qué tan inteligente crees que sea un típico barrista de fútbol en comparación con el adulto promedio. En esta escala, 50 sería el adulto promedio, 0 sería el menos inteligente y 100 sería el más inteligente.: 76
Usando la escala debajo, ¿qué tan bien crees que se desempeñaría un típico barrista de futbol en la tarea de conocimiento general que acabas de completar? Asume que el adulto promedio contestaría bien 15 preguntas (50%). Por favor indica cuántas de las 30 preguntas un típico barrista de fútbol contestaría bien.: 26
Imagina un típico profesor universitario. Los profesores, como grupo, tienden a haber completado un programa de doctorado, trabajar en universidades, dedicar su tiempo a la enseñanza y a la investigación, y a tratar de publicar sus investigaciones en revistas científicas. 

Usa la escala debajo para evaluar qué tan inteligente crees que sea un típico profesor universitario en comparación con el adulto promedio. En esta escala, 50 sería el adulto promedio, 0 sería el menos inteligente y 100 sería el más inteligente.: 89
Usando la escala debajo, ¿qué tan bien crees que se desempeñaría un típico profesor universitario en la tarea de conocimiento general que acabas de completar? Asume que el adulto promedio contestaría bien 15 (50%). Por favor indica cuántas de las 30 preguntas un típico profesor universitario contestaría bien: 30
“Antes de participar en este estudio, ¿estabas familiarizado con el término ‘barrista de fútbol’?:no
