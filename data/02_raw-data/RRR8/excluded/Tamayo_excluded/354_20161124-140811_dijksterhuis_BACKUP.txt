Prime condition:profesor universitario
Prime essay: Tengo una familia, todos los dias me levanto temprano para ir a dictar mi clase. Entre mis actividades como profesor se encuentran: prepar las clases, calificar trabajos y evaluaciones, realizar investigacion sobre temas realacionados con la materias que dicto, dirigir tesis de grado y escribir articulos cientificos. En mi tiempo libre me gusta ver peliculas con mi familia, viajar, conocer nuevos lugares y compartir con mi familia y amigos. Tambien me gusta participar en congresos y hablar con otros profesores. Me caracterizo por ser una persona divertida, practica, responsable y soñadora.
01. Gaborone es la capital de qué país africano:0
02. ¿Cuál es el nombre de un esquema de codificación de caracteres usado por muchos computadores?:ASCII
03. ¿Cuál es el roedor viviente más grande del mundo?:Castor
04. ¿Qué planeta está a 149.600.000 kilómetros del sol?:0
05. ¿Cuántos pares de costillas tiene un cuerpo humano normal?:6
06. ¿Qué país lideró Jawaharlal Nehru?:0
07. ¿El cúbito es un hueso largo en qué parte del cuerpo?:Brazo
08. ¿Cuál de estas personas está particularmente asociada con el 5 de noviembre?:0
09. ¿Qué aleación se forma del cobre y el estaño?:Bronce
10. ¿Qué se rompe cuando su altura es 3/4 de su profundidad?:Iceberg
11. En meteorología, ¿Qué nombre se le da a líneas de presión atmosférica igual?:Isotermas
12. ¿Cuál es el número total de puntos en un dado?:21
13. ¿Qué fábrica de automóviles creó en 1985 a la compañía 'Saturn'?:0
14. ¿Cuánto suman los ángulos suplementarios en grados?:360
15. ¿Cuál es el segundo planeta más cercano al sol?:Marte
16. ¿Qué lider de la Unión Soviética renunció el 25 de diciembre de 1991?:0
17. ¿Al físico Alemán Hans Geiger se le recuerda como el co-inventor de un aparato que medía qué?::0
18. ¿Cuál computador fue el primero en vencer a un campeón vigente de ajedrez?:0
19. ¿Cuál es el nombre de la franja sumergida de un continente?:Plataforma continental
20. ¿Qué tipo de gobierno había en Italia durante la segunda guerra mundial?:0
21. ¿Qué descubrimiento llevó a Alexander Fleming a ganar el Premio Nobel?:Penicilina
22. ¿Qué artista pintó el Guernica, retratando escenas de la Guerra Civil Española?:0
23. ¿A qué río los mexicanos le llaman 'Rio Bravo'?:0
24. ¿Dónde se ubica la úvula?:0
25. ¿Qué región abarca el 75% de Rusia?:0
26. ¿Cuál es el planeta más grande en el sistema solar?:Júpiter
27. ¿Quién compuso 'El Cascanueces'?:0
28. ¿Qué mide un barómetro?:Presión atmosférica
29. ¿Cuál es la principal estructura molecular en el cabello y en las uñas?:Queratina
30. ¿Juana de Arco es una heroína nacional de qué país?:Francia
¿Cuál es tu lengua materna?:Español
¿Cuál es tu sexo?:femenino
¿Qué año de la carrera estás cursando?:cuarto año o superior
¿Qué carrera estás cursando?:psicologia
¿Qué edad tienes?:0
Tarea 1 (la tarea en la que tenías que hacer una redacción),Tarea 2 (la tarea de preguntas de conocimiento general):Describirme pensando que soy una profesora universitaria,Preguntas sobre ciencia
En tu opinión, ¿Cuál fue el propósito de estas tareas? Si no sabes,puedes contestar escribiendo “no sé":No se
1. ¿Crees que pueda haber una conexión entre pensar en un profesor universitario y las preguntas de conocimiento general?:no
1. Crees que pensar en un profesor universitario afectó tu rendimiento en las preguntas de conocimiento general?:no
¿Tienes alguna otra opinión o comentario sobre las tareas hasta ahora?:no
Imagina un típico barrista de futbol. Los barristas de fútbol, como grupo, tienden a ser hombres jóvenes fanáticos de los deportes, generalmente beben mucho en público, les dicen cosas ofensivas a las personas que pasan, y algunas veces provocan peleas o destruyen propiedades.

Usa la escala debajo para evaluar qué tan inteligente crees que sea un típico barrista de fútbol en comparación con el adulto promedio. En esta escala, 50 sería el adulto promedio, 0 sería el menos inteligente y 100 sería el más inteligente.: 40
Usando la escala debajo, ¿qué tan bien crees que se desempeñaría un típico barrista de futbol en la tarea de conocimiento general que acabas de completar? Asume que el adulto promedio contestaría bien 15 preguntas (50%). Por favor indica cuántas de las 30 preguntas un típico barrista de fútbol contestaría bien.: 12
Imagina un típico profesor universitario. Los profesores, como grupo, tienden a haber completado un programa de doctorado, trabajar en universidades, dedicar su tiempo a la enseñanza y a la investigación, y a tratar de publicar sus investigaciones en revistas científicas. 

Usa la escala debajo para evaluar qué tan inteligente crees que sea un típico profesor universitario en comparación con el adulto promedio. En esta escala, 50 sería el adulto promedio, 0 sería el menos inteligente y 100 sería el más inteligente.: 62
Usando la escala debajo, ¿qué tan bien crees que se desempeñaría un típico profesor universitario en la tarea de conocimiento general que acabas de completar? Asume que el adulto promedio contestaría bien 15 (50%). Por favor indica cuántas de las 30 preguntas un típico profesor universitario contestaría bien: 19
Antes de participar en este estudio, ¿estabas familiarizado con el término ‘barrista de fútbol’?:sí
