
Random-Effects Model (k = 10; tau^2 estimator: REML)

tau^2 (estimated amount of total heterogeneity): 0 (SE = 0.0803055353951420)
tau (square root of estimated tau^2 value):      0
I^2 (total heterogeneity / total variability):   0.00%
H^2 (total variability / sampling variability):  1.00

Test for Heterogeneity: 
Q(df = 9) = 7.3860284243188463, p-val = 0.5969955077999293

Model Results:

          estimate                  se                zval                pval                ci.lb               ci.ub   
0.1002108310012187  0.1341959558136400  0.7467500074322881  0.4552144776359890  -0.1628084092644442  0.3632300712668814   

---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1 

