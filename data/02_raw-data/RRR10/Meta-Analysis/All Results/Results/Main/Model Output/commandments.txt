
Random-Effects Model (k = 19; tau^2 estimator: REML)

tau^2 (estimated amount of total heterogeneity): 0.0236709408322818 (SE = 0.0685342041286013)
tau (square root of estimated tau^2 value):      0.1538536344461247
I^2 (total heterogeneity / total variability):   10.96%
H^2 (total variability / sampling variability):  1.12

Test for Heterogeneity: 
Q(df = 18) = 20.3570353960487509, p-val = 0.3130816022657427

Model Results:

          estimate                  se                zval                pval               ci.lb               ci.ub    
0.2946991179918532  0.1070863739814735  2.7519758773682796  0.0059236884188899  0.0848136817531781  0.5045845542305283  **

---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1 

