
Random-Effects Model (k = 19; tau^2 estimator: REML)

tau^2 (estimated amount of total heterogeneity): 0.0079484216179043 (SE = 0.0524543256546874)
tau (square root of estimated tau^2 value):      0.0891539209339909
I^2 (total heterogeneity / total variability):   4.81%
H^2 (total variability / sampling variability):  1.05

Test for Heterogeneity: 
Q(df = 18) = 20.2467065245776396, p-val = 0.3191029003548202

Model Results:

          estimate                  se                zval                pval               ci.lb               ci.ub     
0.6720927272536129  0.0931867143288451  7.2123234743729219  0.0000000000005501  0.4894501233314540  0.8547353311757718  ***

---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1 

