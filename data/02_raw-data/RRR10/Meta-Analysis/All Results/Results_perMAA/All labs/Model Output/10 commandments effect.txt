
Random-Effects Model (k = 25; tau^2 estimator: REML)

tau^2 (estimated amount of total heterogeneity): 0 (SE = 0.0553713758487905)
tau (square root of estimated tau^2 value):      0
I^2 (total heterogeneity / total variability):   0.00%
H^2 (total variability / sampling variability):  1.00

Test for Heterogeneity: 
Q(df = 24) = 17.4654441069962907, p-val = 0.8281439829628958

Model Results:

          estimate                  se                zval                pval                ci.lb               ci.ub   
0.1740956595283492  0.0911861919189844  1.9092327014053498  0.0562320795210885  -0.0046259925202174  0.3528173115769158  .

---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1 

