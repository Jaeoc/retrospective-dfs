
Random-Effects Model (k = 25; tau^2 estimator: REML)

tau^2 (estimated amount of total heterogeneity): 0.1069699964236691 (SE = 0.0804106666714627)
tau (square root of estimated tau^2 value):      0.3270626796558559
I^2 (total heterogeneity / total variability):   38.68%
H^2 (total variability / sampling variability):  1.63

Test for Heterogeneity: 
Q(df = 24) = 38.5833209878659105, p-val = 0.0301867978790857

Model Results:

          estimate                  se                zval                pval               ci.lb               ci.ub     
0.5621003224636185  0.1068658438207986  5.2598688445879347  0.0000001441581874  0.3526471173973710  0.7715535275298659  ***

---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1 

