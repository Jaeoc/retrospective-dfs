- cheating: (mean matrices solved in the cheating group for the book prime) - (mean correct matrices solved in the no-cheating for the book prime)

- commandments condition: (mean matrices solved in the cheating group for commandments prime) - (mean correct matrices solved in the no-cheating  for commandments prime)

- 10 commandments effect: (mean matrices solved in the cheating group primed with commandments) - (mean matrices solved in the cheating group primed with books)

- control condition: (mean correct matrices solved in the no cheating group primed with commandments) - (mean correct matrices solved in the no-cheating group primed with books)

- difference: (difference in cheating vs. no cheating for the book-primed group) - (difference in cheating vs. no cheating for the commandments-primed group)