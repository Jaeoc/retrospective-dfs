Upgrade to the latest version of R and RStudio.

Double-click the file `MazarSrull.Rproj` to open the project in a new RStudio session.

Navigate to File > Open file. It will open to the meta-analysis directory. From here, open the `mazar_srull_meta_analysis.R` file.

Install or upgrade to the latest version the packages loaded in lines 1-12. All packages are required for the entire script to run, from raw data to output.

Click the 'Source' button, or command+A to select all and then the 'Run' button to run the script.