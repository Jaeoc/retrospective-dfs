#********************************************************************************

##Project: Retrospective p-hacking
##Script purpose: Run meta-analytic multiverse functions and save output
##Code: Anton Olsson Collentine (j.a.e.olssoncollentine@uvt.nl)


#********************************************************************************

##Script content:----

#[0] Packages
#[1] Random draws
#[2] Bias sampling based on significance

#********************************************************************************
#[0] Packages
#********************************************************************************
if(!require(metafor)){install.packages("metafor")} #must be installed to use rma_summarizer function
if(!require(purrr)){install.packages("purrr")} #must be installed to use rma_summarizer function (for 'possibly' function)
if(!require(broom)){install.packages("broom")} #required for the PETPEESE function
if(!require(weightr)){install.packages("weightr")} #required for the 3PSM method function
if(!require(puniform)){install.packages("puniform")} #required for the puniform* function
#These packages are are loaded internally by eg., metafor:: to not load full packages

library(parallel) #comes pre-installed with R

source("4.0_functions-meta-analytic-multiverse.r")
source("4.1_PETPEESE-function.r")


set.seed(1334) #for sampler


#********************************************************************************
#[1] Random draws----
#********************************************************************************
#In this section we run with significance and hyp_direction = FALSE
#From the random draws we compute the following in figure 4:
#1) the approximated meta-anlytic distribution, 2) random draw estimate,
#3) most significant, and 4) below alpha

#Load list of study-level files and sort them
files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")

#Running on a single core:
# start <- Sys.time()
# #only secondary output (saved files) so output in console printed as "NULL"
# lapply(files, #list-object we do parallel loop over
#                     sample_meta_save, #function used
#                     samples = 1e5, #number of samples, by default 100
#                     n_equal_more_than = 24, #exclude when less than N in each experimental group
#                     significance = FALSE, #If possible, do only draws amongst significant results
#                     sig_level = 0.05, #using this significance level
#                     hyp_direction = FALSE, #Filter results on whether they were in the hypothesized direction
#                     sampling_type = "complete", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
#                     save_folder = "../data/06_meta-analytic-multiverse-samples/")
#
#
# end <- Sys.time()
# end - start


#For a parallel setup (looping over RRRs) [which is what we actually ran]

start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment

parallel::parLapply(
    cl = cl, # cluster
    files, # list-object we do parallel loop over
    sample_meta_save, # function used
    samples = 1e5, # number of samples, by default 100
    n_equal_more_than = 24, # exclude when less than N in each experimental group
    significance = FALSE, # If possible, do only draws amongst significant results
    sig_level = 0.05, # using this significance level
    hyp_direction = FALSE, # Filter results on whether they were in the hypothesized direction
    sampling_type = "complete", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
    p_bias_correction = TRUE,
    save_folder = "../data/06_meta-analytic-multiverse-samples/")

stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#*******************************
#troubleshooting
#*******************************
# files_intent <- files[[3]]
# a <- lapply(files_intent, sampler, samples = 10)
# b <- order_by_draw(a)
# e <- Map(rma_summarizer, b$es, b$var)


#********************************************************************************
#[2] Bias sampling based on significance----
#********************************************************************************
#Note the changed significance, hyp_direction options compared to above run.
#The difference between the two below runs is in sampling_type = "complete"
#versus sampling_type = "bounded_hack"
#Compared to the above section, we have also set significance and hyp_direction = TRUE
#This section gives us the values in Figure 4 for: 1) random significant,
#and 2) bounded significant

#Run 1 - Random significant (sampling_type = "complete")
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment

parallel::parLapply(cl = cl, #cluster
                    files, #list-object we do parallel loop over
                    sample_meta_save, #function used
                    samples = 1e5, #number of samples, by default 100
                    n_equal_more_than = 24, #exclude when less than N in each experimental group
                    significance = TRUE, #If possible, do only draws amongst significant results
                    sig_level = 0.05, #using this significance level
                    hyp_direction = TRUE, #Filter results on whether they were in the hypothesized direction
                    sampling_type = "complete", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
                    p_bias_correction = TRUE,
                    save_folder = "../data/06_meta-analytic-multiverse-samples/")

stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#Run 2 - Bounded significant (sampling_type = "bounded_hack")
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment

parallel::parLapply(cl = cl, #cluster
                    files, #list-object we do parallel loop over
                    sample_meta_save, #function used
                    samples = 1e5, #number of samples, by default 100
                    n_equal_more_than = 24, #exclude when less than N in each experimental group
                    significance = TRUE, #If possible, do only draws amongst significant results
                    sig_level = 0.05, #using this significance level
                    hyp_direction = TRUE, #Filter results on whether they were in the hypothesized direction
                    sampling_type = "bounded_hack", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
                    p_bias_correction = TRUE,
                    save_folder = "../data/06_meta-analytic-multiverse-samples/")

stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start
