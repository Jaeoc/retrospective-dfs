#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: This script replaces raw values by standardized values with
#                 variance (SE^2) and p-values. In addition, the rows where 100%
#                 of participants were excluded are discarded. This is done once
#                 and resulting datasets saved because takes a relatively long time.
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)


#********************************************************************************

##Script content:----
#[1] Functions
#[2] Summarize and save


#********************************************************************************
#[1] Functions----
#********************************************************************************

smder <- function(m1, SD1, n1, m2, SD2, n2){ #assumes ES is raw mean difference

  m <- m1 - m2
  sdpooled <- sqrt(((n1 - 1)*SD1^2 + (n2 - 1)*SD2^2) / (n1 + n2 - 2)) #Borenstein, M. (2009), p. 226. eq. 12.12, in The handbook of research synthesis and meta-analysis, 2nd ed
  smd <- m / sdpooled
  var_smd <- (n1 + n2) / (n1*n2) + smd^2 / (2*(n1 + n2)) #Borenstein, M. (2009), p. 226, eq. 12.13

  t <- m / (sdpooled * sqrt(1/n1 + 1/n2)) #t-value assuming equal variance and comparing against H0 = 0

  p_value <- 2*pt(-abs(t), n1+n2-2) # two-tailed p-value, (see also https://stats.stackexchange.com/questions/30394/how-to-perform-two-sample-t-tests-in-r-by-inputting-sample-statistics-rather-tha)

  cbind(smd, var_smd, p_value, n1, n2, N = n1 + n2) #SMD and p-value out.
  #In the cases where sdpooled == 0 (rrr3 lab 10) or NA [i.e., when n in either group <= 1] out will be NaN.
  #This is not a problem because later we a) exclude all cases where N =0, and b) keep only cases where n > 1
}

#out we need ES, var_ES, n1, n2, N, p-value
#Need to add the logor variance below
#A, B, C, and D in the function below correspond to:
#********************************************
#           #   Correct  # Incorrect  # total
#********************************************
# Treatment #     A            B      #   n1
# Control   #     C            D      #   n2
#********************************************
#See also the function summarize_OR in 2.0_function-study-level-multiverses.r

logOR <- function(A, B, C, D){

 log_OR <- log((A / B) / (C / D))
 var_log_OR <- 1 / A + 1/ B + 1/ C + 1/ D #https://stats.stackexchange.com/questions/420795/log-odds-standard-error

 z <- log_OR / sqrt(var_log_OR) #z-test, ES/SE
 p_value_onetailed <- pnorm(abs(z), lower.tail = FALSE) #get p-value from standard normal distribution
 p_value <- p_value_onetailed*2 #make two-tailed

 cbind(log_OR, var_log_OR, p_value, n1 = A + B, n2 = C + D, N = A+B+C+D)
}


summarize_smd <- function(dat){ #Dataframe with mean1, sd1, n1, mean2, sd2, n2

  effect_size <- as.data.frame( #as.data.frame to be able to combine with instance in cbind and set the colname for smd, which makes plotting easier
    t( #transpose because otherwise we get the smd and p-value in two rows from the apply function instead of in two columns
      apply(dat, 1, function(x) smder(m1 = x[1], SD1 = x[2], n1 = x[3], m2 = x[4], SD2 = x[5], n2 = x[6])))) #apply because the smder funciton should be applied to each row in the dataframe

  names(effect_size) <- c("effect_size", "var_es", "p_value", "n1", "n2", "N") #otherwise names are V1 V2
  effect_size

}

summarize_logOR <- function(dat){ #dataframe with cells A, B, C, D [A, B = "treatment"; C, D = "control"]

  dat[["zero"]] <- apply(dat, 1, function(row_value) any(row_value == 0)) #check if any cell in a row (lab) has zero observations, add column called "zero" indicating this
  dat <- sapply(dat, function(row_value) ifelse(dat[["zero"]], row_value + 1/2, row_value)) #if so, add 1/2 to all cells in that row

  effect_size <- as.data.frame(t(apply(dat, 1, function(x) logOR(A = x[1], B = x[2], C = x[3], D = x[4]))))

  names(effect_size) <- c("effect_size", "var_es", "p_value", "n1", "n2", "N") #otherwise names is the line of code above
  effect_size
}

summarizer <- function(dat, SMD = TRUE){ #standardize effect sizes

  dat <- dat[dat[["excluded_percent"]] < 100,] #When 100% excluded, sample size get NA (cells ABCD for OR)

  if(SMD){ #i.e, if true
    dat <- dat[dat[["treatment_n"]] > 1 & dat[["control_n"]] > 1,] #keep only rows with cell-size > 1, otherwise sd is NaN and summary functions will throw errors.
    dat <- dat[!(dat[["treatment_sd"]] == 0 & dat[["control_sd"]] == 0),] #drop when both sds == 0, happens in a few cases with low cell-sizes and results in Inf ES
  }else{
    dat <- dat[dat[["cell_A"]] + dat[["cell_C"]] > 1 & dat[["cell_B"]] + dat[["cell_D"]] > 1,] #For OR, do by condition rather than cell. Not necessary but done for consistency.
  }


  if(SMD){ #use if statement to make function work also for odds ratios

    statistics <- dat[, 3:8] #means, sds and ns
    summarized <- summarize_smd(statistics)
    cbind(summarized, dat[,-c(1:8)]) #keep instance + excluded % (cols not 1:9) information for diagnostics later

  }else{ #if log odds ratio
    statistics <- dat[, 3:6] #cells A, B, C, D
    summarized <- summarize_logOR(statistics)
    cbind(summarized, dat[,-c(1:6)]) #keep instance + excluded % (cols not 1:6) information for diagnostics later
  }

}

namer <- function(file_list){ #for naming list elements within the wrapper 'plot_wrapper'
  #use formatC to make names into e.g, L01 instead of L1 for better sorting (any number with width < 2 gets a zero to the left)
  paste0("L", formatC(1:length(file_list), width = 2, format = "d", flag = "0"))
}


read_sum_save <- function(file_path, smd, save_folder){

    dat1 <- readRDS(file_path)
    summed <- summarizer(dat1, SMD = smd)

    split_1 <- unlist(strsplit(file_path, split = "/"))
    RRR_name <- split_1[4]
    split_2 <- unlist(strsplit(split_1[5], split = "_"))
    lab_name <- split_2[3]
    append <- split_2[4]

    df_out <- cbind(RRR = RRR_name, lab = lab_name, summed)
    save_name <- paste0(save_folder, RRR_name, "/",
                        RRR_name, "_multiverse_w_sum_", lab_name, "_", append)
    saveRDS(df_out, save_name)

}


#********************************************************************************
#[2] Summarize and save----
#********************************************************************************

#File paths to multiverse datasets with raw mean group values (smd) and cell numbers (OR)

file_paths <- list.files("../data/04_study_multiverses", #folder with subfolders for each RRR
                         recursive = TRUE, #look up files in all subfolders
                         full.names = TRUE) #get the full file_path instead of just the file name
files_rrr1_2 <- grep("RRR1_|RRR2", file_paths, value = TRUE) #must grep RRR1 with _ because otherwise we also get RRR10
files_rest <- setdiff(file_paths, files_rrr1_2)

#RRR1 and RRR2 (which are (log) odds ratios
invisible(lapply(files_rrr1_2, read_sum_save, smd = FALSE, save_folder = "../data/05_study_multiverses_cleaned/"))
#The invisible function is just to suppress console output (printed null lists) since the function only has side-effects (saving dataframes)

#RRR3-RRR10 which are all mean differences
invisible(lapply(files_rest, read_sum_save, smd = TRUE, save_folder = "../data/05_study_multiverses_cleaned/"))
