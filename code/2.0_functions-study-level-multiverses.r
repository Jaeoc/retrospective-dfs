#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Functions used to create study-level summary statistics from
#a dataset multiverse
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)


#********************************************************************************

##Script content:----

#[0.5] Helper functions
#[1] Common DF functions
#[2] RRR1&2 unique functions
#[3] RRR3 unique functions
#[4] RRR4 unique functions
#[5] RRR5 unique functions
#[6] RRR6 unique functions
#[7] RRR7 unique functions
#[8] RRR8 unique functions
#[9] RRR9 unique functions
#[10] RRR10 unique functions


#********************************************************************************
#[0.5] Helper functions ----
#********************************************************************************
create_multiverse <- function(..., s1 = 0, s2 = 0, e1 = 0, e2 = 0, e3 = 0, e4 = NA, e5 = 0, e6 = 0, e7 = 0){ #add default options for all the common DFs
  if(length(e4) > 1) stop("NB! e4 takes as input the number of e4 variables! All others take the values of the choices.")

  choices <- c(as.list(environment()), ...) #environment command gets the non-dots and the list(...) gets the ellipsis input. NB!

  if(is.na(choices$e4)){ #This stuff is all to setup e4 to permit multiple e4 variables
    choices$e4 <- 0
  }else if(e4 == 1){
    choices$e4 <- 0:1
  }else if(e4 > 1){
    choices$e4 <- NULL
    e4_names <- paste0("e4_", seq_len(e4)) #when multiple e4 variables, create one indicator for each
    e4_dat <- lapply(e4_names, function(x) x = 0:1)
    names(e4_dat) <-  e4_names
    choices <- c(choices, e4_dat)
    all_except_s <- names(choices)[-c(1:2)] #this just to place the new e4 variables after e3 instead of at the end
    choices <- choices[c("s1", "s2", all_except_s[order(all_except_s)])]
  }
  data.matrix(expand.grid(choices)) #expand grid is then fed a named list and thus outputs a named matrix
}

item_rest_cor <- function(df, dv){
  rest_scores <- sapply(dv, function(x) {rowMeans(df[dv[dv != x]], na.rm = TRUE)}) #psych::alpha compute mean scores by default and removes missing
  # colnames(rest_scores) <- paste0(colnames(rest_scores), "_rest")
  diag(cor(df[dv], rest_scores, use = "pairwise.complete.obs")) #psych::alpha uses pairwise.complete.obs by default, see below why we don't
  #Note that the psych::alpha, cor, and cov + cov2cor result in slightly different values when using pairwise.complete.obs. The cov2cor(cov(x)) approach is faulty, see link below.
  #See here for the difference between cor and cov + cov2cor approach: https://stackoverflow.com/questions/58606350/cor-and-cov2cor-different-results-with-use-pairwise-complete-obs/58608515?noredirect=1#comment103528769_58608515
  #psych::alpha uses the variances from cov in their computations (personal correspondence 2019-11-11), and is thus faulty.
}


keep_only_most_common <- function(df, variable){ #variable name should be input surrounded by "". Outputs a dataframe, with only rows with themost common value on 'variable'

  if(all(is.na(df[[variable]]))){ #if only na, don't drop any participants

    df

    }else{
      most_common_value <- names(sort(table(df[[variable]]), decreasing = TRUE))[1]

      df[df[[variable]] == most_common_value | is.na(df[[variable]]),] #keep only most common value or if subject has NA on variable

    }
}


summarize_MD <- function(rrr, dv, condition_var = "condition", conditions = c(NA, NA)){
  if(nrow(rrr) == 0){ #if all subjects excluded
    matrix(ncol = 6) #empty matrix with NAs
  }else{

    treatment_mean <- mean(rrr[[dv]][rrr[condition_var] == conditions[1]])
    control_mean <- mean(rrr[[dv]][rrr[condition_var] == conditions[2]])
    treatment_sd <- sd(rrr[[dv]][rrr[condition_var] == conditions[1]])
    control_sd <- sd(rrr[[dv]][rrr[condition_var] == conditions[2]])
    treatment_n <- nrow(rrr[rrr[condition_var] == conditions[1],])
    control_n <- nrow(rrr[rrr[condition_var] == conditions[2],])

    matrix(c(treatment_mean, treatment_sd, treatment_n, control_mean, control_sd, control_n), ncol = 6)
  }
}

summarize_OR <- function(rrr, dv, condition_var = "condition", conditions = c(NA, NA), value = c(NA, NA)){ #condition eg. c("treatment", "control"), value e.g., c("correct", "incorrect")
  if(nrow(rrr) == 0){
    matrix(ncol = 4) #empty out
  }else{

    condition1_value1 <- sum(rrr[[dv]][rrr[condition_var] == conditions[1]] == value[1]) #cell A in outcome table, where condition in rows and value in cols
    condition1_value2 <- sum(rrr[[dv]][rrr[condition_var] == conditions[1]] == value[2]) #cell B
    condition2_value1 <- sum(rrr[[dv]][rrr[condition_var] == conditions[2]] == value[1]) #cell C
    condition2_value2 <- sum(rrr[[dv]][rrr[condition_var] == conditions[2]] == value[2]) #cell D

    matrix(c(condition1_value1, condition1_value2, condition2_value1, condition2_value2), ncol = 4)

  }
}

#function to use in lapply to create the dataverses for each replication project. Currently only has side-effect (saves data) and no immediate output.
#This is to not end up with a list of 20 with 2.6 million rows each
#Note that the function requires the global objects multiverse_rrr (the multiverse created by the create_multiverse funciton)
#and N_multiverse [= nrow(multiverse_rrr)]. This is to avoid copying the multiverse_rrr object multiple times since it can be very large
dataverser <- function(lab_data, RRR_name, appendix = NULL, apply_df_rrr, dv_input = NULL, es = "md"){ #rrr_name = e.g., "RRR3", dv_appendix = e.g., "intent", apply_df_rrr = e.g., apply_df_rrr3 (no ""), dv_input = eg., dv_items
  if(es == "md"){

    lab_multiverse <- matrix(nrow = N_multiverse, ncol = 7 + length(colnames(multiverse_rrr))) #ncols equals number of colnames below, depends on number of unique DF
    colnames(lab_multiverse) <- c(paste0("treatment_", c("mean", "sd", "n")),
                                paste0("control_", c("mean", "sd", "n")), "excluded_percent", colnames(multiverse_rrr))
  }else if(es == "OR"){
    lab_multiverse <- matrix(nrow = N_multiverse, ncol = 5 + length(colnames(multiverse_rrr))) #
    colnames(lab_multiverse) <- c(paste0("cell_",LETTERS[1:4]), "excluded_percent", colnames(multiverse_rrr))

  }else stop("es must be one of c('md', 'OR')")


  for(r in 1:N_multiverse){ #loop over all multiverse instances for that particular lab

    lab_multiverse[r,] <-  apply_df_rrr(multiverse_rrr[r,], dat = lab_data, dv_input) #do.call allows taking list of (un)named arguments as input for a function. Here all arguments are named
  }


  lab_multiverse <- as.data.frame(lab_multiverse)
  lab_multiverse <- cbind(RRR = RRR_name,lab = lab_data$lab[1], lab_multiverse)
  save_name <- paste0("../data/04_study_multiverses/", RRR_name, "/", RRR_name, "_multiverse_", lab_multiverse$lab[1], "_", appendix,
  ".RDS")
  saveRDS(lab_multiverse, save_name)

}


#********************************************************************************
#[1] Common DF functions ----
#********************************************************************************

apply_s1 <- function(s1, df, dv){

  if(s1 == 0){
    return(dv)

  }else if(s1 == 1){
    cors <- item_rest_cor(df, dv) #gives item rest correlations
    lowest_cor <- names(which.min(cors)) #select item with the lowest item-rest correlation
    dv[dv != lowest_cor] #drop item with lowest cor from dv

  }
  else if(s1 == 2){
    cors <- item_rest_cor(df, dv) #gives item rest correlations
    lowest_cor <- names(cors)[which(cors %in% sort(cors)[1:2])] #select two lowest item-rest correlation items
    dv[!dv %in% lowest_cor] #drop them from dv

  }
}

apply_s2 <- function(s2, df, dv){

  if(length(dv) == 1){ #if only one dv var input and s2 = 0, then no adjustment is made
      return(list(df, dv))


  }else if(s2 == 0){
    df$composite_score <- rowMeans(df[dv], na.rm = TRUE) #na.rm here drops subjects with missing, which after applying e1 corresponds to pair-wise deletion if b was applied there
    list(df, dv = "composite_score")

  }else if(s2 == 1){
    df$composite_score <- rowSums(df[dv], na.rm = TRUE) #na.rm here drops subjects with missing, which after applying e1 corresponds to pair-wise deletion if e1.b was applied (if e1.a applied all NA already removed)
    list(df, dv = "composite_score")

  }else if(s2 == 2){ #if s1 drops items this might -> only 1 item left, doesn't matter for s2 = 0:1, but for s2 = 2 is problematic. The combination s1 = 2 and s2 = 2 must be deleted from the multiverse if length(dv) < 5

    weights <- psych::pca(df[dv], nfactors = 2, rotate = "varimax")$weights[,1] #select first component. The psych::pca by default does pair-wise deletion when computing components, corresponds to e1.b ifs applied (if e1.a applied all NA already)
    weighted_dvs <- sweep(df[dv], 2, weights, "*") #However, it then computes scores with list-wise deletion, meaning this must be manually instead. sweep multiplies each column by the correpponding value in weights
    df$composite_score <- rowSums(weighted_dvs, na.rm = TRUE) #By then using na.rm= TRUE when computing the component scores, this is equivalent to pair-wise deletion scores, weighted by components computed by pairwise deletion as well
    list(df, dv = "composite_score")
  }
}

#We can also use the basic prcomp function to compute principal components, but this does not allow for pair-wise deletion when computing the components (list-wise obligatory)
#Note, by default SPSS does list-wise deletion. The below code is 2 seconds faster for 1e3 iterations for rrr5
#so for 2.6e6 iterations about 2.5 hours faster, and for all 20 labs about 30 hours faster (131 vs 160)
# a <- prcomp(na.omit(df[dv]), center = TRUE, scale = TRUE) #alternative built-in function that might be faster, won't do pairwise deletion when computing scoresthough
# raw_loadings <- a$rotation[,1:2] %*% diag(a$sdev, 2, 2)
# scores <- scale(a$x[,1:2]) %*% varimax(raw_loadings)$rotmat
#the above is equivalent to #psych::pca(na.omit(df[dv]), rotate = "varimax", nfactors = 2, scores = TRUE)$scores
#see also https://stats.stackexchange.com/questions/59213/how-to-compute-varimax-rotated-principal-components-in-r


apply_e1 <- function(e1, df, dv){ #dv variable must be input as "dv", "dv" should be a vector of all dv-items (one or more variables)
  if(e1 == 0){
    df[complete.cases(df[dv]),] #drop all rows where any dv-item is missing
  }
  else{
    missing <- rowSums(is.na(df[dv])) / length(dv) #% input dv variables with missing values
    df[!missing > 0.25,] #list-wise deletion of any rows with more than 25% missing DV-items. Anyone with remaining missing has below 25% and above 0%
  }
}

apply_e2 <- function(e2, df, ...){ #in ... e3 and e4 variables should be input surrounded by ""
  if(e2 == 0){
    df
  }else{

    e3_e4_vars <- list(...)
    df[complete.cases(df[unlist(e3_e4_vars)]),] #drop any rows with missing on e3 or e4 variables

  }
}

apply_e3 <- function(e3, df, age){ #age variable must be quoted "age"
  if(e3 == 0){
    df
  }else if(is.null(age)){ stop('"age_var" must be input when e3 > 0')
  }else if(e3 == 1){
    df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 24),] #NB! subjects not excluded when missing on age_var even when this variable used for exclusions
  }else if(e3 == 2){
    df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 23),]
  }else if(e3 == 3){
    df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 22),]
  }else if(e3 == 4){
    df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 21),]
  }
}

#Alternative version of apply_e3.
# Uses the age ranges: 18-24  18-25, 18-30, and 18-34, as suggested by a reviewer

# apply_e3 <- function(e3, df, age){ #age variable must be quoted "age"
#   if(e3 == 0){
#     df
#   }else if(is.null(age)){ stop('"age_var" must be input when e3 > 0')
#   }else if(e3 == 1){
#     df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 34),] #NB! subjects not excluded when missing on age_var even when this variable used for exclusions
#   }else if(e3 == 2){
#     df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 30),]
#   }else if(e3 == 3){
#     df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 25),]
#   }else if(e3 == 4){
#     df[is.na(df[[age]]) | !(df[[age]] < 18 | df[[age]] > 24),]
#   }
# }

apply_e4 <- function(e4, df, e4_col = NULL){ ##in e4_col name should be input surrounded by ""
    if(e4 == 0){
      df
    }else{ #e4 == 1
      keep_only_most_common(df, variable = e4_col)
    }
}


apply_e5 <- function(e5, df, check){ #NB! first create variable indicating what % of attention checks failed, input as "check"
  if(e5 == 0 || all(is.na(df[[check]]))){ #added condition to return data fram is all is.na [otherwise checks return NA dataframe)
    df
  }else if(is.null(check)){ stop('"e5_var" must be input when e5 > 0')
  }else if(e5 == 1){
    df[df[check] <= 0.5,] #drop participants with less than 50% correct attention checks
  }else if(e5 == 2){
    df[df[check] == 0,] #drop participants with any incorrect attention checks
  }
}

apply_e6 <- function(e6, df, dv){ #enter dv as "dv"
  if(e6 == 0 || nrow(df) < 2){ #can't check for outliers (i.e, can't compute sd, with nrow < 2)
    df
  }else if(e6 == 1){

    two_sd <- 2*sd(df[[dv]])
    avg <- mean(df[[dv]])
    df[(df[[dv]] - avg) <= two_sd,] #keep ss who were less than or equal to 2 sd from mean (i.e., exclude subjects with DV > 2 SD from mean)

  }else if(e6 == 2){

    three_sd <- 3*sd(df[[dv]])
    avg <- mean(df[[dv]])
    df[(df[[dv]] - avg) <= three_sd,] #keep ss who were less than or equal to 3 sd from mean
  }else if(e6 == 3){

    dv_quantiles <- quantile(df[[dv]])
    iqr_1.5 <- 1.5*IQR(df[[dv]]) #check ?IQR to see how quantiles are computed by default
    bottom <- dv_quantiles["25%"] - iqr_1.5
    top <- dv_quantiles["75%"] + iqr_1.5
    df[!(df[[dv]] < bottom | df[[dv]] > top),] #exclude subjects with DV score > 1.5 IQR. NB! Fix in DF list! should be "> 1.5 IQR below Q1 or above Q3"!
  }
}

apply_e7 <- function(e7, df, dv){
  df
} #not applied to any of the included data, skip completely? Also not sure how to compute

#wrapper function
apply_common_DF <- function(instance, df, dv, age_var = NULL, e4_var = NULL, e5_var = NULL){ #variable names must be surrounded by "", input as character vector if multiple
  #input needed: row from multiverse, df, variables
  #dv can be one or multiple items, when multiple must be input as vector
  #output: df after applying all common DF

  #Order within this group and that it runs before the next group is important. order that matters: s1 -> e1 -> s2 (and -> e6, can't run before s2)
  dv <- apply_s1(instance["s1"], df, dv) #drop the dv item with the lowest item-rest correlation
  df <- apply_e1(instance["e1"], df, dv) #
  out_s2 <- apply_s2(instance["s2"], df, dv)
  df <- out_s2[[1]]
  dv <- out_s2[[2]]

  #order within this group does not matter
  df <- apply_e2(instance["e2"], df, age_var, e4_var)
  df <- apply_e3(instance["e3"], df, age_var)
  df <- apply_e5(instance["e5"], df, e5_var)
  df <- apply_e6(instance["e6"], df, dv)
  df <- if(is.null(e4_var)){ #because there can be multiple e4 variables this requires some checking
    df
  }else if(length(e4_var) == 1){
    apply_e4(instance["e4"], df, e4_var)
  }else{ #when multiple
    df1 <- apply_e4(instance["e4_1"], df, e4_var[1]) #currently max 2 e4 variables. To extend, generalize the input "e4_x" and e4_var[x]
    df2 <- apply_e4(instance["e4_2"], df, e4_var[2])
    df <-  merge(df1, df2) #the two must be applied separately, otherwise "the most common" values in e4_var[2] may change after applyying e4_var[1]
  }

  list(df, dv) #out, because dv-variables get updated by s1/s2 when multiple items this must be output as well

}
#********************************************************************************
#[2] RRR1 and RRR2 unique functions ----
#********************************************************************************

rrr1_2_u1 <- function(u1, df){ #input u1 = instance["u1"]
  if(u1 == 0){
    df
  }else{
    df[df$comprehension == "yes",] #exclude subjects who did not understand video
  }
}

rrr1_2_u2 <- function(u2, df){
  if(u2 == 0){
    df
  }else{
    df[is.na(df$fam_with_effect) | df$fam_with_effect == "yes",] #exclude subjects who were familiar with study
  }
}

rrr1_2_u3 <- function(u3, df){
  if(u3 == 0){
    df
  }else{
    df[!is.na(df$confidence),] #exclude subjects who gave no confidence rating
  }
}

rrr1_2_u4 <- function(u4, df){
  if(u4 == 0){
    df
  }else{
    df[is.na(df$mturk_studied_psychology) | df$mturk_studied_psychology == "no",] #exclude subjects who had studied psychology previously
  }
}

rrr1_2_u5 <- function(u5, df){
  if(u5 == 0){
    df
  }else{
    df[is.na(df$mturk_5_attempts) | df$mturk_5_attempts == "yes",] #exclude subjects who did not give at least 5 countries/capitals in control condition
  }
}

rrr1_2_unique <- function(instance, df){
  df <- rrr1_2_u1(instance["u1"], df)
  df <- rrr1_2_u2(instance["u2"], df)
  df <- rrr1_2_u3(instance["u3"], df)
  df <- rrr1_2_u4(instance["u4"], df)
  rrr1_2_u5(instance["u5"], df)
}

#wrapper function
apply_df_rrr1_2 <- function(instance, dat, ...){ #the ... is so that this function does not throw an error when wrapped in the dataverser function because other unique functions need the dv input
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = "lineup_choice", age_var = "age", e4_var = c("race", "mturk_english_first_language"),
                              e5_var = "mturk_attent_checks_passed")
  rrr_x <- rrr_list[[1]] #dataframe
  rrr_x <- rrr1_2_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_OR(rrr_x, dv = "lineup_choice", conditions = c("treatment", "control"), value = c("correct", "incorrect"))


  matrix(cbind(summary,
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 5 + length(instance)) #NB! ncol is different because not a SMD variable
}



#********************************************************************************
#[3] RRR3 unique functions ----
#********************************************************************************


rrr3_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else if(u1 == 1){

    df[!df$native_lang == "other",] #exclude subjects who did not speak English as one of their native languages

  }else{

    df[df$native_lang == "english",] #exclude subjects who did not speak only English as their native language
  }
}

rrr3_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else{
    df[is.na(df$education) | df$education >= 4,] #Keep only subjects with some college education (note, NA are not excluded)
  }
}

rrr3_unique <- function(instance, df){

  df <- rrr3_u1(instance["u1"], df)
  rrr3_u2(instance["u2"], df)
}

apply_df_rrr3 <- function(instance, dv_items, dat){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = dv_items, age_var = "age", e4_var = "native_lang", e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  dv_items <- rrr_list[[2]] #updated vector of dv-items after applying s1/s2
  rrr_x <- rrr3_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = dv_items, conditions = c("imperfect", "perfect")) #conditions = (treatment, control)


  matrix(cbind(summary, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))
}





#********************************************************************************
#[4] RRR4 unique functions ----
#********************************************************************************

rrr4_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else if(u1 == 1){
    df[!df$acc_overall_MSIT < 0.7,]

  }else if(u1 == 2){
    df[!df$acc_overall_MSIT < 0.75,]

  }else if(u1 == 3){
    df[!df$acc_overall_MSIT < 0.8,]

  }else if(u1 == 4){
    df[!df$acc_overall_MSIT < 0.85,]

  }else {
    df[!df$acc_overall_MSIT < 0.9,]
  }
}

rrr4_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else if(u2 == 1){
    df[!df$acc_overall_LetE < 0.7 | is.na(df$acc_overall_LetE),]  #one subject has missing on acc_overall_LetE even after removing missings on DV

  }else if(u2 == 2){
    df[!df$acc_overall_LetE < 0.75 | is.na(df$acc_overall_LetE),]

  }else if(u2 == 3){
    df[!df$acc_overall_LetE < 0.8 | is.na(df$acc_overall_LetE),]

  }else if(u2 == 4){
    df[!df$acc_overall_LetE < 0.85 | is.na(df$acc_overall_LetE),]

  }else {
    df[!df$acc_overall_LetE < 0.9 | is.na(df$acc_overall_LetE),]

  }
}

rrr4_unique <- function(instance, df){

  df <- rrr4_u1(instance["u1"], df)
  df <- rrr4_u2(instance["u2"], df)
  apply_e6(e6 = instance["u3"], df = df, dv = "RT_MSIT_mean_incongruent") #u3 is the same as e6 but applied to secondary DV
}

apply_df_rrr4 <- function(instance, dat, ...){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = "RTV_MSIT_mean_incongruent", age_var = "age", e4_var = c("native_speaker", "race"), e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  rrr_x <- rrr4_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = "RTV_MSIT_mean_incongruent", conditions = c("treatment", "control"))



  matrix(cbind(summary, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))
}
#********************************************************************************
#[5] RRR5 unique functions ----
#********************************************************************************

rrr5_u1 <- function(u1, df){
  if(u1 == 0){
    df
  }else{
    df[is.na(df$in_rom_rel) | df$in_rom_rel == "current",] #drop all subjects not currently in a romantic relationship
  }
}

rrr5_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else if(u2 == 1){
    df[is.na(df$months_dating) | !df$months_dating < 2,] #drop all who have been dating < 2 months

  }else if(u2 == 2){
    df[is.na(df$months_dating) | !df$months_dating < 3,] #drop all who have been dating < 3 months

  }else if(u2 == 3){
    df[is.na(df$months_dating) | !df$months_dating < 4,] #drop all who have been dating < 4 months
  }
}

rrr5_u3 <- function(u3, e1, df){
  if(u3 == 0){
    df

  }else{ #when u3 == 1
    if(e1 == 0){ #when any NA in DV leads to list-wise deletion

      df[rowSums(df[c("NA_exit_percent", "NA_neglect_percent", "NA_voice_percent", "NA_loyalty_percent")]) == 0,] #drop subjects with missing on any other DV item

    }else if(e1 == 1){ #when more than 25% NA leads to listwise deletion and below = pair-wise deletion. Only drop cases when the ohter DVs have > 25% missing

      df[apply(df[c("NA_exit_percent", "NA_neglect_percent", "NA_voice_percent", "NA_loyalty_percent")], 1, function(x) !any(x > 25)),] #d
    }
  }
}


rrr5_u4 <- function(u4, df){
  if(u4 == 0){
    df

  }else{
    df[complete.cases(df[c("in_rom_rel", "months_dating", "status_of_rel",
                           "rel_exclusive", "days_per_month", "partner_within_60m")]),] #drop all subjects with missing value on any of the u1, u2, or u5:u8 vars
  }
}

rrr5_u5 <- function(u5, df){
  if(u5 == 0){
    df

  }else if(u5 == 1){
    df[grepl("dating", df$status_of_rel),] #drop all not dating
  }else { #u5 == 2
    keep_only_most_common(df, variable = "status_of_rel")
  }
}


rrr5_u6 <- function(u6, df){
  if(u6 == 0){
    df

  }else{
    keep_only_most_common(df, variable = "rel_exclusive")
  }
}

rrr5_u7 <- function(u7, df){
  if(u7 == 0){
    df

  }else if(u7 == 1){
    df[is.na(df$days_per_month) | !df$days_per_month < 1,] #drop all who see each other less than 1 day per month
  }else { #u7 == 2
    df[is.na(df$days_per_month) | !df$days_per_month < 2,] #drop all who see each other less than 2 day per month
  }
}

rrr5_u8 <- function(u8, df){
  if(u8 == 0){
    df

  }else{
    df[is.na(df$partner_within_60m) | df$partner_within_60m == "yes",] #drop ss who do not live within 60 miles of their partner
  }
}

rrr5_u9 <- function(u9, df){
  if(u9 == 0){
    df

  }else if(u9 == 1){
    df[is.na(df$year_in_school) | df$year_in_school == 1,] #drop all with school year > 1 [zero does not exist]
  }else { #u9 == 2
    df[is.na(df$year_in_school) | df$year_in_school < 5,] #drop all ss in 'other' category (i.e., keep only ss in year 1-4)
  }
}


rrr5_unique <- function(instance, df){
  df <- rrr5_u1(u1 = instance["u1"], df)
  df <- rrr5_u2(u2 = instance["u2"], df)
  df <- rrr5_u3(u3 = instance["u3"], e1 = instance["e1"], df)
  df <- rrr5_u4(u4 = instance["u4"], df)
  df <- rrr5_u5(u5 = instance["u5"], df)
  df <- rrr5_u6(u6 = instance["u6"], df)
  df <- rrr5_u7(u7 = instance["u7"], df)
  df <- rrr5_u8(u8 = instance["u8"], df)
  rrr5_u9(u9 = instance["u9"], df)
}

#wrapper function
apply_df_rrr5 <- function(instance, dv_items, dat){

  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = dv_items, age_var = "age", e4_var = NULL, e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  dv_items <- rrr_list[[2]] #updated vector of dv-items after applying s1/s2
  rrr_x <- rrr5_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary_dat <- summarize_MD(rrr_x, dv = dv_items, conditions = c("treatment", "control"))


  #out
  matrix(cbind(summary_dat, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))

}


#********************************************************************************
#[6] RRR6 unique functions ----
#********************************************************************************

#EDIT: for U3 -> computing values with pair-wise deletion in some cases makes code too unclear, change to only exclude

rrr6_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else{
    df[is.na(df$guessed_study_goal) | df$guessed_study_goal == "no",] #exclude participants who guessed study goal
  }
}

rrr6_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else{
    df[is.na(df$comprehension_cartoons) | df$comprehension_cartoons == "yes",] #exclude participants who did not understand cartoons
  }
}

rrr6_u3 <- function(u3, df){
  if(u3 == 0){
    df

  }else if(u3 == 1){
    df[df$pen_correct_total > 2,] #exclude participants who held 1 or 2 pens /4 incorrectly

  }else if(u3 == 2){
    df[df$pen_correct_total > 1,] #exclude participants who held 1,2, or 3 pens /4 incorrectly

  }else{ #u3 == 3
    df[df$pen_correct_total == 4,] #exclude participants who held pen incorrectly for any cartoon
  }
}

rrr6_u4 <- function(u4, df){
  if(u4 == 0){
    df

  }else if(u4 == 1){
    df[df$pen_correct_self_report >= 2,]

  }else { #u4 == 2
    df[df$pen_correct_self_report >= 5,]
  }
}

rrr6_u5 <- function(u5, df){
  if(u5 == 0){
    df

  }else{
    keep_only_most_common(df, variable = "study_field")
  }
}

rrr6_unique <- function(instance, df){
  df <- rrr6_u1(u1 = instance["u1"], df)
  df <- rrr6_u2(u2 = instance["u2"], df)
  df <- rrr6_u3(u3 = instance["u3"], df)
  df <- rrr6_u4(u4 = instance["u4"], df)
  rrr6_u5(u5 = instance["u5"], df)
}

apply_df_rrr6 <- function(instance, dv_items, dat){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = dv_items, age_var = "age", e4_var = "student", e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  dv_items <- rrr_list[[2]] #updated vector of dv-items after applying s1/s2
  rrr_x <- rrr6_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = dv_items, conditions = c("smile", "pout"))



  matrix(cbind(summary, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))
}

#********************************************************************************
#[7] RRR7 unique functions ----
#********************************************************************************

#NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

rrr7_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else{
    df[df$comprehend == "yes",] #drop all subjects who failed comprehension check
  }
}

rrr7_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else{
    df[df$decision_time == "compliant",] #drop all did not comply with time limit
  }
}

rrr7_u3 <- function(u3, df){
  if(u3 == 0){
    df

  }else if(u3 == 1){
    df[!df$participated_identical_exp > 1,] #drop all who participated in > 1 identical experiment previously

  }else if(u3 == 2){
    df[!df$participated_identical_exp > 2,] #drop all who participated in > 2 identical experiments previously

  }else if(u3 == 3){
    df[!df$participated_identical_exp > 3,] #drop all who participated in > 3 identical experiments previously
  }
}

rrr7_u4 <- function(u4, df){
  if(u4 == 0){
    df

  }else{
    df[df$missing_non_DV == "no",] #only keep ss without missing on any task
  }
}

rrr7_u5 <- function(u5, df){
  if(u5 == 0){
    df

  }else{
    df[df$heard_about_study == "no",] #keep only those who had not heard of the study before
  }
}

rrr7_u6 <- function(u6, df){
  if(u6 == 0){
    df

  }else if(u6 == 1){
    df[is.na(df$know_other_part) |!df$know_other_part > 0,] #drop all who knew other participants in the room. NB! has missings so these must be kept

  }else if(u6 == 2){
    df[is.na(df$know_other_part) |!df$participated_identical_exp > 1,] #drop all who knew more than one other participant in the room

  }else if(u6 == 3){
    df[is.na(df$know_other_part) | !df$participated_identical_exp > 2,] #drop all who knew more than two other other participants in the room

  }
}

rrr7_u7 <- function(u7, df){
  if(u7 == 0){
    df

  }else if(u7 == 1){
    df[is.na(df$participated_any_exp) | !df$participated_any_exp > 0,] #drop all who participated in > 0 experiments previously. NB! has missings so these must be kept

  }else if(u7 == 2){
    df[is.na(df$participated_any_exp) | !df$participated_any_exp > 5,] #drop all who participated in > 5 experiments previously

  }else if(u7 == 3){
    df[is.na(df$participated_any_exp) | !df$participated_any_exp > 10,] #drop all who participated in > 10 experiments previously
  }
}

rrr7_u8 <- function(u8, df){
  if(u8 == 0){
    df

  }else if(u8 == 1){
    df[is.na(df$participated_paid_exp) | !df$participated_paid_exp > 0,] #drop all who participated in > 0 experiments previously. NB! has missings so these must be kept

  }else if(u8 == 2){
    df[is.na(df$participated_paid_exp) | !df$participated_paid_exp > 5,] #drop all who participated in > 5 experiments previously

  }else if(u8 == 3){
    df[is.na(df$participated_paid_exp) | !df$participated_paid_exp > 10,] #drop all who participated in > 10 experiments previously
  }
}

rrr7_u9 <- function(u9, df){
  if(u9 == 0){
    df

  }else if(u9 == 1){
    df[!df$participated_online_exp > 0,] #drop all who participated in > 0 experiments previously

  }else if(u9 == 2){
    df[!df$participated_online_exp > 5,] #drop all who participated in > 5 experiments previously

  }else if(u9 == 3){
    df[!df$participated_online_exp > 10,] #drop all who participated in > 10 experiments previously
  }
}

rrr7_u10 <- function(u10, df){
  if(u10 == 0){
    df

  }else if(u10 == 1){
    df[is.na(df$participated_deceitful_exp) | !df$participated_deceitful_exp > 0,] #drop all who participated in > 0 experiments previously. NB! has missings so these must be kept

  }else if(u10 == 2){
    df[is.na(df$participated_deceitful_exp) |  !df$participated_deceitful_exp > 5,] #drop all who participated in > 5 experiments previously

  }else{ #if u10 == 3
    df[is.na(df$participated_deceitful_exp) |  !df$participated_deceitful_exp > 10,] #drop all who participated in > 10 experiments previously
  }
}

rrr7_unique <- function(instance, df){
  df <- rrr7_u1(u1 = instance["u1"], df)
  df <- rrr7_u2(u2 = instance["u2"], df)
  df <- rrr7_u3(u3 = instance["u3"], df)
  df <- rrr7_u4(u4 = instance["u4"], df)
  df <- rrr7_u5(u5 = instance["u5"], df)
  df <- rrr7_u6(u6 = instance["u6"], df)
  df <- rrr7_u7(u7 = instance["u7"], df)
  df <- rrr7_u8(u8 = instance["u8"], df)
  df <- rrr7_u9(u9 = instance["u9"], df)
  rrr7_u10(u10 = instance["u10"], df)
}

#wrapper function
apply_df_rrr7 <- function(instance, dat, ...){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = "contribution", age_var = "age", e4_var = NULL, e5_var = NULL)
  rrr_x <- rrr_list[[1]] #because the DV is a single item s1/s2 not applied
  rrr_x <- rrr7_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = "contribution", conditions = c("time pressure", "forced delay"))



  matrix(cbind(summary,
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 26)
}

#********************************************************************************
#[8] RRR8 unique functions ----
#********************************************************************************


rrr8_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else if(u1 == 1){
    df[is.na(df$link_yn) | df$link_yn == "no",] #exclude participants who believed there was a link between tasks

  }else if(u1 == 2){
    df[is.na(df$thinking_yn) | df$thinking_yn == "no",] #exclude participants who thought first task affected performance on second task

  }else if(u1 == 3){
    df[is.na(df$thinking_yn) | is.na(df$link_yn) | !(df$thinking_yn == "yes" | df$link_yn == "yes"),] #exclude participants if either 1 or 2

  }else{ #u1 == 4
    df[is.na(df$thinking_yn) | is.na(df$link_yn) | !(df$thinking_yn == "yes" & df$link_yn == "yes"),] #exclude participants if both 1 and 2
  }
}

rrr8_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else{
    df[is.na(df$prior_hool) | df$prior_hool == "yes",] #exclude participants who were not familiar with the term "hooligan"
  }
}

rrr8_u3 <- function(u3, df){
  if(u3 == 0){
    df

  }else{
    df[is.na(df$year) | df$year %in% c("1", "1 or more", "not a student"),] #exclude participants clearly in year > 1 who might have had experimental experience
  }
}


rrr8_u4 <- function(u4, df){
  if(u4 == 0){
    df

  }else{
    keep_only_most_common(df, variable = "major")
  }
}

rrr8_unique <- function(instance, df){
  df <- rrr8_u1(u1 = instance["u1"], df)
  df <- rrr8_u2(u2 = instance["u2"], df)
  df <- rrr8_u3(u3 = instance["u3"], df)
  rrr8_u4(u4 = instance["u4"], df)
}

apply_df_rrr8 <- function(instance, dv_items, dat){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = dv_items, age_var = "age", e4_var = c("student", "language"), e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  dv_items <- rrr_list[[2]] #updated vector of dv-items after applying s1/s2
  rrr_x <- rrr8_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = dv_items, conditions = c("hooligan", "professor")) #conditions = (treatment, control)



  matrix(cbind(summary, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))
}
#********************************************************************************
#[9] RRR9 unique functions ----
#********************************************************************************


rrr9_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else{
    df[!is.na(df$gender),] #exclude participants who did not report gender
  }
}

rrr9_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else{
    df[df$sentence_complete == "complete",] #exclude participants who did not complete sentence descrambling task
  }
}

rrr9_u3 <- function(u3, df){
  if(u3 == 0){
    df

  }else{
    df[df$below_50_participants == "no",] #exclude participants who when their experimental group was < 50 ss
  }
}

rrr9_u4 <- function(u4, df){
  if(u4 == 0){
    df

  }else{
    keep_only_most_common(df, variable = "major")
  }
}


rrr9_unique <- function(instance, df){
  df <- rrr9_u1(u1 = instance["u1"], df)
  df <- rrr9_u2(u2 = instance["u2"], df)
  df <- rrr9_u3(u3 = instance["u3"], df)
  rrr9_u4(u4 = instance["u4"], df)
}

apply_df_rrr9 <- function(instance, dv_items, dat){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = dv_items, age_var = "age", e4_var = c("language", "student"), e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  dv_items <- rrr_list[[2]] #updated vector of dv-items after applying s1/s2
  rrr_x <- rrr9_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = dv_items, conditions = c("treatment", "control"))

  matrix(cbind(summary, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))
}
#********************************************************************************
#[10] RRR10 unique functions ----
#********************************************************************************

rrr10_u1 <- function(u1, df){
  if(u1 == 0){
    df

  }else{
    df[!is.na(df$gender),] #exclude participants who did not report gender
  }
}

rrr10_u2 <- function(u2, df){
  if(u2 == 0){
    df

  }else if(u2 == 1){
    df[is.na(df$intervention_completion) | !df$intervention_completion == "no list and no time spent",] #exclude participants who did not complete intervention and reported spending no time

  }else {
    df[is.na(df$intervention_completion) | df$intervention_completion == "completed",] #exclude participants who did not complete interventions
  }
}

rrr10_u3 <- function(u3, df){
  if(u3 == 0){
    df

  }else{
    df[df$below_50_participants == "no",] #exclude participants who when their experimental group was < 50 ss
  }
}


rrr10_u4 <- function(u4, df){
  if(u4 == 0){
    df

  }else if(u4 == 1){
    df[is.na(df$religiousness) | !df$religiousness < 3,] #exclude participants who had a religiousness score below 3

  }else {
    df[is.na(df$religiousness) | !df$religiousness < 4,] #exclude participants who had a religiousness score below 4
  }
}

rrr10_u5 <- function(u5, df){
  if(u5 == 0){
    df

  }else{
    keep_only_most_common(df, variable = "major")
  }
}

rrr10_unique <- function(instance, df){
  df <- rrr10_u1(u1 = instance["u1"], df)
  df <- rrr10_u2(u2 = instance["u2"], df)
  df <- rrr10_u3(u3 = instance["u3"], df)
  df <- rrr10_u4(u4 = instance["u4"], df)
  rrr10_u5(u5 = instance["u5"], df)

}

apply_df_rrr10 <- function(instance, dat, ...){
  n_original <- nrow(dat)
  rrr_list <- apply_common_DF(instance, df = dat, dv = "matrices_solved", age_var = "age", e4_var = c("language", "student"), e5_var = NULL)
  rrr_x <- rrr_list[[1]] #dataframe
  rrr_x <- rrr10_unique(instance, rrr_x) #NB! By default, missing values on variables does not lead to exclusion, even when those variables are used for exclusions.

  #summary data
  summary <- summarize_MD(rrr_x, dv = "matrices_solved", conditions = c("treatment", "control"))

  matrix(cbind(summary, #out as matrix to improve speed of subsetting
               excluded_percent = (1 - nrow(rrr_x)/n_original)*100,
               t(instance)), #must be transposed to be a row in a matrix rather than a vector with rownames
         ncol = 7 + length(instance))
}
