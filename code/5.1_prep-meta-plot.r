#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Produce main paper plot
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)

#********************************************************************************

##Script content:----

#[0] Packages and sourced functions
#[1] Unique meta-analyses results
#**[1.1] Most significant
#**[1.2] Below alpha
#[2] Summarize meta-analytic samples
#**[2.1] random sample
#**[2.2] Random samples with bounded p-hacked multiverse
#**[2.3] Random  sample with significant multiverse
#**[2.4] The distribution of the multiverse
#[3] Pre-registered meta-analytic values
#[4] Multiverse sizes
#[5] Median N and multiverse after filtering
#[6] Combine into list-object and save

#********************************************************************************
#[0] Packages and sourced functions----
#********************************************************************************

#for files_by_meta-analysis and sig_selector_list functions
source("4.0_functions-meta-analytic-multiverse.r") 
source("5.0_functions-plots-and-tables.r")

#********************************************************************************
#[1] Unique meta-analyses results----
#********************************************************************************
#That is, most significant effect sizes per each lab and the effect sizes closes
#to the alpha cutoff (0.05 by default)

#get file paths to study-level multiverses
files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")

#**[1.1] Most significant----
#Extract the most signigicant effect sizes per lab and meta-analyze
a <- lapply(files, sig_selector_list, n_equal_more_than = 24, type = "most_sig", hyp_direction = TRUE)
# a <- lapply(files, sig_selector_list, n_equal_more_than = 2, type = "most_sig", hyp_direction = TRUE) #for supplment S3
most_sig <- dplyr::bind_rows(a, .id = "meta_analysis")

results_most_sig <- most_sig[most_sig$type == "meta",] #get only the rma summaries for plotting

#**[1.2] Below alpha----
#Extract the effect sizes with p closes to alpha per lab and meta-analyze
a <- lapply(files, sig_selector_list, n_equal_more_than = 24, type = "below_alpha", hyp_direction = TRUE)
# a <- lapply(files, sig_selector_list, n_equal_more_than = 24, type = "below_alpha", hyp_direction = TRUE) #for supplement s3
below_alpha <- dplyr::bind_rows(a, .id = "meta_analysis")

results_below_alpha <- below_alpha[below_alpha$type == "meta",]


#********************************************************************************
#[2] Summarize meta-analytic samples----
#********************************************************************************
#These all consist of multiple sampled meta-analytic estimates and to plot nicely
#we take the average of them

#**[2.1] random sample----
files_all <- list.files("../data/06_meta-analytic-multiverse-samples", full.names = TRUE)
#files_all <- list.files("../data/07_supplement-s3-meta-analytic-samples", full.names = TRUE) #to create data for supplemental Figure 5 without sample size restriction 
files_full_sample <- grep("sig", files_all, value = TRUE, invert = TRUE) #invert = TRUE -> "do not select". If we have any significance sample (bounded files dont pick these here

summary_random_draws <- load_summarize_rma_multiverses(files_full_sample)

#**[2.2] Random samples with bounded p-hacked multiverse----
files_bounded_draws <- grep("bound", files_all, value = TRUE) #If we have any bounded sample files  pick these here

summary_bounded_draws <- load_summarize_rma_multiverses(files_bounded_draws)

#**[2.3] Random  sample with significant multiverse----
files_sig_draws <- grep("sig", files_all, value = TRUE) #If we have any significance sample files  pick these here
files_sig_draws <- setdiff(files_sig_draws, files_bounded_draws) #drop the sig_bound draws

summary_sig_draws <- load_summarize_rma_multiverses(files_sig_draws)


#**[2.4] The distribution of the multiverse----
load_full <- load_and_rename(files_full_sample)
load_full <- lapply(load_full, function(x) x$rma_summary) #select the random effect model (i.e, not publication bias models)
distribution <- lapply(load_full, "[", 1) #extract first column

#combine into dataframe
distribution <- dplyr::bind_rows(distribution, .id = "meta_analysis")

#********************************************************************************
#[3] Meta-analytic multiverse SD----
#********************************************************************************

#split distribution by RRR, compute SD, then recombine
dist_split <- split(distribution, distribution$meta_analysis)
dist_split <- lapply(dist_split, 
                     function(x) data.frame(sd = sd(x$rma_es)))

meta_sd <- dplyr::bind_rows(dist_split, .id = "meta_analysis")
meta_sd$label <- paste0("UMV: ", 
                        format(round(meta_sd$sd, 2), nsmall = 2))

#********************************************************************************
#[4] Pre-registered meta-analytic values----
#********************************************************************************
#These may differ slightly from those reported by original studies because we ran
#the basic rma and not with e.g., knapp and hartung adjustment to be comparable
#to our other meta-analyses

#Get summary data from heterogeneity project
dat_original_file <- list.files("../data/01_original-summary-data", full.names = TRUE, pattern = ".csv")
dat_original <- read.csv(dat_original_file, stringsAsFactors = FALSE)

#Filter out Many Labs projects
dat <- dat_original[grep("RRR", dat_original$rp),]

#split and rename for clarity
dat_split <- split(dat, dat$rp)
dat_split <- dat_split[c(1, 3:10, 2)] #re-order so that RRR10 is last instead of after RRR1

names(dat_split) <- paste0("RRR", #add names of the shape RRR01 - RRR09 for better ordering later
                           formatC(1:length(dat_split), #If we didn't drop RRR10 above this would be more difficult
                                   width = 2, format = "d", flag = "0"))


#split out the different meta-analyses in RRR3, RRR5 and RRR9 

#Separate by dv rrr3
rrr3_intent <- dat_split$RRR03[dat_split$RRR03$effect == "Grammar on intentionality",]
rrr3_attrib <- dat_split$RRR03[dat_split$RRR03$effect == "Grammar on intention attribution",]
rrr3_process <- dat_split$RRR03[dat_split$RRR03$effect == "Grammar on detailed processing",]

#separate by dv rrr5
rrr5_neglect <- dat_split$RRR05[dat_split$RRR05$effect == "Commitment on neglect",]
rrr5_exit <- dat_split$RRR05[dat_split$RRR05$effect == "Commitment on exit",]

#separate by dv rrr9
rrr9_behavior <- dat_split$RRR09[dat_split$RRR09$effect == "Hostility priming - behavior",] 
rrr9_hostility <- dat_split$RRR09[dat_split$RRR09$effect == "Hostility priming - hostility",] 


#Add these as separate elements
dat_split[c("RRR03", "RRR05", "RRR09")] <- NULL #drop original from list

dat_split <- c(dat_split,
                 list('RRR03 Attribution' = rrr3_attrib,
                      'RRR03 Intention' = rrr3_intent, #names improved for later plotting
                      'RRR03 Process' = rrr3_process,
                      'RRR05 Neglect' = rrr5_neglect,
                      'RRR05 Exit' = rrr5_exit,
                      'RRR09 Behavior' = rrr9_behavior,
                      'RRR09 Hostility' = rrr9_hostility)
)


#Meta-analyze and summarize
results_original <- lapply(dat_split, meta_analyzer_original)
results_original <- dplyr::bind_rows(results_original, .id = "meta_analysis") 


#********************************************************************************
#Save the single-MA results at the lab-level(!) for publication bias estimates
# (i.e., most sig, below alpha, pre-reg)
# saveRDS(list(most_sig = most_sig, below_alpha = below_alpha, pre_reg = dat_split),
#         "../data/single_MA_study_level.RDS")

#********************************************************************************
#[4] Multiverse sizes----
#********************************************************************************

files <- files_by_meta_analysis(directory = "../data/04_study_multiverses") #NB! must use the 04_study_multiverses
# because in 05_study_multiverses_cleaned instances with 100% exclusions were removed
files <- lapply(files, "[[", 1)
list_res <- lapply(files, function(x) nrow(readRDS(x))) #gives us the original multiverse sizes
multiverse_sizes <- do.call(rbind, list_res)
multiverse_sizes <- data.frame("meta_analysis" = row.names(multiverse_sizes),
                               N = paste0("M: ",
                                          format(multiverse_sizes, big.mark=",", trim=TRUE))) #add comma, see https://stackoverflow.com/questions/3838774/comma-separator-for-numbers-in-r


#********************************************************************************
#[5] Median N and multiverse after filtering----
#********************************************************************************

files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")
#Here we use the 05_study_multiverses_cleaned which gives multiverse with 100%
#exclusions removed, and more importantly, consistent names ("N") for sample size
#across log odds ratio and smd effect types

#Extract median N (w/ quantiles) per multiverse in each lab, and lab multiverse size
#both after excluding small-N
list_res <- lapply(files, list_extract_N, n_equal_more_than = 24) 
#list_res <- lapply(files, list_extract_N, n_equal_more_than = 2) #For supplement S3

#Take the median N across the median Ns to get it at the meta-level
#also median multiverse size in a meta-analysis after exclusions on sample size
meta_median_n <- lapply(list_res, function(x)  
  data.frame(as.list(apply(x, 2, median)))) #apply outputs named vector and we want to conver this to a data.frame

#combine
meta_median_n <- dplyr::bind_rows(meta_median_n, .id = "meta_analysis")

#Formatting
meta_median_n[,c(2:5)] <- round(meta_median_n[,c(2:5)], digits = 0) #round to closest whole number
meta_median_n$M_filtered_median <- paste0("M: ", #Median multiverse size after filtering
                                   format(meta_median_n$M_filtered, big.mark=",", trim=TRUE)) #add comma
meta_median_n$N <- paste0("N: ", meta_median_n$median_n, 
                          " [", meta_median_n$lower_quartile,
                          ", ", meta_median_n$upper_quartile,
                          "]")

meta_median_n <- meta_median_n[,c("meta_analysis", "N", #select final vars
                                  "M_filtered_median")]


#********************************************************************************
#[6] Combine into list-object and save----
#********************************************************************************

res <- list("Random draw" = summary_random_draws,
            "Random significant" = summary_sig_draws,
            "Bounded significant" = summary_bounded_draws,
            "Pre-registered" = results_original,
            "Most significant" = results_most_sig,
            "Below alpha" = results_below_alpha)


#combine estimates into one dataframe
res <- dplyr::bind_rows(res, .id = "Selection")

#combine everything into a list for plotting 
plot_data <- list(estimates = res,
                  distribution = distribution,
                  multiverse_sizes = multiverse_sizes,
                  median_N = meta_median_n,
                  multiverse_sd = meta_sd)

#Save output
# saveRDS(plot_data, "../data/meta_plot_data_n24.RDS")

#If creating data for supplement (search this file for "supplement" for other commented out lines)
# saveRDS(plot_data, "../data/meta_plot_data_n2.RDS") 
