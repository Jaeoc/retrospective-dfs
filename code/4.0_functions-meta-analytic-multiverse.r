#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Functions to compute meta-analytic multiverse
##Code: Anton Olsson Collentine (j.a.e.olssoncollentine@uvt.nl)


#********************************************************************************

#A note on save file-format for large data:
#Turns out rds files are compressed, cannot read particular rows, and are too
#large uncompressed. Other file-types (and also uncompressed rds files) are faster
#but too large, making RDS file s the best alternative,
#see https://waterdata.usgs.gov/blog/formats/
#********************************************************************************

##Script content:----

#[0] Function to order file-paths
#[1] Internal functions
#[2] Functions to input a single file path
#[3] Functions to clean up results from single path functions
#[4] Functions to meta-analyze samples from single file path functions
#[5] Functions to input lists of file_paths


#********************************************************************************
#[0] Function to order file-paths----
#********************************************************************************

#for creating a list of pathways by meta-analysis
files_by_meta_analysis <- function(directory){

  #Load files according to folder structure
  files <- list.files(directory, recursive = TRUE, full.names = TRUE) #load files across all folders
  files_by_folder <- split(files, dirname(files)) #split by folders
  files_by_folder <- files_by_folder[c(1, 3, 4:10, 2)] #reorder because RRR10 comes after RRR1
  names(files_by_folder) <- paste0("RRR", #add names of the shape RRR01 - RRR10
                                   formatC(1:length(files_by_folder),
                                           width = 2, format = "d", flag = "0"))


  #Separate by dv rrr3
  rrr3_intent <- grep("intent", files_by_folder[["RRR03"]], value = TRUE)
  rrr3_attrib <- grep("attrib", files_by_folder[["RRR03"]], value = TRUE)
  rrr3_process <- grep("process", files_by_folder[["RRR03"]], value = TRUE)

  #separate by dv rrr5
  rrr5_neglect <- grep("neglect", files_by_folder[["RRR05"]], value = TRUE)
  rrr5_exit <- grep("exit", files_by_folder[["RRR05"]], value = TRUE)

  #separate by dv rrr9
  rrr9_behav <- grep("behavior", files_by_folder[["RRR09"]], value = TRUE)
  rrr9_host <- grep("hostility", files_by_folder[["RRR09"]], value = TRUE)

  #Add these as separate elements
  files_by_folder[c("RRR03", "RRR05", "RRR09")] <- NULL #drop original from list

  files_by_ma <- c(files_by_folder,
                   list('RRR03 Intention' = rrr3_intent, #names improved for later plotting
                        'RRR03 Attribution' = rrr3_attrib,
                        'RRR03 Process' = rrr3_process,
                        'RRR05 Neglect' = rrr5_neglect,
                        'RRR05 Exit' = rrr5_exit,
                        'RRR09 Behavior' = rrr9_behav,
                        'RRR09 Hostility' = rrr9_host)
  )

  files_by_ma[c(1:2, 8:10, 3, 11:12, 4:6, 13:14, 7)] #reorder output to be in RRR order


}



#********************************************************************************
#[1] Internal functions----
#********************************************************************************
#internal function to the sampler function below
read_filter <- function(file_path, n_equal_more_than){
  #drops instances where either experimental cell is smaller than the set value
  dat <- readRDS(file_path)
  # dat <- data.matrix(dat) #doesn't actually seem to speed things up + makes RRR and lab labels useless, which is annoying for plotting
  dat[dat[,"n1"] >= n_equal_more_than & dat[,"n2"] >= n_equal_more_than,]

}

#function to print a warning when a lab has no multiverse with more participants
#than requested
get_warning_message <- function(file_path, n_equal_more_than){

  #get RRR and lab name for a more informative warning message
  split_1 <- unlist(strsplit(file_path, split = "/"))
  RRR_name <- split_1[4]
  split_2 <- unlist(strsplit(split_1[5], split = "_"))
  lab_name <- split_2[5]

  #output message
  paste0("The ", RRR_name, " ", lab_name,
         " dataset has no multiverse with more than the minimum set sample size (",
         n_equal_more_than, ") -> output set to NULL.")

}



#This function filters and select only significant p-values if any exist
subset_sig_values <- function(dat, sig_level){

  dat_out <- dat[dat[,"p_value"] <= sig_level,] #subset only the significant values

  #if no significant values select the effect size with the smallest p-value
  #NB! this results in a single (or multiple if several equally low p-value) result
  if(nrow(dat_out) == 0){
    min_p <- min(dat[,"p_value"]) #use min and two lines of code instead of which.min
    dat_out <- dat[dat[,"p_value"] == min_p, ] #just so that we can get multiple lines of output
    }

  dat_out #out
}


#This function finds the effect size with a p-value closest to the cutoff
find_closest <- function(p_value, cutoff = 0.05){ # Output is index in the p_value vector

  if(min(p_value) > cutoff){ #if no p-values below the cutoff

    which.min(p_value - cutoff) #select the observation which is closest to the cutoff

  }else{ #if there are p-value below the cutoff, choose the one closest below the cutoff

    p_sig <- p_value[p_value <= cutoff] #select all p-values below or equal to the cutoff
    max_sig <- max(p_sig) #check which one is closest to the cutoff
    index <- which(p_value == max_sig) #find the index (possibly multiple) in original vector that corresponds

    if(length(index) > 1) index <- index[1] #NB! for now just arbitrarily select first value when multiple equal. This is built-in in the which.min function
  }
}

select_sig <- function(type, df, cutoff = 0.05){

  if(type == "most_sig"){
    df[which.min(df[,"p_value"]),] #select the observation with the lowest p-value. Returns FIRST match

  }else{ #closest below alpha

    df[find_closest(df[,"p_value"], cutoff = cutoff),] #select the observation closest below the cutoff (if any)
  }
}


#Expected hypothesis directions for each RRR (taken from papers)
#RRR1 [proportion correct] (treatment - control) [m-turk not included] [expected negative ES]
#RRR1 [proportion correct] (treatment - control) [m-turk not included] [expected negative ES]
#RRR3 [raw scores] intentionality (imperfective - perfect) [expected positive ES]
#RRR3 detailed processing =||=
#RRR3 attribution =||=
#RRR4 [cohens d] (treatment - control) [expected positiv ES (= more depletion treatment group)]
#RRR5 exit [raw scores] (high - low commitment) [expected negative ES]
#RRR5 neglect [expected negative ES]
#RRR6 [raw 10 point scale] (smile - pout) [expected positive ES]
#RRR7 [raw, mean % of max] (pressure - delay) [expected positive ES]
#RRR8 [% correct] (professor - hooligan) [expected positive ES]
#RRR9 hostility [raw scores] (80% hostile - 20% hostile) [expected positive ES]
#RRRR9 behavior [raw scores] (80%hostile - 20% hostile) [expected positive ES]
#RRR10 [raw scores = number of matrices solved] (commandments - books cheat) [expected negative ES]

hyp_direction_filter <- function(file_path, df, sig_level){ #this function filters effect sizes based on originally predicted direction

  if(grepl(pattern = "RRR3|RRR4|RRR6|RRR7|RRR8|RRR9", file_path)){ #these predicted a positive effect size, given treatment - control

    df_out <- df[df[, "effect_size"] > 0,] #keep only positive effect sizes


  }else{ #for RRR1, RRR2, RRR5, RRR10 (these predicted a negative effect size, given treatment - control)

    df_out <- df[df[, "effect_size"] < 0,] #keep only negative effect sizes

  }

  #If filtering results above results in no rows, keep only non-significant results in original df
  #since significant ones then would be in the "wrong" direction for the p-hacker (= intent to p-hack, but no publication bias)
  if(nrow(df_out) == 0) df_out <- df[df[, "p_value"] > sig_level,]


  df_out
}

#Sample 'multiverse_size' number of effect sizes, return index of effect sizes with smallest p-value
#samples 100 effect sizes at a time, unless fewer than this remains in the dataframe
bounded_multiverse_search <- function(multiverse_size, df){

  multiverse_searched <- if(multiverse_size < 100) multiverse_size else 100 #if multiverse size less than 100 sampling from it without replacement -> error
  bounded_multiverse <- sample(multiverse_size,
                               size = multiverse_searched,
                               replace = FALSE) #replace must = FALSE, otherwise we try the same choice-combination several times, which doesn't make sense

  which.min(df[bounded_multiverse,"p_value"]) #gives single index value

}

#Sample effect sizes, either most sig. in chunks, or randomly across full dataset
#outputs a vector of indices with the sampled rows
row_sampler <- function(dat, samples, sampling_type){
  #dat = dataframe, samples = e.g., 100, sampling_type = c("bounded_hack", "complete")


      max_n <- nrow(dat) #find out number of multiverses remaining in this lab after filtering

      if(sampling_type == "bounded_hack"){ #search in chunks of 100 unless fewer remain in dataframe

        replicate(samples, #number of times to re-run the below function
                  bounded_multiverse_search(max_n, dat), #
                  simplify = TRUE) #output is coerced from list into (in this case) a vector

      }else{ #full complete sampling (i.e., random sample)

        sample(max_n, #takes sample from 1:max_n
               samples, #number of samples might > max_n but this is not a problem since replace = TRUE
               replace = TRUE) #and the exact same combination of effect sizes across labs is unlikely

      }

  #output vector of row indices
}


#********************************************************************************
#[2] Functions to input a single file path----
#********************************************************************************

#Takes a file_path to a study-level multiverse dataframe as input
#outputs a corresponding dataframe filtered/sampled by arguments
sampler <- function(file_path, #applies to a single file_path
                    samples = 100,
                    n_equal_more_than = 24,  #filter, applies to both experimental groups
                    significance = FALSE, #B. If TRUE, sample random sig values
                    sig_level = 0.05,
                    hyp_direction = FALSE,
                    sampling_type = "complete"){ #complete or bounded_hack

  #read and filter by sample size
  dat <- read_filter(file_path, n_equal_more_than)

  if(nrow(dat) == 0){ #if lab contains no multiverse with ns larger than the set value

    warning_message <- get_warning_message(file_path, n_equal_more_than)
    warning(warning_message)
    return() #exit function, print warning and return NULL value

  }

  if(hyp_direction){ #If we wish to select only results in hypothesized direction
    dat <- hyp_direction_filter(file_path, dat, sig_level)
  }

  if(significance){ #If significance = TRUE, subsets only sig values.
    dat <- subset_sig_values(dat, sig_level) # If none, select lowest p-value
  }

  #sample rows (with replacement)
  sampled_rows <- row_sampler(dat, samples = samples, sampling_type = sampling_type)

  #select only sampled rows
  dat_sample <- dat[sampled_rows,]

  dat_sample[, c("effect_size", "var_es")] #out. Save only the vital information
}

#Takes a file_path to a single study-level multiverse as input
#outputs the most significant/with p-value closest to alpha effect size in the dataframe
#after filtering options
sig_selector <- function(file_path, #applies to a single file_path
                         n_equal_more_than = 24, #filter, applies to both experimental groups
                         type = "most_sig", #or "below_alpha"
                         hyp_direction = FALSE,
                         sig_level = 0.05){ #select only results in the originally hypothesized direction?

  #read and filter by sample size
  dat <- read_filter(file_path, n_equal_more_than) #nb, because of internal conversion to matrix RRR and lab vars become useless

  if(nrow(dat) == 0){ #if lab contains no multiverse with ns larger than the set value

    warning_message <- get_warning_message(file_path, n_equal_more_than)
    warning(warning_message)
    return() #exit function, print warning and return NULL value

  }

  if(hyp_direction){ #If we wish to select only results in hypothesized direction
    dat <- hyp_direction_filter(file_path, dat, sig_level)
  }

  #find rows with specified significant values
  select_sig(type, dat, cutoff = sig_level) #NB! There might be multiple equivalent obs. but first (least applied DF) is chosen

}






#********************************************************************************
#[3] Functions to clean up results from single path functions----
#********************************************************************************

#run: a <- lapply(files_intent, sampler)

#reorder output so that each col is values from across labs for a particular sample
order_by_draw <- function(result_list){

  result_list <- result_list[lengths(result_list) != 0] #When null lists, drop these. See sampler function for when a list might be NULL

  es <-  sapply(result_list, function(x) x[,"effect_size"]) #each row is an ES from a set of samples across labs
  var <- sapply(result_list, function(x) x[,"var_es"]) #each row is a variance from a set of samples across labs
  es <- as.data.frame(t(es)) #transpose to make into long format which is more easy to work with, i.e, row -> columns
  var <- as.data.frame(t(var)) #convert matrix to dataframe to be able to use Map easily. Each col is now values from across labs for a particular sample

  list(es = es, var = var) #out

}


#run: b <- order_by_draw(a) #each column is a set of values for meta-analysis



#For cleaning up the output from the sig_selector function (see sig_selector_list function)
combine_study_meta <- function(study, meta){

  study$ci_lb <- study$effect_size - 1.96*sqrt(study$var_es) #add vars with 95% CI
  study$ci_ub <- study$effect_size + 1.96*sqrt(study$var_es)
  study$se <- sqrt(study$var_es)
  study$type <- "study"
  meta$lab <- "Summary"
  meta$type <- "meta"

  #combine dataframes
  study_data <- study[,c("lab", "effect_size", "se", "ci_lb", "ci_ub", "p_value", "type")]
  names(study_data)[2] <- "rma_es"
  rbind(study_data, meta[,c("lab", "rma_es", "se", "ci_lb", "ci_ub","p_value", "type")])
}

#********************************************************************************
#[4] Functions to meta-analyze samples from single file path functions----
#********************************************************************************

#function to run meta-analysis across sampled observations across studies
rma_summarizer <- purrr::possibly(function(es, var){ #NB! because REML is an iterative method, it is possible it will not converge and throw an error
  #EDIT: across 1,400,00 runs, this happened 102 times.

  ma <- metafor::rma(yi = es, vi = var) #If this happens, we just drop that particular sample (i.e, return NULL; if error 'possibly' returns 'otherwise' value)
  tau <- confint(ma)$random[2,] #tau and CIs
  data.frame(rma_es = ma$b, se = ma$se, p_value = ma$pval,
             ci_lb = ma$ci.lb, ci_ub = ma$ci.ub,
             tau = tau[1], tau_lb = tau[2], tau_ub = tau[3])

},otherwise = NULL, quiet = FALSE) #part of possibly function. Quiet = FALSE mean an error message is returned if error thrown

#run:
#set.seed(13) #should be set before function for reproducibilility
#analyzed <- Map(rma_summarizer, b$es, b$var)
#analyzed <- do.call(rbind, analyzed)
#in "analyzed" each row is now a separate meta-analysis with summary statistics


#********************************************************************************
#Sidenote: some comments on Map
#mapply = Map but with *attempted* dataframe as output (did not work out for me).
#Transpose for "long" format of output. the mapply function passes two objects to
#a function and applies the function to 1st element of object 1 and 1st element of
#object 2 etc. In this case, because ES and Var are dataframes, and dataframes
#internally are lists where each column is a list-element the the function in
#mapply above takes as input first col 1 of ES and col 1 of var, then col 2 of ES
#and col 2 of var
#********************************************************************************

#function to apply the petpeese functions and clean up output
petpeese_summarizer <- function(es, var, file_path){

  pp <- PETPEESE.est(d = es, v = var, file_path = file_path) #

  #PetPEESE results: b0 (intercept) "is the estimate of true underlying effect that is uninfluence by
  #small-study effects" (Carter et al., 2019, p. 9)
  pp <- pp[pp$method == "PETPEESE" & pp$term == "b0",] #choose PETPEES and not PET or PEESE, and the relevant term
  names(pp)[c(3:4, 6:8)] <-  c("rma_es", "se", "p_value", "ci_lb", "ci_ub") #rename relevant variables

  pp[,c(3:4, 6:8)] #out

}

#3 parameter selection method (3PSM)
sel_method_summarizer <- purrr::possibly(function(es, var, file_path){
  #NB! weightr function sometimes gives error in solve.default(output_adj$hessian). No obvious solution, so we drop these since we need both CI and point-estimates.
  #On first check, out of 1e5 samples, 2% - 26.5% of results were excluded, with a mean across RRR03 - RRR10 of 14%.

  hyp_direction <- if(grepl(pattern = "RRR3|RRR4|RRR6|RRR7|RRR8|RRR9", file_path)) 0.025 else 0.975 #the grepl:ed predicted a positive effect size, given treatment - control, rest negative

  a <- weightr::weightfunct(effect = es, v = var, steps = c(hyp_direction, 1)) #hyp_direction steers direction of one-tailed test

  adjusted_values <- data.frame(rma_es = a$adj_est, se = a$adj_se, p_value = a$p_adj,
                                ci_lb = a$ci.lb_adj, ci_ub = a$ci.ub_adj)

  selection_method_estimates <- adjusted_values[2, ]
  names(selection_method_estimates)[c(1:2)] <- c("rma_es", "se") #for some reason not renamed when creating dataframe above

  selection_method_tau <- adjusted_values[1,]
  selection_method_tau[,-3] <- sqrt(selection_method_tau[,-3]) #take square root of everything except p-value
  names(selection_method_tau) <- c("tau", "tau_se", "tau_p", "tau_lb", "tau_ub")

  cbind(selection_method_estimates, selection_method_tau) #out

},otherwise = NULL, quiet = FALSE) #part of possibly function. Quiet = FALSE mean an error message is returned if error thrown

#function to apply puniform*
puni_star_summarizer <- function(es, var, file_path){ #puniform* [file_path should be a single file_path from a list for a meta-analysis]

  hyp_direction <- if(grepl(pattern = "RRR3|RRR4|RRR6|RRR7|RRR8|RRR9", file_path)) "right" else "left" #the grepl:ed predicted a positive effect size, given treatment - control

  a <- puniform::puni_star(yi = es, vi = var, side = hyp_direction)
  #NB! side depends on hypothesized direction and must be adjusted for each RRR

  data.frame(rma_es = a$est, p_value = a$pval.0, ci_lb = a$ci.lb, ci_ub = a$ci.ub,
             tau = sqrt(a$tau2), tau_lb = sqrt(a$tau2.lb), tau_ub = sqrt(a$tau2.ub))

}


#********************************************************************************
#[5] Functions to input lists of file_paths----
#********************************************************************************

#Rather than creating wrappers for the full file_paths we could also do
#lapply(list_of_folders, function(x) lapply(x, sampler))
#And then run clean-up functions afterwards rather than having them in the wrapper
#but this would make parallelization more difficult


#For exctracting either the most significant or below alpha results for a study
#applies to a vector of file paths to study dataframes for the same meta-analysis
sig_selector_list <- function(file_list, #Sig selector function but vectorized
                              n_equal_more_than = 24,
                              type = c("most_sig", "below_alpha"),
                              hyp_direction = FALSE,
                              sig_level = 0.05){
  type <- match.arg(type)

  a <- lapply(file_list, sig_selector,
              n_equal_more_than = n_equal_more_than,
              type = type,
              hyp_direction = hyp_direction,
              sig_level = sig_level)
  b <- do.call(rbind, a)
  meta <- rma_summarizer(b$effect_size, b$var_es)
  combine_study_meta(b, meta)

}

#For taking samples from the multiverse, either randomly or prioritizing sig results
#applies to a vector of file paths for study dataframes from the same meta-analysis
#Output are saved files with different meta-analytic techniques applied
sample_meta_save <- function(file_list,
                             n_equal_more_than = 24,
                             samples = 100,
                             significance = FALSE,
                             sig_level = 0.05,
                             hyp_direction = FALSE,
                             sampling_type = c("complete", "bounded_hack"),
                             p_bias_correction = TRUE,
                             save_folder){

  sampling_type <- match.arg(sampling_type)

  a <- lapply(file_list,
              sampler,
              n_equal_more_than = n_equal_more_than,
              samples = samples,
              significance = significance,
              sig_level = sig_level,
              hyp_direction = hyp_direction,
              sampling_type = sampling_type)

  b <- order_by_draw(a) #cleanup

  # Meta-analytic summaries
  rma_summary <- Map(rma_summarizer, b$es, b$var) # output is a list with rma results
  if (p_bias_correction) { #run publication bias methods if TRUE
      selection_method_3PSM <- Map(sel_method_summarizer, b$es, b$var, file_list[1])
      petpeese <- Map(petpeese_summarizer, b$es, b$var, file_list[1])
      p_uniform <- Map(puni_star_summarizer, b$es, b$var, file_list[1])

      meta_analyzed <- list(
          rma_summary = rma_summary,
          selection_method_3PSM = selection_method_3PSM,
          petpeese = petpeese,
          p_uniform = p_uniform
      )
  } else{ #if FALSE, no publication bias methods
      meta_analyzed <- list(
          rma_summary = rma_summary
      )
  }

  #convert into list of dataframes where each row is a draw from meta-analytic multiverse
  meta_analyzed <- lapply(meta_analyzed, function(x) do.call(rbind, x))

  #out is now just one file across all labs, optionally one file per MA if don't use do.call
  #The below lines are to get the RRR number and appendix input list to use for save-name
  split_1 <- unlist(strsplit(file_list[1], split = "/"))
  RRR_name <- split_1[4]
  split_2 <- unlist(strsplit(split_1[5], split = "_"))
  append <- split_2[6]
  append <- gsub(".RDS", "", append) #remove .rds so that either only "" or e.g., "behavior" (for RRR9)
  sig_indic <- if(significance) "_sig" else NULL #so that we can save "significance" samples separately
  bounded_indic <- if(sampling_type == "bounded_hack") "_bound" else NULL #so that we can save bounded samples separately

  #Create save-name and save as RDS file (native R-format to keep list-structure and keep file size down)
  save_name <- paste0(save_folder, RRR_name, sig_indic, bounded_indic,
                      "_MA_multiverse_samples_", append, "_", n_equal_more_than, ".RDS")
  save_name <- gsub("__", "_", save_name) #if no append (i.e, = "") will end up with two __ in a row
  saveRDS(meta_analyzed, save_name)

}
