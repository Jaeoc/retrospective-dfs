#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Functions for plots and tables
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)


#********************************************************************************

##Script content:----

#[1] Small internal helper functions
#[2] Functions to extract information from dataset
#**[2.1] Vectorized versions of the extractor functions
#[3] Summarizer functions
#**[3.1] Publication bias summarizers
#[4] Funnel plot function
#[5] Meta-analytic summary plot function (Figure 4)
#**[5.1] log OR plot
#**[5.2] smd plo
#**[5.3] Combined plot

#********************************************************************************
#[1] Small internal helper functions----
#********************************************************************************
##function to improve naming of RRRs by adding a zero (i.e., RRR1 -> RRR01)
length_with_zeroes <- function(num1, num2){ 
  formatC(num1:num2,
          width = 2, format = "d", flag = "0")
}

#Load the multiverse samples and separate by effect size type
load_and_rename <- function(file_list){
  
  files <- file_list[c(1, 3:14, 2)] #re-orders so RRR10 is last
  
  data <- lapply(files, readRDS)
  names(data) <- c("RRR01",
                   "RRR02", 
                   paste0("RRR03 ", c("Attribution", "Intention", "Process")),
                   "RRR04",
                   paste0("RRR05 ", c("Exit", "Neglect")),
                   paste0("RRR", length_with_zeroes(6, 8)),
                   paste0("RRR09 ", c("Behavior", "Hostility")),
                   "RRR10")
  
  data
}


#********************************************************************************
#[2] Functions to extract information from dataset----
#********************************************************************************

#Extract median N and multiverse size for a dataframe
extract_N <- function(file_path, n_equal_more_than){
  
  dat <- readRDS(file_path)
  
  #filter based on sample size
  dat <- dat[dat[,"n1"] >= n_equal_more_than & dat[,"n2"] >= n_equal_more_than,] 
  
  if(nrow(dat) == 0){ #if lab contains no multiverse with ns larger than the set value
    
    warning_message <- get_warning_message(file_path, n_equal_more_than)
    warning(warning_message)
    return() #exit function, print warning and return NULL value
    
  }
  
  quant <- quantile(dat$N)
  
  data.frame(lower_quartile = quant[["25%"]],
             median_n = quant[["50%"]],
             upper_quartile = quant[["75%"]],
             M_filtered = nrow(dat)) #multiverse after excluding small-N multiverses
}



#extract proportion significant results from a dataframe
extract_prop_sig <- function(file_path, n_equal_more_than, sig_level){
  
  dat <- readRDS(file_path) #sourced function
  
  #filter based on sample size
  dat <- dat[dat[,"n1"] >= n_equal_more_than & dat[,"n2"] >= n_equal_more_than,] 
  
  if(nrow(dat) == 0){ #if lab contains no multiverse with ns larger than the set value
    
    warning_message <- get_warning_message(file_path, n_equal_more_than)
    warning(warning_message)
    return() #exit function, print warning and return NULL value
    
  }
  
  all_sig <- mean(dat$p_value <= sig_level) #proportion below/equal to sig_level
  
  dat_directional <- hyp_direction_filter(file_path = file_path, 
                                          df = dat,
                                          sig_level = sig_level)
  
  hyp_sig <- sum(dat_directional$p_value <= sig_level) #number sig in hyp direction
  hyp_dir_sig <- hyp_sig / nrow(dat) #proportion of total obs. that was sig. in hypothesized direction
  
  data.frame(lab = dat$lab[1], all_sig = all_sig, hyp_dir_sig = hyp_dir_sig)
}



#extract variance in a dataframe multiverse before and after filtering on sample size
extract_multivar <- function(file_path, n_equal_more_than){
  
  dat <- readRDS(file_path) 
  
  multivar_full <- var(dat$effect_size, na.rm = TRUE)
  
  #filter based on sample size
  dat <- dat[dat[,"n1"] >= n_equal_more_than & dat[,"n2"] >= n_equal_more_than,] 
  
  if(nrow(dat) == 0){ #if lab contains no multiverse with ns larger than the set value
    
    warning_message <- get_warning_message(file_path, n_equal_more_than)
    warning(warning_message)
    return() #exit function, print warning and return NULL value
    
  } 
  
  multivar_filtered <- var(dat$effect_size, na.rm = TRUE)
  
  #get lab-name
  split_1 <- unlist(strsplit(file_path, split = "/"))
  split_2 <- unlist(strsplit(split_1[5], split = "_"))
  lab <- split_2[5]
  
  data.frame(lab, multivar_full, multivar_filtered)
  
}



#Helper for next function
internal_extract_df_sd <- function(dat){
  #dat = data.frame with RRR multiverse results
  
  dat_df <- dat[,-c(1:9)] #subset only DF columns
  
  dfs <- 1:ncol(dat_df) #seq_len of number of DFs 
  
  df_sd <- lapply(dfs, function(x){ #for each DF column
    #Get the rows where all except the current DF == 0
    df_rows <- dat[as.logical(rowSums(dat_df[,-x]) == 0),]
    #and then get the effect size sd for that DF when all other DFs == 0 
    sd(df_rows$effect_size)
  })
  #output is a list with sd for each DF
}

#extract standard deviation for each DF when all other DF are set to zero
extract_df_sd <- function(file_path, n_equal_more_than){
  dat <- readRDS(file_path) #load data
  
  df_sd_complete <- internal_extract_df_sd(dat)
  
  dat_df <- dat[,-c(1:9)] #subset only DF columns. Done inside of internal function, but convenient for printing later
  rrr_lab <- dat[1, 1:2] #These two lines must be run before the filtering, else creates errors
  
  #filter based on sample size
  dat <- dat[dat[,"n1"] >= n_equal_more_than & dat[,"n2"] >= n_equal_more_than,] 
  
  #NB! This is the one function that does not return NULL and warning if no multiverse
  #smaller thant n_equal_more_than. This is because we still want the df_sd_complete
  #under such circumstances. When that is the case the df_sd_filtered will be all NAs.
  
  df_sd_filtered <- internal_extract_df_sd(dat)

  #combine into a dataframe instead of list of DF results + add column with DF names
  df_sd_complete <- do.call(rbind, df_sd_complete)
  df_sd_filtered <- do.call(rbind, df_sd_filtered)
  dat_out <- data.frame(rrr_lab, #RRR and lab name
                        DF = names(dat_df), #DF identifier
                        sd_complete = df_sd_complete,
                        sd_filtered = df_sd_filtered,
                        row.names = NULL)
  
  
  #DF that were not applied always have the DF value zero, drop these
  applied_df <- colnames(dat_df)[colSums(dat_df) != 0] #get vector of only those DF which were applied
  dat_out[dat_out$DF %in% applied_df,] #keep only these DF in dataset
  
  #NB! sd_filtered may contain NAs for some values in output, because no zero observation exists there for these DF
  #If sd_filtered is all NAs, it means there was no multiverse size of sufficient size after filtering
}

#********************************************************
#**[2.1] Vectorized versions of the extractor functions----
#********************************************************
list_extract_N <- function(file_list, n_equal_more_than){
  
  a <- lapply(file_list,
              extract_N,
              n_equal_more_than = n_equal_more_than)
  
  do.call(rbind, a)
  
  
}

list_extract_prop_sig <- function(file_list, n_equal_more_than = 24, sig_level = 0.05){
  
  a <- lapply(file_list, extract_prop_sig,
              n_equal_more_than = n_equal_more_than,
              sig_level = sig_level)
  
  do.call(rbind, a)
  
}

list_extract_multivar <- function(file_list, n_equal_more_than = 24){
  
  a <- lapply(file_list, extract_multivar, n_equal_more_than = n_equal_more_than)
  
  do.call(rbind, a)
  
}

list_extract_df_sd <- function(file_list, n_equal_more_than){
  
  a <- lapply(file_list, extract_df_sd, n_equal_more_than)
  
  do.call(rbind, a)
  
}

#********************************************************************************
#[3] Summarizer functions----
#********************************************************************************

#A function to summarize the loaded data after filtering.
#Filtering = e.g., lapply(data_rest, function(x) x$rma_summary)
#takes a list of dataframes as input, outputs average point estimates and CIs
summarize_multiverse <- function(loaded_data){ 
  
  #Summarize by taking mean of point estimates + mean of upper/lower CIs estimates across draws
  results_rest <- lapply(loaded_data, colMeans, na.rm = TRUE) #na.rm because 3psm sometimes results in no SE estimate (but prints the rma_es)
  results_rest <- do.call(rbind, results_rest) #because colMeans results in a vector dplyr:bind_rows adds them in rwong order
  data.frame('meta_analysis' = row.names(results_rest), results_rest)
  
}

#Wrapper of two above functions, with additional filter so only use the standard meta-analytic results 
load_summarize_rma_multiverses <- function(file_list){
  
  data_rest <- load_and_rename(file_list)
  
  data_rma <- lapply(data_rest, function(x) x$rma_summary) #select the rma summary and ignore publication bias results
  
  summarize_multiverse(data_rma)
  
}

#A function to meta-analyze the original RRR results
meta_analyzer_original <- function(df){
  if(any(df[, "rp"] %in% c("RRR1", "RRR2"))){
    
    fit <- metafor::rma(measure = "OR", ai = outcome_t1, bi = outcome_t2, ci = outcome_c1, di = outcome_c2, 
                        n1i = ntreatment, n2i = ncontrol,  data = df)
  }else {
    
    fit <- metafor::rma(measure = "SMD", m1i = outcome_t1, m2i = outcome_c1, sd1i = outcome_t2, sd2i = outcome_c2,
                        n1i = ntreatment, n2i = ncontrol, data = df) 
  }
  
  hetero <- metafor::confint.rma.uni(fit)$random[2, ] #Gives us the tau estimates with confidence intervals
  data.frame(rma_es = fit$b[[1]], ci_lb = fit$ci.lb, ci_ub = fit$ci.ub, #effect size (point estimate) with confidence intervals
             tau = hetero[1], tau_lb = hetero[2], tau_ub = hetero[3]) #tau with confidence intervals
}

#********************************************************************************
#**[3.1] Publication bias summarizers----
#********************************************************************************

#For single results
pub_bias_summarizer <- function(x){ #internal functions sourced from 4.0_functions-meta-analytic-multiverse.r
  pp <-  petpeese_summarizer(es = x$rma_es,
                             var = x$se^2,
                             file_path = x$meta_analysis[1]) #"file_path" just needs to be a var that contains RRR number (e.g., RRR03) [to get direction of hypothesis]
  sm <- sel_method_summarizer(es = x$rma_es,
                              var = x$se^2,
                              file_path = x$meta_analysis[1])
  pu <- puni_star_summarizer(es = x$rma_es,
                             var = x$se^2,
                             file_path = x$meta_analysis[1])
  collected <- rbind(pp[c("rma_es", "p_value", "ci_lb", "ci_ub")], #the methods provide slightly different output
                     sm[c("rma_es", "p_value", "ci_lb", "ci_ub")], #keep only that which they have in common
                     pu[c("rma_es", "p_value", "ci_lb", "ci_ub")]) #[pp = no tau estimates, ps = no SE]
  collected$Correction <- c("PETPEESE", "3PSM", "Puniform*")
  collected
  
}

#For sampled results 
load_summarize_pub_bias_methods <- function(file_list){
  
  data <- load_and_rename(file_list) 
  
  sm <- lapply(data, function(x) x$selection_method_3PSM)
  sm <- summarize_multiverse(sm)
  sm$Correction <- "3PSM"
  
  pp <- lapply(data, function(x) x$petpeese)
  pp <- summarize_multiverse(pp)
  pp$Correction <- "PETPEESE"
  
  pu <- lapply(data, function(x) x$p_uniform)
  pu <- summarize_multiverse(pu)
  pu$Correction <- "Puniform*"
  
  rbind(pp[c("meta_analysis", "rma_es", "p_value", "ci_lb", "ci_ub", "Correction")], #the methods provide slightly different output
        sm[c("meta_analysis", "rma_es", "p_value", "ci_lb", "ci_ub", "Correction")], #keep only that which they have in common
        pu[c("meta_analysis", "rma_es", "p_value", "ci_lb", "ci_ub", "Correction")]) #[pp = no tau estimates, ps = no SE]
  
  
}

#********************************************************************************
#[4] Funnel plot functions----
#********************************************************************************
#A function to load a study level dataframe and filter it for plotting
read_memory_saving <- function(file_path, n_equal_or_more){
  dat <- readRDS(file_path)
  dat <- dat[dat[["n1"]] >= n_equal_or_more & dat[["n2"]] >= n_equal_or_more,] #filter on sample size
  dat <- dat[,c("lab", "effect_size", "var_es", "p_value", "N")] #only keep the variables used to minimize data.frame size
  dat[["p_value"]] <- ifelse(dat[["p_value"]] > 0.05, "Non-sig", "Sig") #categorical whether sig. at alpha = 0.5 [for improved plotting]
  dat
}

#funnel plot lines. 
#See also: https://stats.stackexchange.com/questions/5195/how-to-draw-funnel-plot-using-ggplot2-in-r

logor_critical <- function(max_se){ #funnel plot lines.
  se <- seq(from = 0, to = max_se + 0.01, by = 0.001) 
  critical_z <- qnorm(0.975) #critical z value for standard normal distribution
  lower <- 0 - critical_z*se #z = ES/SE
  upper <- 0 + critical_z*se
  data.frame(y_value = se, lower, upper)
}


d_critical <- function(min_N, max_N){ #Min_N/Max_N is the min/max sample sizes across labs.
  N <- seq.int(from = min_N, to = max_N + 10) #add a little extra on top for a nicer graph
  t_lower <- qt(0.025, df = N - 2) #t-distribution
  t_upper <- qt(0.975, df = N - 2)
  multiplier <- sqrt(N / (N/2)^2) #transform t to d assuming n1 = n2, see e.g., Cooper et al., p.228, table 12.1 row 2
  lower <- t_lower*multiplier #critical d-values
  upper <- t_upper*multiplier
  data.frame(y_value = N, lower, upper)
}


#Hexplot. Plot a funnel plot but with hexagons instead of points (necessary for for larger dataframes like RRR05 and RRR07)
funnel_hex <- function(data, title, funnel_line, x_label, y_label, annotation){ 
  
  ggplot() + 
    {if(grepl("01|02|03", title)) #These contain at least one lab with  very few unique values which makes geom_hex break down
      geom_bin2d(aes(x = effect_size, y = y_value), data = data) #hence plot them with square bins instead
      else 
        geom_hex(aes(x = effect_size, y = y_value), data = data)} + #this makes figures look a little blurry, so keep hexagonal bins whenever possible
    
    geom_line(aes(y = y_value, x = lower, linetype = "dashed"), data = funnel_line, alpha = 0.5) + #funnel lines
    geom_line(aes(y = y_value, x = upper, linetype = "dashed"), data = funnel_line, alpha = 0.5) +
    geom_vline(xintercept = 0, linetype = "dotted", alpha = 0.5) +
    {if(grepl("01|02", title))
      geom_label(data = annotation, aes(x = -Inf, y = 0 + max(data$y_value)/50, #RRr01 and RRr02 have inverted y-axes
                                        label = label), vjust = 1, hjust = 0, size = 2.8)
      else
    geom_label(data = annotation, aes(x = -Inf, y = max(data$y_value) + max(data$y_value)/50, #this is to get the label in almost upper left corner
                                      label = label), vjust = 1, hjust = 0, size = 2.8)} + 
    
    scale_fill_viridis_c(trans = "log", labels = round) + #color blind friendly color scale and on a log scale so not most values are just dark. rounding to make labels nicer than log-scale makes them
    scale_x_continuous(breaks = scales::pretty_breaks(n = 3)) + #set number of breaks but not values, scales is automatically imported
    theme_classic() +
    theme(panel.border = element_rect(fill = NA, colour = "black", size = 0.5, linetype = "solid")) +
    guides(linetype = FALSE,#remove legend for linetype (geom_line)
           color = guide_legend(override.aes = list(alpha = 1, #make color legend visible instead of having alpha = 0.05 as the points
                                                    shape = 15, #make the color legend symbols square instead of points
                                                    size = 4.5))) + #and increase their size
    xlab(x_label) +
    ylab(y_label) +
    ggtitle(title) + 
    facet_wrap(~lab)
  
}

#Wrapper function for above funnel plot functions.
funnel_plotter <- function(file_list, n_equal_or_more, plot_title, annotation){ 
  #where file_list is a list of file paths within a single RRR
  #n_equal_or_more = exclude multiverses with fewer participants (normally = 24)
  #plot_title = a character vector with the plot title
  
  # #read and combine into dataframe
  dat <- lapply(file_list, read_memory_saving, n_equal_or_more)
  dat <- do.call(rbind, dat) #can be replaced by data.table::rbindlist (fastest) or dplyr::bind_rows for less memory usage
  
  #Change plot y-axis and funnel lines depending of effect size type
  if(grepl("01|02", plot_title)){ #for log odds ratio effect types
    #create lines for funnel in plot
    max_se <- sqrt(max(dat[["var_es"]]))
    funnel_line <- logor_critical(max_se)
    
    #Prep labels for plotting
    dat$y_value <- sqrt(dat[["var_es"]])
    y_label <- "Standard Error"
    x_label <- "Log Odds Ratio"
    
    #create plot
    funnel_hex(data = dat, title = plot_title, funnel_line = funnel_line, x_label = x_label,
               y_label = y_label, annotation = annotation) +
      scale_y_reverse()
    
  }else{ #for smd
    #create lines for funnel in plot
    max_N <- max(dat[["N"]]) 
    min_N <- min(dat[["N"]])
    funnel_line <- d_critical(min_N, max_N)
    
    #Prep labels for plotting
    names(dat)[names(dat) == "N"] <- "y_value" #to make funnel_hex work for both logor and smd y-axis name needs to be consistent
    y_label = "N" 
    x_label <- "Standardized Mean Difference"
    
    #create plot
    funnel_hex(data = dat, title = plot_title, funnel_line = funnel_line, x_label = x_label,
               y_label = y_label, annotation = annotation)

  }
  
}

#********************************************************************************
#[5] Meta-analytic summary plot function (Figure 4)----
#********************************************************************************

#Large function that takes as input a list of summary data for each meta-analytic
#method for all meta-analyses (RRRs) and outputs the main meta-analytic plot
meta_analytic_plotter <- function(dat_list, name){
  
  ##Invert RRR1, RRR5, and RRR10 so that plotted in same direction as same as others
  ##despite hypothesis direction being opposite
  inverter <- function(object, values){ #function to invert values
    ifelse(dat_list[[object]][["meta_analysis"]] %in% c("RRR01", "RRR02", #if
                                                        "RRR05 Exit", "RRR05 Neglect",
                                                        "RRR10"),
           -dat_list[[object]][[values]], #then
           dat_list[[object]][[values]]) #else
  }
  
  #invert point estimates and CIs, as well as distribution
  dat_list$estimates$rma_es <- inverter("estimates", "rma_es")
  dat_list$estimates$ci_lb <- inverter("estimates", "ci_lb")
  dat_list$estimates$ci_ub <- inverter("estimates","ci_ub")
  dat_list$distribution$rma_es <- inverter("distribution", "rma_es")
  
  #Later in the plot we add an indicator (*) using annotate() so that 
  #it is clear in the plot that these RRRs have been inverted

                     
  ##Get max and min y-values in data to use to set boundaries of plot so that consistent across a_plot and b_plot
  ##Must be run after the inversion, because the inversion changes max/min CIs
  y_range <- range(c(dat_list$estimates$ci_lb, dat_list$estimates$ci_ub))
 
  #**[5.1] log OR plot----
  
  #select only effect with log odds ratio
  logor_plot <- lapply(dat_list, function(x) x[grepl("RRR01|RRR02", x$meta_analysis),])
  
  #Create vector indicating the minor lines between the factors, see https://stackoverflow.com/questions/16570026/create-minor-gridlines-in-ggplot2-for-categorical-data
  logor_sep_lines <- seq(from = 0.5, to = length(unique(logor_plot$estimates$meta_analysis)), by = 1)
  
  #factor the meta-analysis names [to be able to invert order to low-high]
  logor_plot$multiverse_sizes$meta_analysis <- factor(logor_plot$multiverse_sizes$meta_analysis) #for changing the direction of these in the plot
  
  ##Add manual jitter to the points
  #Create the jitter amount
  jitter_range <- (length(unique(logor_plot$estimates$Selection)) - 1)/2*0.12 #center on zero, steps of 0.12
  jitter <- seq(from = -jitter_range, to = jitter_range, by = 0.12) #jitter distance 0.12, center jitter on zero
  #convert factor to numeric to be able to add the jitter. Note that this is a different var than the factor above.
  logor_plot$estimates$meta_analysis <- as.numeric(factor(logor_plot$estimates$meta_analysis))
  
  #order the meta_analysis + selection so that they are in alphabetic order across meta_analysis
  logor_plot$estimates <- logor_plot$estimates[order(logor_plot$estimates$meta_analysis,
                                                     logor_plot$estimates$Selection),]
  #Add the jitter
  logor_plot$estimates$meta_analysis <- logor_plot$estimates$meta_analysis + jitter
  
  #By setting up the x-axis with another meta_analysis variable which is still a factor
  #rather than numeric (see annotate() below and logor_plot$multiverse_sizes$meta_analysis)
  #we can both manually jitter and then control the order of the x-variable 
  
  
  a_plot <- ggplot(data = logor_plot$estimates) +
    annotate("text", label = logor_plot$median_N$M_filtered_median , x  = logor_plot$median_N$meta_analysis, #add the post-filter multiverse size
             y = y_range[1] - 0.2, #set to min y in dataset, so that left-placed in plot
             hjust = 0, #left-adjust, center, or right-adjust text (0, 0.5, 1)
             vjust = -1, size = 2.8) +
    annotate("text", label = logor_plot$median_N$N, x  = logor_plot$median_N$meta_analysis, #add median N
             y = y_range[1] - 0.2, hjust = 0, vjust = 0.5, size = 2.8) +
    annotate("text", label = logor_plot$multiverse_sd$label, x  = logor_plot$median_N$meta_analysis, #add multiverse SD
             y = y_range[1] - 0.2, hjust = 0, vjust = 2, size = 2.8) +
    annotate("text", label = "*", x  = logor_plot$median_N$meta_analysis, #indicate that these have been inverted (*)
             y = y_range[2], hjust = 1, vjust = 0, size = 3.4) +
    geom_violin(data = logor_plot$distribution, aes(x = meta_analysis, y = rma_es),
                fill = "grey", alpha = 0.3, color = "grey") +
    geom_pointrange(aes(x = rev(meta_analysis),  #NB! flip the indicators so that the point-range estimates match after flipping rest of plot in scale_x_discrete below
                        y = rma_es, ymin = ci_lb, ymax = ci_ub,
                        shape = Selection, color = Selection), size = 0.3, alpha = 0.9) +  
    geom_hline(yintercept = 0, lty = 2, alpha = 0.6) +  # add a dotted line at x=0 after flip
    geom_vline(xintercept = logor_sep_lines, linetype = 2, color = "grey") + #add minor lines around MA names
    scale_x_discrete(limits = rev(levels(logor_plot$multiverse_sizes$meta_analysis))) + #change the order to low - high
    ylim(y_range[1] - 0.2, y_range[2]) + #set manually to be consistent across a_plot/b_plot for when combining
    scale_shape_manual(values = c(17, 16, 8, 15, 3, 7)) + #manual shape to be able to be consistent across plots
    scale_color_manual(values = c("#1B9E77", "#D95F02", "#E6AB02", "#7570B3", #manual color be be able to be consistent across plots
                                  "#E7298A", "#66A61E")) + #colors from RColorBrewer::display.brewer.pal(n = 8, name = 'Dark2') and RColorBrewer::brewer.pal(n = 8, name = "Dark2")
    xlab(NULL) + #remove because we will use the xlab from b_plot when combining the two
    ylab("Log odds ratio") +
    theme_classic() +
    #Hide (make white) color/shape legend because we will use the one fom b_plot when combining. If we remove the legend it makes alignment tricky, see: https://stackoverflow.com/questions/41569817/align-multiple-plots-in-ggplot2-when-some-have-legends-and-others-dont
    theme(legend.text = element_blank(), #remove the legend text
          legend.title = element_blank()) + 
    guides(color = guide_legend(override.aes = list(color = "white"))) + #make the legend shapes white
    coord_flip()  + # flip coordinates (puts labels on y axis)
    ggtitle(name)
  
  
  
  #**[5.2] smd plot----
  
  #selection smd effects
  smd_plot <- lapply(dat_list, function(x) x[!grepl("RRR01|RRR02", x$meta_analysis),])
  
  #Create vector indicating the minor lines between the factors, see https://stackoverflow.com/questions/16570026/create-minor-gridlines-in-ggplot2-for-categorical-data
  smd_sep_lines <- seq(from = 0.5, to = length(unique(smd_plot$estimates$meta_analysis)), by = 1)
  
  #factor the meta-analysis names [to be able to invert order to low-high]
  smd_plot$multiverse_sizes$meta_analysis <- factor(smd_plot$multiverse_sizes$meta_analysis) #for changing the direction of these in the plot
  
  ##Add manual jitter to the points
  #convert factor to numeric to be able to add the jitter
  smd_plot$estimates$meta_analysis <- as.numeric(factor(smd_plot$estimates$meta_analysis))
  #order the meta_analysis + selection so that they are in alphabetic order across meta_analysis
  smd_plot$estimates <- smd_plot$estimates[order(smd_plot$estimates$meta_analysis,
                                                 smd_plot$estimates$Selection),]
  #Add the jitter
  smd_plot$estimates$meta_analysis <- smd_plot$estimates$meta_analysis + jitter #jitter var created above a_plot
  
  #smd stars to indicate which meta-analyses have been inverted
  smd_stars <- data.frame(meta_analysis = unique(smd_plot$multiverse_sizes$meta_analysis),
                          stars = c(rep("", 4),
                                    rep("*", 2),
                                    rep("", 5),
                                    "*"))
    
  
  b_plot <- ggplot(data = smd_plot$estimates) +
    annotate("text", label = smd_plot$median_N$M_filtered_median, x  = smd_plot$median_N$meta_analysis,
             y = y_range[1] - 0.2, hjust = 0, vjust = -1, size = 2.8) + #add multiverse size after filtering
    annotate("text", label = smd_plot$median_N$N, x  = smd_plot$median_N$meta_analysis, #add median N
             y = y_range[1] - 0.2, hjust = 0, vjust = 0.5, size = 2.8) +
    annotate("text", label = smd_plot$multiverse_sd$label, x  = smd_plot$median_N$meta_analysis, #add multiverse SD
             y = y_range[1] - 0.2, hjust = 0, vjust = 2, size = 2.8) +
    annotate("text", label = smd_stars$stars, x  = smd_stars$meta_analysis, #indicate that these have been inverted (*)
             y = y_range[2], hjust = 1, vjust = 0, size = 3.4) +
    geom_violin(data = smd_plot$distribution, aes(x = meta_analysis, y = rma_es), 
                fill = "grey", alpha = 0.3, color = "grey") +
    geom_pointrange(aes(x = rev(meta_analysis), ##NB! flip the indicators so that the printed estimates match after flipping rest of plot in scale_x_discrete below
                        y = rma_es, ymin = ci_lb, ymax = ci_ub,
                        shape = Selection, color = Selection), size = 0.3, alpha = 0.9) + 
    geom_hline(yintercept = 0, lty = 2, alpha = 0.6) +  # add a dotted line at x=0 after flip
    geom_vline(xintercept = smd_sep_lines, linetype = 2, color = "grey") + #add minor lines around MA names
    scale_x_discrete(limits = rev(levels(smd_plot$multiverse_sizes$meta_analysis))) + #change the order to low - high
    scale_shape_manual(values = c(17, 16, 8, 15, 3, 7)) + #manual shape to be able to be consistent across plots
    scale_color_manual(values = c("#1B9E77", "#D95F02", "#E6AB02", "#7570B3", #manual color be be able to be consistent across plots
                                  "#E7298A", "#66A61E")) + #colors from RColorBrewer::display.brewer.pal(n = 8, name = 'Dark2') and RColorBrewer::brewer.pal(n = 8, name = "Dark2")
    ylim(y_range[1] - 0.2, y_range[2]) + #set same xlim as for a_plot ["ylim" because of coord_flip]
    xlab("Meta-analysis") +
    ylab("Standardized Mean Difference") +
    labs(shape = "Selection mechanism", color = "Selection mechanism") +
    theme_classic() + #must be before theme(), otherwise resets it
    theme(legend.justification = "top") + #move legend to top right to work better after combining plots
    coord_flip() # flip coordinates (puts labels on y axis)
  
  
  
  
  
  #**[5.3] Combined plot----
  cowplot::plot_grid(a_plot, b_plot,
                     ncol = 1,
                     rel_heights = c(1, 3.4), #Relative size of plots to each other
                     align = "v") #align them vertically
  
  #Couldn't figure out how to move the y-axis title up. Consider removing it
  #and re-adding on top just using cowplot
  #Flat violin plot if desired (from: https://stackoverflow.com/questions/52034747/plot-only-one-side-half-of-the-violin-plot?noredirect=1&lq=1)
  
}

