#*******************************************************************************

## Project: Meta-multiverse
## Script purpose: Drop PCA DF (S2. c) and compare results. This is the one DF
# that maximizes variance. These results are for Supplement S5.
## Code: Anton Olsson Collentine (anton@olssoncollentine.com)


#*******************************************************************************
# Setup
#*******************************************************************************

source("4.0_functions-meta-analytic-multiverse.r") #source data summary functions

files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")

# Drop S2 c
drop_s2c <- function(file_path){
    #read and drop
    original <- readRDS(file_path)
    dropped <- original[original$s2 < 2, ] # s2 == 2 corresponds to the DF S2c (PCA)

    # Save file with dropped S2c for later analysis and meta-analysis
    split_name <- unlist(strsplit(file_path, split = "/")) # get file + (RRR) folder name

    save_folder <- "../data/08.1_supplement-F-study-cleaned/"
    subfolder <- split_name[4]
    save_folder <- paste0(save_folder, subfolder, "/")
    dir.create(save_folder, showWarnings = FALSE) #create folder if non-existant

    file_name <- split_name[5]
    save_name <- paste0(save_folder, file_name)
    saveRDS(object = dropped, file = save_name)
}

# usage example
# file_path <- "../data/05_study_multiverses_cleaned/RRR6/RRR6_multiverse_w_sum_L05_.RDS"
# drop_s2c(file_path)


# apply to the above files
lapply(files, function(x) lapply(x, drop_s2c))
# Explanation: first apply to object in the top-level list (files2), and apply
# the function to each object (RRR) within these to top-level lists

# Note that S2.c only applied to the following RRRs: rrr3, rrr5, rrr6, rrr8,
#rrr9 (see supplements/S1_DF-per-RRR.pdf).
#We nonetheless run the above across all datasets, because we otherwise could not have used the `files_by_meta_analysis`` function in the next section, and being able to do so saved us some time

#*******************************************************************************
# Meta-analytic part
#*******************************************************************************
#first, must create the meta-analytic data

if(!require(metafor)){install.packages("metafor")} #must be installed to use rma_summarizer function
if(!require(purrr)){install.packages("purrr")} #must be installed to use rma_summarizer function (for 'possibly' function)
#These packages are are loaded internally by eg., metafor:: to not load full packages

library(parallel) #comes pre-installed with R

source("4.0_functions-meta-analytic-multiverse.r")


set.seed(1334) #for sampler

# Load list of study-level files and sort them
files <- files_by_meta_analysis(directory = "../data/08.1_supplement-F-study-cleaned")

# S2.c only applied to the following RRRs: rrr3, rrr5, rrr6, rrr8, rrr9
# (see supplements/S1_DF-per-RRR.pdf)
# So we will only meta-analyze these

files <- files[c(
    "RRR03 Intention", "RRR03 Attribution", "RRR03 Process", "RRR05 Neglect", "RRR05 Exit", "RRR06", "RRR08", "RRR09 Behavior",
    "RRR09 Hostility"
)]

## Here I run the same code as in code/4.2_create-meta-analytic-multiveres.r
# section [1] Random draws, with the difference that I have set
#p_bias_correction = FALSE since the publication bias methods are not of
#interest for this sensitivity analysis.

start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment

parallel::parLapply(
    cl = cl, # cluster
    files, # list-object we do parallel loop over
    sample_meta_save, # function used
    samples = 1e5, # number of samples, by default 100
    n_equal_more_than = 24, # exclude when less than N in each experimental group
    significance = FALSE, # If possible, do only draws amongst significant results
    sig_level = 0.05, # using this significance level
    hyp_direction = FALSE, # Filter results on whether they were in the hypothesized direction
    sampling_type = "complete", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
    p_bias_correction = FALSE, #do not run
    save_folder = "../data/08.2_supplement-F-meta-samples/")

stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

# Biased sampling---


#Run 1 - Random significant (sampling_type = "complete")
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment

parallel::parLapply(cl = cl, #cluster
                    files, #list-object we do parallel loop over
                    sample_meta_save, #function used
                    samples = 1e5, #number of samples, by default 100
                    n_equal_more_than = 24, #exclude when less than N in each experimental group
                    significance = TRUE, #If possible, do only draws amongst significant results
                    sig_level = 0.05, #using this significance level
                    hyp_direction = TRUE, #Filter results on whether they were in the hypothesized direction
                    sampling_type = "complete", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
                    p_bias_correction = FALSE,
                    save_folder = "../data/08.2_supplement-F-meta-samples/")

stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#Run 2 - Bounded significant (sampling_type = "bounded_hack")
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment

parallel::parLapply(cl = cl, #cluster
                    files, #list-object we do parallel loop over
                    sample_meta_save, #function used
                    samples = 1e5, #number of samples, by default 100
                    n_equal_more_than = 24, #exclude when less than N in each experimental group
                    significance = TRUE, #If possible, do only draws amongst significant results
                    sig_level = 0.05, #using this significance level
                    hyp_direction = TRUE, #Filter results on whether they were in the hypothesized direction
                    sampling_type = "bounded_hack", # or "bounded_hack" for sampling in chunks of 100 and selecting lowest p-value each iteration
                    p_bias_correction = FALSE,
                    save_folder = "../data/08.2_supplement-F-meta-samples/")

stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#*******************************************************************************
# Create plots and data for supplement
#*******************************************************************************
source("4.0_functions-meta-analytic-multiverse.r")

files <- files_by_meta_analysis(directory = "../data/08.1_supplement-F-study-cleaned")

uses_pca <- c( #only these meta-analyses apply the S2.c option (PCA)
    "RRR03 Intention", "RRR03 Attribution", "RRR03 Process", "RRR05 Neglect", "RRR05 Exit", "RRR06", "RRR08", "RRR09 Behavior",
    "RRR09 Hostility"
)
files <- files[uses_pca] #select only affected RRRs

source("5.0_functions-plots-and-tables.r")


#es_sd <- lapply(files, list_extract_df_sd, n_equal_more_than = 24)
#saveRDS(es_sd, "../data/08.3_supplement-F-intermediate-files/es_sd_no_PCA.RDS") #takes a bit of time, so create intermediate dataset

es_sd <- readRDS("../data/08.3_supplement-F-intermediate-files/es_sd_no_PCA.RDS")

es_sd_original <- readRDS("../data/es_sd.RDS")
es_sd_original <- es_sd_original[uses_pca]

es_split <- list(
    "Without PCA" = es_sd,
    "With PCA" = es_sd_original
)


#Function copied from 5.2_plots.r
median_per_DF <- function(es_type){
  a <- split(es_type, es_type$DF)
  b <- lapply(a, function(x){
    data.frame(DF = x[1, "DF"],
               median_sd_complete = median(x$sd_complete),
               median_sd_filtered = median(x$sd_filtered, na.rm = TRUE), #na.rm necessary because filtering in some labs (RRR8) -> NA because no DF choice variation left
               max_DF_sd_complete = max(x$sd_complete), #used for ordering for individual RRR plots
               max_DF_sd_filtered = max(x$sd_filtered, na.rm = TRUE)) #used for ordering
  })
  out <- do.call(rbind, b)

  #because DF is a factor, splitting by it creates Na results where an RRR lacks a DF, get rid of these
  #This applies only to the use of this function in section 2)
  out[!is.na(out$median_sd_complete),]
}

# Do some prep of data

es_split <- lapply(es_split, function(x) {
   x <- dplyr::bind_rows(x, .id = "meta_analysis")
   x$DF <- gsub("u.*", "u", x$DF) #gives multiple rows of "u" with different values
   x$DF <- gsub("e4_1|e4_2", "e4", x$DF) # we will average across these
   es_medians <- median_per_DF(x)
   merge(x, es_medians)
})

# Here load the original es_sd for comparisons of values:
# 1) change in median importance of S2 before/after
# 2) Check figure 4 for SMDs (plot not necessary, just data)
# 3) median UMV across labs in an RRR + IQR (total)
# 4) Check values for meta-analysis for these RRRs
#Possibly this can all be in a table except for 3


# Plot here

es_split <- lapply(es_split, function(x){ #order by median sd and max
 x$median_sd_filtered <- ifelse(x$median_sd_filtered < 1e-14, 0, x$median_sd_filtered) #this is because s2 is rounded to something like 1e-16 and the order then becomes strange in the next step
 x <- x[order(x$median_sd_filtered, x$max_DF_sd_filtered),]
 x$order <- as.numeric(factor(x$DF, levels = unique(x$DF))) #create order variable per effect type
 x
})

#add the values of the logor to the smd order variable to make them all appear in descending order
es_split$`With PCA`$order <-
  es_split$`With PCA`$order + length(unique(es_split$`Without PCA`$order))

#Recombine to be able to do a facet plot   (warnings can be ignoreD)
es_sd_all <- dplyr::bind_rows(es_split, .id = "effect_type")

#Factor order variable
es_sd_all$order <- factor(es_sd_all$order)

#Rename to be able to change order in plot, not have extra DF showing up on axis, and nice output
es_sd_all$DF <- factor(es_sd_all$DF,
                       levels = c("e1", "e2", "e3", "e4", "e5", "e6", "s1", "s2", "u"),
                       labels = c("Missingness DV (E1)",
                                  "Miss. age/demog. (E2)",
                                  "Age (E3)",
                                  "Demographics (E4)",
                                  "Attention check (E5)",
                                  "Univariate outliers (E6)",
                                  "Scale length (S1)",
                                  "Composite score (S2)",
                                  "Unique (U)"))
# plot for >=24 participants
# Warnings can be ignored: due to some DF not resulting in any results for some labs
library(ggplot2)

ggplot(data = es_sd_all, aes(x = order, y = sd_filtered)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(width = 0.2, alpha = 0.1) +
  geom_hline(yintercept = 0, lty = 2, alpha = 0.6) +
  scale_x_discrete(name = "DF",
                   labels =  setNames(as.character(es_sd_all$DF),
                                      as.numeric(es_sd_all$order))) +
  scale_y_continuous(name = "Standard Deviation") +
  coord_flip() +
  theme_classic() +
  facet_wrap(~effect_type, scales = "free", ncol = 1, drop = TRUE)

# ggsave("../figures/supplement/sd_without-pca.png", dpi = 600, width = 6, height = 6*1.2)



#As the study gets less crystallized in measurement, the uncertainty goes up and p-hacking is easier.
#Leave in and discuss it, when outcomes not correlated, the multiverse variance goes up.

#*******************************************
# Extract UMV for the 5 RRRs without PCA
#*******************************************
# see .Rmd file
