#**********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Load data from cohort project to check age groups
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)


#**********************************************************************************
#Packages and data
#**********************************************************************************
#Data not allowed to be uploaded. Available from:
#Wicherts, Dr J.M. (Tilburg University - Tilburg School of Social and Behavioural Sciences)
#(2010): Cohort Differences in Big Five Personality Factors Over a Period of 25 Years. DANS.
#https://doi.org/10.17026/dans-z5p-zedj 

if(!require("ggplot2")){install.packages("ggplot2")}
if(!require("dplyr")){install.packages("dplyr")}

library(ggplot2) #for plotting
library(dplyr) #for data wrangling
#**********************************************************************************

dat <- read.csv("../Dans data-do-not-upload/data5pft19822007.csv", stringsAsFactors = FALSE)

dat %>% select(AGE) %>% #shows the distribution. Everyone between 18 - 25 (inclusive)
  ggplot(aes(x = AGE)) +
  geom_histogram(binwidth = 1) +
  theme_classic()

sorted_age <- sort(dat$AGE)
quantile(sorted_age, c(.8, .9, .95))

age <- dat$AGE
sum(age < 25) / length(age) #96.6%
sum(age < 24) / length(age) #92.7%
sum(age < 23) / length(age) #86.6%
sum(age < 22) / length(age) #77.8%




