#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Extract in-text values
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)

#********************************************************************************

##Script content:----

#[0] Packages and sourced functions
#[1] Introduction section
#[2] Results section
#**[2.1] Figure 2 paragraphs
#***[2.1.1] variability within first 16/24 RRR04 labs
#***[2.1.2] correlation N - multiverse SD 16/24 RRR04 labs
#***[2.1.3] Labs with most significant RRR04 first 16 labs
#***[2.1.4] max and median effect size range within first 16/24 RRR04 labs
#***[2.1.5] variability within labs across all RRRs
#***[2.1.6] max and median effect size ranges in a lab across all RRRs
#***[2.1.7] Labs with most significant RRR04 first 16 labs
#***[2.1.8] N significant for each meta-analysis (Table 4)
#***[2.1.9] Correlation multiverse size and percent labs with sig  (Table 4 paragraph)
#***[2.1.10] % labs across RRRs with any significant results (Figure 5 paragraphs)
#**[2.2] Figure 4 paragraph
#***[2.2.1] median E3 variable upper and lower panel Figure 4
#***[2.2.2]Median composite score Figure 3 lower panel
#***[2.2.3]Median SD unique DF Figure 3 lower panel
#***[2.2.4]Largest SD Figure 3
#**[2.2.5] Median SD RRR07 u2
#**[2.3]Figure 5 paragraphs
#***[2.3.1] Variability meta-analytic multiverses
#***[2.3.2]overestimate range
#***[2.3.3]correlation p-hack range and multiverse size
#***[2.3.4]RRR07 random draw vs. most significant
#[3] Save as object

#********************************************************************************
#[0] Sourced functions----
#********************************************************************************

source("4.0_functions-meta-analytic-multiverse.r") #for files_by_meta-analysis
source("5.0_functions-plots-and-tables.r") #for read_memory_saving function

#********************************************************************************
#[1] Introduction section ----
#********************************************************************************
files <- list.files(path = "../data/03_cleaned-raw-data", pattern = "rrr", full.names = TRUE)
raw_data <- lapply(files, read.csv, stringsAsFactors = FALSE)
names(raw_data) <- c("RRR01", "RRR10", paste0("RRR0", 2:9)) #name and fix order
raw_data <- c(raw_data[-2], raw_data[2]) #since rrr10 loads after rrr1

#Get number of participants before any exclusions
participants <- sapply(raw_data, nrow)
total_participants <- Reduce('+', participants) #37,602 participants total

#Get number of labs
n_labs <- lapply(raw_data, function(x) length(unique(x$lab))) #get number of labs per RRR
n_labs <- unlist(n_labs)
total_labs <- sum(n_labs) #236 labs

#Multiverse sizes
plot_data <- readRDS("../data/meta_plot_data_n24.RDS")

multiverse <- plot_data$multiverse_sizes$N
multiverse <- as.numeric(gsub("[[:alpha:]]|[[:punct:]]",  "" , multiverse))
#gsub exchanges any letters (alpha) or non-letters/digits (punct), with nothing ("")
#see https://en.wikibooks.org/wiki/Regular_Expressions/POSIX_Basic_Regular_Expressions

range(multiverse) #from 3,840 to 2,621,440


#********************************************************************************
#[2] Results section ----
#********************************************************************************

files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")

#********************************************************************************
#*[2.1] Figure 2 paragraphs----
#********************************************************************************
#***[2.1.1] variability within first 16/24 RRR04 labs----

rrr4_cut <- lapply(files$RRR04[1:16], read_memory_saving, n_equal_or_more = 24)

sd_rrr4 <- lapply(rrr4_cut, function(x){
  data.frame(lab = x[["lab"]][1],
             lab_sd = sd(x[["effect_size"]]),
             lab_n = x[["N"]][1]) #original sample size before exclusions
})

sd_rrr4 <- do.call(rbind, sd_rrr4)

#output
rrr4_sd_quantile <- quantile(sd_rrr4$lab_sd)

#***[2.1.2] correlation N - multiverse SD 16/24 RRR04 labs----

#output
rrr4_corr <- cor(sd_rrr4$lab_n, sd_rrr4$lab_sd) #sd_rrr4 object created in section above

#***[2.1.3] Labs with most significant RRR04 first 16 labs----

#See section 2.1.8

#***[2.1.4] max and median effect size range within first 16/24 RRR04 labs----

range_rrr4 <- lapply(rrr4_cut, function(x){ #extract range in Cohen's d
  d <- max(x[["effect_size"]]) - min(x[["effect_size"]])
  data.frame(lab = x[["lab"]][1], d)
})

range_rrr4 <- do.call(rbind, range_rrr4)

#output objects
max_range_rrr4 <- range_rrr4[which.max(range_rrr4$d),]
median_range_rrr4 <- median(range_rrr4$d)


#***[2.1.5] variability within labs across all RRRs----
rrr_sd <- lapply(files, function(file_paths){

  a <- lapply(file_paths, function(x){ #get ES SD for each lab in an RRR (list)
    lab_data <- read_memory_saving(x, n_equal_or_more = 24)
    if(nrow(lab_data) == 0){ #if lab contains no multiverse with ns larger than the set value

      warning_message <- get_warning_message(x, "")
      warning(warning_message)
      return() #print warning and return NULL value for that lab
      #with sample size 24, expect warnings for RRR8 L24; RRR2 L09, L17, L26
    }

    data.frame(lab = lab_data$lab[1], lab_sd = sd(lab_data$effect_size),
               lab_n = lab_data$N[1])
  })

  b <- do.call(rbind, a)

})

logor_sd <- do.call(rbind, rrr_sd[1:2])
smd_sd <- do.call(rbind, rrr_sd[-c(1:2)])

#output objects
logor_sd_quantile <- quantile(logor_sd$lab_sd)
smd_sd_quantile <- quantile(smd_sd$lab_sd) #output object

#***[2.1.6] correlation N - multiverse SD within all RRRs----

rrr_corr <- lapply(rrr_sd, function(x) cor(x$lab_n, x$lab_sd)) #rrr_sd from section above
meta_analysis <- names(rrr_corr)
rrr_corr <- do.call(rbind, rrr_corr)

#output
rrr_corr <- data.frame(meta_analysis = meta_analysis, corr = rrr_corr, row.names = NULL)

#***[2.1.7] max and median effect size ranges in a lab across all RRRs----
rrr_ranges <- lapply(files, function(file_paths){

  a <- lapply(file_paths, function(x){ #get ES range for each lab in an RRR (list)
    lab_data <- read_memory_saving(x, n_equal_or_more = 24)
    if(nrow(lab_data) == 0){ #if lab contains no multiverse with ns larger than the set value

      warning_message <- get_warning_message(x, "")
      warning(warning_message)
      return() #print warning and return NULL value for that lab
      #with sample size 24, expect warnings for RRR8 L24; RRR2 L09, L17, L26
    }

    es_range <- max(lab_data$effect_size) - min(lab_data$effect_size)
    data.frame(lab = lab_data$lab[1], es_range)
  })

  b <- do.call(rbind, a)

})


#median range in labs across rrrs
logor_ranges <- do.call(rbind, rrr_ranges[1:2])
smd_ranges <- do.call(rbind, rrr_ranges[-c(1:2)])

#output objects
logor_quantile_range <- quantile(logor_ranges$es_range)
smd_quantile_range <- quantile(smd_ranges$es_range) #output object

#Max range
rrr_max_ranges <- lapply(rrr_ranges, function(x){
  x[which.max(x$es_range),] #lab + range of lab with largest ES range
})

rrr_max_ranges <- do.call(rbind, rrr_max_ranges)

logor_max_range <- rrr_max_ranges[which.max(rrr_max_ranges[1:2, "es_range"]),] #two first are RRR1 and RRR2
smd_max_ranges <- rrr_max_ranges[-c(1:2),] #get max amongst remaining
smd_max_range <- smd_max_ranges[which.max(smd_max_ranges[, "es_range"]),]

#output objects
logor_max_range <- data.frame(RRR = row.names(logor_max_range), logor_max_range)
smd_max_range <- data.frame(RRR= row.names(smd_max_range), smd_max_range)

#***[2.1.8] N significant for each meta-analysis (Table 4)----

all_prop_sig <- lapply(files, list_extract_prop_sig, n_equal_more_than = 24, sig_level = 0.05)
#function above from 5.0_functions_plots_and_tables.r

#******************
#Side-note: Labs with most significant in RRR04 first 16 labs (section 2.1.3)
rrr4_sig_prop <- all_prop_sig$RRR04[1:16,]

#output object
rrr4_sig_prop_descending <- rrr4_sig_prop[order(rrr4_sig_prop$all_sig, decreasing = TRUE),]
#******************

#N significant for each meta-analysis (Table 4)
count_n_labs_with_sig <- function(x){
    #x = dataframe with lab name and proportion significant in each lab, with or without hyp_direction_filter

    n_labs <- nrow(x)
    n_labs_any_sig <- sum(x[["all_sig"]] > 0)
    n_labs_hyp_sig <- sum(x[["hyp_dir_sig"]] > 0)

    #out
    data.frame(n_labs, n_labs_any_sig, n_labs_hyp_sig)

}
n_labs_with_sig <- lapply(all_prop_sig, count_n_labs_with_sig)

#output
n_labs_with_sig <- dplyr::bind_rows(n_labs_with_sig, .id = "meta_analysis")

#***[2.1.9] Correlation multiverse size and percent labs with sig  (Table 4 paragraph)----
plot_data <- readRDS("../data/meta_plot_data_n24.RDS")
median_N <- plot_data$median_N
#Remove some things to turn numeric again
M_filtered_median <-  gsub("M: ", "", median_N$M_filtered_median)
M_filtered_median <- as.numeric(gsub(",", "", M_filtered_median))

sig <- n_labs_with_sig$n_labs_hyp_sig / n_labs_with_sig$n_labs

rel <- cor.test(M_filtered_median, sig)

#output
cor_multiverse_hyp_sig <- data.frame(cor = rel$estimate, lower_CI = rel$conf.int[1], upper_CI = rel$conf.int[2])

#***[2.1.10] % labs across RRRs with any significant results (Figure 5 paragraphs)----
all_prop_sig_disagg <- dplyr::bind_rows(all_prop_sig, .id = "RRR")
n_labs_total_multiple_dvs <- nrow(all_prop_sig_disagg)
n_labs_no_sig_multiple_dvs <- sum(all_prop_sig_disagg$all_sig == 0)
n_labs_no_hyp_sig_multiple_dvs <- sum(all_prop_sig_disagg$hyp_dir_sig == 0)

#output object (n tested effects with any (hypothesized direction) significant results)
overall_prop_sig <- data.frame(n_labs_total_multiple_dvs,
                               n_labs_no_sig_multiple_dvs,
                               n_labs_no_hyp_sig_multiple_dvs)
#NB, this counts labs that measure multiple DVs multiple times (e.g., RRR3)

#NB! this is after excluding labs with small sample sizes (<24 /group) (RRR2 = 3 labs, RRR8 = 1)

#********************************************************************************
#*[2.2] Figure 4 paragraph ----
#********************************************************************************

#***[2.2.1] median E3 variable upper and lower panel Figure 4----
es_sd <- readRDS("../data/es_sd.RDS")

es_sd_all <- dplyr::bind_rows(es_sd, .id = "meta_analysis") #(warnings can be ignoreD)

#Compbine all unique into one category and all e4 into one category
es_sd_all$DF <- gsub("u.*", "u", es_sd_all$DF)
es_sd_all$DF <- gsub("e4_1|e4_2", "e4", es_sd_all$DF) #we will average across these

#Separate between log odds ratios and SMD effect types
es_sd_smd <- es_sd_all[!es_sd_all$meta_analysis %in% c("RRR01", "RRR02"),]
es_sd_logor <- es_sd_all[es_sd_all$meta_analysis %in% c("RRR01", "RRR02"),]

es_split <- list(logor = es_sd_logor,
                 smd = es_sd_smd)

#Create function to compute median ES across DF in labs
#disaggregated across RRRs
median_per_DF <- function(es_type){
  a <- split(es_type, es_type$DF)
  b <- lapply(a, function(x){
    data.frame(DF = x[1, "DF"],
               median_sd_complete = median(x$sd_complete),
               median_sd_filtered = median(x$sd_filtered, na.rm = TRUE),
               max_DF_sd_filtered = max(x$sd_filtered, na.rm = TRUE)) #na.rm necessary because filtering in some labs (RRR8) -> NA because no DF choice variation left
  })
  do.call(rbind, b)
}

es_split <- lapply(es_split, function(x){
  es_medians <- median_per_DF(x) #compute medians for each DF
  merge(x, es_medians) #merge with full SD dataset
})

##Es_split is the data used for plotting Figure 3
logor_e3 <- es_split$logor[es_split$logor$DF == "e3",]
smd_e3 <- es_split$smd[es_split$smd$DF == "e3",]

#output object
logor_e3_median <- logor_e3$median_sd_filtered[1]
smd_e3_median <- smd_e3$median_sd_filtered[1]

#***[2.2.2] Median composite score Figure 4 lower panel----
smd_s2 <- es_split$smd[es_split$smd$DF == "s2",]

#output object
smd_s2_median <- smd_s2$median_sd_filtered[1]

#***[2.2.3] Median SD unique DF Figure 4 lower panel----
smd_u <- es_split$smd[es_split$smd$DF == "u",]
quantiles <- quantile(smd_u$sd_filtered, na.rm = TRUE)

#output object
smd_u_quantiles <- data.frame(median = quantiles[["50%"]],
                              lq = quantiles[["25%"]],
                              uq = quantiles[["75%"]])


#***[2.2.4] Largest SD Figure 4----

#Output: this section reported in narrative form paragraph after Figure 3
smd_u <- es_split$smd[es_split$smd$DF == "u",]

smd_u_top5 <- head(smd_u[order(smd_u$sd_filtered, decreasing = TRUE),], n = 5)
#RRR07 L02, L07, RRR9 host. L01, RRR07 L15, L19

#But which unique DF?
es_sd <- readRDS("../data/es_sd.RDS")

rrr7 <- es_sd$RRR07
rrr7_top4 <- head(rrr7[order(rrr7$sd_filtered, decreasing = TRUE),], n = 4)
unique(rrr7_top4$DF) #all u2 -> = Non-compliant time limit

rrr9_host <- es_sd$`RRR09 Hostility`
rrr9_host_max <- rrr9_host[which.max(rrr9_host$sd_filtered),]
#L01 u4 -> = 'study major' (exclude not belonging to dominant category)

#***[2.2.5] Median SD RRR07 u2----

rrr7_u2 <- rrr7[rrr7$DF == "u2",] #rrr7 object is from section 2.2.4
rrr7_u2_median <- median(rrr7_u2$sd_filtered, na.rm = TRUE) #na.rm: some labs had no multiverse with n >=24 after applying u2

#*********************************************************
#*[2.3] Figure 5 paragraphs----
#*********************************************************
fig5_data <- readRDS("../data/meta_plot_data_n24.RDS")


#***[2.3.1] Variability meta-analytic multiverses----
distribution <- fig5_data$distribution
dist_split <- split(distribution, distribution$meta_analysis)

dist_split <- lapply(dist_split,
                     function(x) data.frame(sd = sd(x$rma_es)))

meta_sd <- dplyr::bind_rows(dist_split, .id = "meta_analysis")


#output
logor_meta_sd <- meta_sd[1:2,]
smd_meta_sd <- meta_sd[-c(1:2),]

#***[2.3.2] Overestimate range----

#Get minimum + maximum difference
#Already have maximum below
estimate_range <- fig5_data$estimates[fig5_data$estimates$Selection %in% c("Random draw", "Most significant"),]
a <- split(estimate_range, estimate_range$meta_analysis)

b <- lapply(a, function(x){
  x$range <- x$rma_es[x$Selection == "Random draw"] - x$rma_es[x$Selection == "Most significant"]
  x$range <- abs(x$range)
  x[1, c("meta_analysis", "range")]
})

estimate_range <- do.call(rbind, b)
logor_hack_range <- estimate_range[1:2,]
smd_hack_range <- estimate_range[-c(1:2),]

#output
smd_hack_range_descending <- smd_hack_range[order(smd_hack_range$range),]
#logor_hack_range (ordered 1, 2)

#***[2.3.3] correlation p-hack range and multiverse size ----

#get object smd_hack_range from subsection above
multiverse_smd_sizes <- fig5_data$multiverse_sizes[-c(1:2),]
multiverse_smd_sizes$N <- as.numeric(gsub("\\D+", "", multiverse_smd_sizes$N))

multiverse_hack <- dplyr::left_join(smd_hack_range, multiverse_smd_sizes, by = "meta_analysis")
multiverse_hack_corr <- cor(multiverse_hack[,2:3])

#output
multiverse_hack_corr <- multiverse_hack_corr[2,1]

#***[2.3.4] RRR07 random draw vs. most significant----

#(pink cross, d = ES [lw, ub]] and the estimate based on the most significant effect sizes (yellow star , d = ES[lb, ub])
fig5_rrr07_estimates <- fig5_data$estimates[fig5_data$estimates$meta_analysis == "RRR07",]

#output objects
rrr7_random_draw <- fig5_rrr07_estimates[fig5_rrr07_estimates$Selection == "Random draw",
                                         c("rma_es", "ci_lb", "ci_ub")]

rrr7_most_sig <- fig5_rrr07_estimates[fig5_rrr07_estimates$Selection == "Most significant",
                                         c("rma_es", "ci_lb", "ci_ub")]


#********************************************************************************
#[3] Save as object----
#********************************************************************************

#All objects in a list must be named, otherwise no name
saveRDS(list(#Introduction
             total_labs = total_labs,
             total_participants = prettyNum(total_participants, big.mark = ","), #make 37602 -> 37,602
             #Figure 2 paragraphs
             rrr4_sd_quantile = rrr4_sd_quantile,
             logor_sd_quantile = logor_sd_quantile,
             smd_sd_quantile = smd_sd_quantile,
             max_range_rrr4 = max_range_rrr4,
             median_range_rrr4 = median_range_rrr4,
             logor_max_range = logor_max_range,
             logor_quantile_range = logor_quantile_range,
             smd_quantile_range = smd_quantile_range,
             smd_max_range = smd_max_range,
             rrr4_sig_prop_descending = rrr4_sig_prop_descending,
             rrr4_corr = rrr4_corr,
             rrr_corr = rrr_corr,
             #Figure 4 paragraph
             logor_e3_median = logor_e3_median,
             smd_e3_median = smd_e3_median,
             smd_s2_median = smd_s2_median,
             smd_u_quantiles = smd_u_quantiles,
             rrr7_u2_median = rrr7_u2_median,
             #figure 5 paragraphs
             logor_meta_sd = logor_meta_sd,
             smd_meta_sd = smd_meta_sd,
             logor_hack_range = logor_hack_range,
             smd_hack_range_descending = smd_hack_range_descending,
             overall_prop_sig = overall_prop_sig,
             multiverse_hack_corr = multiverse_hack_corr,
             rrr7_random_draw = rrr7_random_draw,
             rrr7_most_sig = rrr7_most_sig,
             #Table 4
             n_labs_with_sig = n_labs_with_sig,
             cor_multiverse_hyp_sig = cor_multiverse_hyp_sig),
        file = "../manuscript/in_text_values.RDS")
