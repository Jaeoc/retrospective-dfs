#*************************************************************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: This script extracts, cleans and formats raw data for RRR1 - RRR10
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)
#********************************************************************************

##Script content:----

#[0] Packages
#[1] RRR1
#[2] RRR2
#[3] RRR3
#[4] RRR4
#[5] RRR5
#[6] RRR6
#[7] RRR7
#[8] RRR8
#[9] RRR9
#[10] RRR10
#[11] Lab indicators


#******************************************

wd_path <- dirname(rstudioapi::getSourceEditorContext()$path) #used for setting working directory to current file location, requires Rstudio
setwd(wd_path)

#******************************************
#[0] Packages----
#******************************************
#This section specifies all external packages used in this script.
#For additional clarity there are commented library() calls in each section specifying the packages used in each section

packages <- c("dplyr", #for data wrangling
              "haven", #for opening SPSS files
              "readxl") #for opening Excel files

lapply(packages, function(x) {if(!require(x, character.only = TRUE)) {install.packages(x)}}) #install packages if needed
lapply(packages, library, character.only = TRUE) #load packages

#******************************************
#[1] RRR1 ----
#******************************************
##Raw data from Registered Replication Report 1
##Main OSF repository: https://osf.io/ybeur/
##Publication: https://doi.org/10.1177/1745691614545653
##Codebook: see headers in data and m-turk codebook https://osf.io/7i9de/
#Direct links to [pre-exclusion] raw data files:

#Michael M-turk: https://osf.io/e54x2/ #800 non-prescreen  + 350 pre-screened [18-25, race = White]. See https://osf.io/7i9de/ for m-turk codebook and https://osf.io/ez4w3/wiki/home/ for exclusion criteria
#Alogna/halberstadt: https://osf.io/tcgqu/
#Birch: https://osf.io/h4ukj/
#Birt: https://osf.io/hmadg/
#Brandimonte: https://osf.io/67muv/
#Carlson: https://osf.io/s73uq/ #prescreened participants ***Only publishing-formatted summary data also after contact***  [no individual level raw data -> not included]
#Bornstein: https://osf.io/3895k/
#Delvenne: https://osf.io/8yghp/ #prescreened participants
#Echterhoff: https://osf.io/8vqns/
#Eggleston: https://osf.io/zr4f2/
#Greenberg: https://osf.io/4yvcf/
#Kehn: https://osf.io/wyi3p/
#Koch: https://osf.io/9mfh7/
#Mammarella: https://osf.io/pcwvs/
#McCoy:  https://osf.io/2nrqa/
#Mitchell: https://osf.io/igbvm/
#Musselman: https://osf.io/bpz7x/
#Poirier: https://osf.io/i3z58/ #pre-screened participants
#Rubinova: https://osf.io/d35cv/ #pre-screened participants
#Susa: https://osf.io/i4uh2/
#Thompson: https://osf.io/bt8j5/
#Ulatowska: https://osf.io/jq6rn/ #prescreened participants [age, English-speaking(different from others!)]
#Wade: https://osf.io/g4mq7/ #pre-screened participants
#chu: https://osf.io/7kpa2/ #pre-screened participants
#Edlund: https://osf.io/yspv3/
#Gabbert: https://osf.io/sdte4/
#Leite: https://osf.io/dt98u/
#McIntyre: https://osf.io/r8d7b/
#Michael: https://osf.io/2c8dy/ #Pre-screened participants with "post-hoc" exclusions [not sure what means]. Data is with coding which can be used for strict exclusions.
#Palmer: https://osf.io/ytwgk/
#Verkoeijen: https://osf.io/7vdy9/
#Was: https://osf.io/rs4xg/ #Pre-screened participants

##Additional comments: Note that some labs made no exclusions and that some labs
##pre-screened participants based on exclusion criteria.(see "Appendix:
##Individual Lab Details" in RRR) and that some labs with only RRR1 data have
##data for RRR2 but collected too few participants and thus were excluded by the
##RRR. In addition, there is no script to indicate how authors got from raw data to
##the summary data uploaded in the main repository.Finally, RRR1 reports that they
##included 31 labs, but this corresponds to 32 datasets because one also collected
##the m-turk dataset. Since we exclude Carlson, see above, we have 31 datasets.



#*********************
# library(dplyr)
# library(readxl)

files1 <- list.files("../data/02_raw-data/RRR1", full.names = TRUE)

labs1 <- gsub("data_", "", tolower(files1)) #pre-processing to simplify extracting names
labs1 <- unlist(lapply(strsplit(labs1, split = "[/_-]"),function(x) x[[7]])) #extract names

#correct a few names because files were named inconsistently
to_correct <- c("cislak", "template", "echterhoffkopietz", "final", "master.xlsx", "mccoydata.xlsx", "schoolerdatabirchlab.xlsx")
corrected <- c("ulatowska" , "michael", "echterhoff", "mitchell", "mturk", "mccoy", "birch")
labs1[match(to_correct, labs1)] <-  corrected #replace names

rrr1 <- lapply(files1, read_excel, col_types = "text") #read raw data as text, otherwise R guesses different between datasets
names(rrr1) <- labs1

#correct some files
was_index <- which(sapply(files1, grepl, pattern = "Was")) #gives the index for was
mcintyre_index <- which(sapply(files1, grepl, pattern = "McIntyre")) #gives the index for mcintyre

rrr1$was <- read_xlsx(files1[[was_index]], sheet = 2, col_types = "text", ) #"Was" data is on second sheet not first
rrr1$mcintyre <- read_xlsx(files1[[mcintyre_index]], n_max = 112, col_types = "text") #mcintyre has extra computations below data that mess up the data and should not be included


#clean up m-turk dataset a bit first since it differs substantially from rest
mturk_cleaner_12 <- function(mturk_data){ #as a function since will be used for RRR2 as well
  mturk_data %>%
    mutate(
      Subject_Number = as.character(row_number()), #first add an identifier to each participant in mturk dataset since the originals were deleted for anonymity. Must be character to be able to bind with other datasets later
      `Which ethnicity do you most strongly identify as?` = case_when( #recode 'Race' variable to be consistent with other datasets
        `Which ethnicity do you most strongly identify as?` == 1 ~ "white",
        TRUE ~ "other")) %>% #sets all other categories to 'other'
    rename( #improve relevant mturk variable names
      mturk_5_attempts = `At least 5 attempts in control task?`, #new name on left-hand side of equal sign
      mturk_follow_instructions = `Followed instructions?`,
      mturk_select_no_attent_check = `Please select 'no' as the answer to this question, and remember the / word 'horse'. On the next page, you will be asked to enter that / word.`,
      mturk_horse_attent_check = `What was the word? If you do not know, please just leave this / answer blank.`,
      mturk_english_first_language = `Is English your first language?`,
      mturk_studied_psychology = `Have you ever studied psychology before?`) %>%
    mutate( #clarify a couple of variables
      mturk_select_no_attent_check = ifelse(mturk_select_no_attent_check == "2", "no", "yes"),
      mturk_english_first_language = ifelse(mturk_english_first_language == "1", "yes", "no"),
      mturk_studied_psychology = ifelse(mturk_studied_psychology == "1", "yes", "no"),
      mturk_5_attempts = ifelse(mturk_5_attempts == "1", "yes", "no" ),
      mturk_follow_instructions = ifelse(mturk_follow_instructions == "1", "yes", "no"),
      fam_with_effect = case_when(`Seen video before?` == "1" | `Done a study like this one before?` == "1" ~ "yes", #for U2.
                                        `Seen video before?` == "0" & `Done a study like this one before?` == "0" ~ "no"),
      mturk_horse_attent_check = tolower(mturk_horse_attent_check),
      mturk_horse_attent_check = ifelse(mturk_horse_attent_check == "horse", mturk_horse_attent_check, NA), #set input failures to NA to match the blank ones

      mturk_attent_checks_passed = case_when(mturk_horse_attent_check == "horse" & mturk_select_no_attent_check == "no" ~ 1, #proportion attention checks passed, missing value = not passing
                                       mturk_horse_attent_check == "horse" & (mturk_select_no_attent_check == "yes" | is.na(mturk_select_no_attent_check)) ~ 0.5,
                                       is.na(mturk_horse_attent_check) & mturk_select_no_attent_check == "no" ~ 0.5,
                                       is.na(mturk_horse_attent_check) & (mturk_select_no_attent_check == "yes" | is.na(mturk_select_no_attent_check)) ~ 0))
}

rrr1$mturk <- mturk_cleaner_12(rrr1$mturk) %>%
  filter(VERSION == "OLD (task then filler)") #corresponds to RRR1, see m-turk codebook https://osf.io/7i9de/

#combine and standardize data across datasets
rrr1 <- bind_rows(rrr1, .id = "lab") #bind list of data frames into one dataframe with an identifier from list name. Non-matching columns get filled with NAs.

rrr1 <- rrr1 %>% #standardize variable names across datasets
  mutate(lineup_choice = coalesce(Lineup_Choice, #the function 'coalesce' is used to combine the same variable named differently across datasets
                                  Lineup_choice,
                                  `Lineup Choice`,
                                  `LINEUP CHOICE`),
         condition = coalesce(Condition,
                              CONDITION),
         reason_for_exclusion = coalesce(Reason_for_Exclusion,
                                         Reason_For_Exclusion,
                                         ReasonForExclusion,
                                         Reason),
         confidence = coalesce(Confidence,
                               `LINEUP CONFIDENCE`),
         subject_number = coalesce(Code,
                                   Subject_Number,
                                   `Subject#`),
         age_categorical = coalesce(`Age18-25 (y/n)`,
                                    ifelse(lab == "michael", Age, NA)), #lab michael's 'Age' variable is categorical
         excluded_original = coalesce(`Excluded?`,
                                      Excluded,
                                      `EXCLUSIONS (1 = pass, 0 = fail)`),
         race = coalesce(Race,
                         `Which ethnicity do you most strongly identify as?`))


rrr1 <- rrr1 %>% #standardize content within variables across datasets
  filter(!is.na(condition)) %>% #remove empty rows and some participants whose computer broke down. Do this first, so second step doesn't have to go through a million rows
  na_if("N/A") %>% #convert manual missings in 'was' dataset to true NAs
  na_if("Blank") %>% #convert manual missings in 'leite' dataset to true NAs
  filter(!is.na(condition)) %>% #remove any newly created missings
  mutate(
    age_categorical = case_when(age_categorical == "y" ~ "18-25", #for clarity recode the y/n from age18-25 (y/n)
                                age_categorical == "n" ~ "not 18-25",
                                TRUE ~ as.character(age_categorical)),
    Age = case_when(age_categorical == "18-25" ~ 21.5, #convert the categorical ages to the value in the middle of the range
                    age_categorical == "not 18-25" ~ 26, #or in this case, just outside the range
                    age_categorical == "16-17" ~ 16.5,
                    age_categorical == "18-20" ~ 19,
                    age_categorical == "21-24" ~ 22.5,
                    age_categorical == "25-39" ~ 32,
                    TRUE ~ as.double(Age)),

    race = case_when( #standardize race to {white, other, NA}
      race %in% c("White", "white", "W", "Caucasian", "Pakeha/NZ European") ~ "white",
      is.na(race) ~ NA_character_, #NAs must be consistent with rest of vector type
      race %in% c("[Decline to Answer]", "Unknown", "Other_Decline", "Asian_Decline", "Black_Decline") ~ NA_character_, #not clear what is meant by the e.g., 'black_decline' but I will categorize them as decline to respond
      TRUE ~ "other"),

    condition = ifelse( #standardize condition to {treatment, control}
      condition %in% c("Experimental", "EXPERIMENTAL", "experimental", "E", "Description"), "treatment", "control"),

    lineup_choice = ifelse( #clarify outcome
      lineup_choice == "6", "correct", "incorrect"),

    excluded_original = case_when( #standardize exclusions to {yes, no}
      excluded_original %in% c("1", "YES", "Yes", "Y", "yes") ~ "yes",
      is.na(excluded_original) ~ "no", #NAs for this variable means participants were not excluded
      TRUE ~ "no"),

    reason_for_exclusion = ifelse(reason_for_exclusion %in% c("NA", "n/a"), NA, reason_for_exclusion), #convert na codings into real NAs
    fam_with_effect = case_when(fam_with_effect == "yes" |
                                  grepl(paste(c("hypothesis known", "overshadowing known", #see U2.
                                                "heard about study design", "May have had awareness of other condition",
                                                "Reported he/she heard from another participant that the first task consisted of describing a person"), collapse = "|"), reason_for_exclusion) ~ "yes",
                                is.na(fam_with_effect) ~ NA_character_,
                                fam_with_effect == "no" ~ "no"),

    comprehension = ifelse(reason_for_exclusion %in% c("Did not understand that it was robbery", "did not understand the event (i.e., identify it as a bank robbery)"), "no", "yes"), #see U1.

    confidence = ifelse(confidence == ".", NA, confidence))

rrr1 <- rrr1 %>% #remove participants with experimenter error or who didn't follow instructions (as coded in reason_for_exclusion)
  filter(!reason_for_exclusion %in% c("Did not continue with describing the bank robber after the experiment leader provided a prompt after 3 minutes",
                                      "Had a very low motivation from the start of the experiment onwards. Also, did not continue with describing the bank robber after the experiment leader provided a prompt after 3 minutes",
                                      "Computer went to sleep and participant spent longer than 20 minutes on crossword",
                                      "participant repeatedly left room during session because to turn off phone alarm ",
                                      "participant opened door during video. Did not watch whole video",
                                      "Lineup did not appear",
                                      "tech. difficulty",
                                      "Experimenter Error",
                                      "Technical difficulties",
                                      "Technical difficulties (could not hear audio)",
                                      "Experimenter error",
                                      "Failure to follow instructions",
                                      "Earthquake interrupted session",
                                      "Race; No sound on video",
                                      "No sound on video",
                                      "Race; Did not follow procedure",
                                      "Error in procedure; Condition assigned between sessions",
                                      "Race; Condition assigned between sessions",
                                      "Condition assigned between sessions",
                                      "Did not attempt crossword",
                                      "Subject insisted on asking questions throughout the session and did not follow instructions to keep working on the crossword task.",
                                      "Subject described the actions rather than the visual characteristics of the robber.",
                                      "Listed states and capitals rather than countries and capitals."))

#exchange lab names for codes
rrr1$lab <- factor(rrr1$lab, levels = labs1, #using formatC below to add zeroes to make L1 into L01 which makes sorting easier later
                   labels = paste0("L", formatC(1:length(labs1), width = 2, format = "d", flag = "0")))
lab_id1 <- data.frame(lab = labs1, id = unique(rrr1$lab)) #for keeping track of which lab is which in an appendix


rrr1 <- rrr1 %>% #select only relevant variables
  select(lab, #{L1:L31}
         subject_number,
         condition, #{treatment, control}
         lineup_choice, #{incorrect, correct, NA}
         age = Age, #make lower-case {3:82}
         race, #{white, other, NA}
         confidence, #{NA, 1.5, 5.5, 1:8)
         original_excl_reason = reason_for_exclusion, #Only information, not used further {56 unique reasons remaining}
         fam_with_effect, #{yes, no, NA}
         comprehension, #{yes, no}
         mturk_5_attempts, #{yes, no, NA}
         mturk_follow_instructions, #{yes, no, NA}
         mturk_attent_checks_passed, #{NA, 1.0, 0.5, 0.0}
         mturk_english_first_language, #{yes, no, NA}
         mturk_studied_psychology) #{yes, no, NA}

#save intermediate dataset
# write.csv(rrr1, "../data/03_cleaned-raw-data/rrr1.csv", row.names = FALSE)

#******************************************
#[2] RRR2 ----
#******************************************
##Raw data from Registered Replication Report 2
##Main OSF repository: https://osf.io/ybeur/
##Publication: https://doi.org/10.1177/1745691614545653
##Codebook: see headers in data and m-turk codebook https://osf.io/7i9de/
#Direct links to [pre-exclusion] raw data files:

#Michael M-turk: https://osf.io/e54x2/ #800 non-prescreen  + 350 pre-screened [18-25, race = White]. See https://osf.io/7i9de/ for m-turk codebook and https://osf.io/ez4w3/wiki/home/ for exclusion criteria
#Alogna: https://osf.io/4xrpk/
#Birch: https://osf.io/87b3s/
#Birt: https://osf.io/hf6tu/
#Brandimonte: https://osf.io/ktx8g/
#Carlson: https://osf.io/s73uq/ -----Only publishing-formatted data****** [pre-screened participants][not currently included]
#Bornstein: https://osf.io/wxeq9/
#Delvenne: https://osf.io/jk45z/
#Echterhoff: https://osf.io/qpigy/
#Eggleston: https://osf.io/672ms/
#Greenberg: https://osf.io/q5x2e/
#Kehn: https://osf.io/9g86r/
#Koch: https://osf.io/6kpnj/
#Mammarella: https://osf.io/fi8p4/
#McCoy: https://osf.io/gpq3d/
#Mitchell: https://osf.io/cy7tx/
#Musselman: https://osf.io/w4yne/
#Poirier: https://osf.io/dkc7v/
#Rubinova: https://osf.io/a7hwn/
#Susa: https://osf.io/ifc3v/
#Thompson: https://osf.io/ivfry/
#Ulatowska: https://osf.io/t6qvu/
#Wade: https://osf.io/q98df/

#Excluded from RRR2 for too small sample size but have data available, so we use them
#Edlund: https://osf.io/izkxg/
#Leite: https://osf.io/xc53p/
#McIntyre: https://osf.io/p79iw/
#Palmer: https://osf.io/urzdx/


##Additional comments: RRR1 & RRR2 data published in same paper and has same
##OSF.


files2 <- list.files("../data/02_raw-data/RRR2", full.names = TRUE)
leite <- files2[grepl("Leite", files2)] #only one with data as a .csv file, must be loaded separately
files2 <- files2[!files2 == leite]

labs2 <- gsub("data_", "", tolower(files2)) #pre-processing to simplify extracting names
labs2 <- unlist(lapply(strsplit(labs2, split = "[/_-]"),function(x) x[[7]])) #extract names

#correct a few names because files were named inconsistently
to_correct <- c("cislak", "template", "echterhoffkopietz", "master.xlsx", "mccoydatastudy2.xls", "schoolerstudy2excludprobpartic")
corrected <- c("ulatowska" , "mitchell", "echterhoff", "mturk", "mccoy", "birch")
labs2[match(to_correct, labs2)] <-  corrected #replace names

rrr2 <- lapply(files2, read_excel, col_types = "text")
leite_data <- read.csv(leite, stringsAsFactors = FALSE, colClasses = "character")
rrr2 <- c(rrr2, list(leite_data))
names(rrr2) <- c(labs2, "leite")


#clean up mturk data
rrr2$mturk <- mturk_cleaner_12(rrr2$mturk) %>% #function from RRR1 section, around line 105
  filter(VERSION == "NEW (filler then task)") #corresponds to RRR2, see m-turk codebook https://osf.io/7i9de/

#fix crossed columns in mamarella dataset
rrr2$mammarella <- rrr2$mammarella %>%
  rename(Age = Gender, Gender = Age)

#combine and standardize data across datasets
rrr2 <- bind_rows(rrr2, .id = "lab") #bind list of data frames into one dataframe with an identifier from list name. Non-matching columns get filled with NAs.

rrr2 <- rrr2 %>% #standardize variable names across datasets
  mutate(lineup_choice = coalesce(Lineup_Choice, #the function 'coalesce' is used to combine the same variable named differently across datasets
                                  Lineup_choice,
                                  `Line-up Identification`,
                                  `LINEUP CHOICE`,
                                  response),
         condition = coalesce(Condition,
                              CONDITION,
                              condition,
                              `Verbalization/Control`),
         reason_for_exclusion = coalesce(Reason_for_Exclusion,
                                         Reason),
         confidence = coalesce(Confidence,
                               `LINEUP CONFIDENCE`,
                               Confidence.RESP),
         subject_number = coalesce(Code,
                                   Subject_Number,
                                   `Subject Number`,
                                   Subject,
                                   randomization),
         age = coalesce(age,
                        Age),
         excluded_original = coalesce(`Excluded?`,
                                      Excluded,
                                      `EXCLUSIONS (1 = pass, 0 = fail)`),
         race = coalesce(Race,
                         `Which ethnicity do you most strongly identify as?`))


rrr2 <- rrr2 %>% #standardize content within variables across datasets
  filter(!is.na(condition)) %>% #remove empty rows and some participants whose computer broke down. Do this first, so second step doesn't have to go through a million rows
  na_if("XXXX") %>% #convert manual missings in 'wade' dataset to true NAs
  na_if("") %>% #convert empty strings in 'leite' dataset to true NAs
  filter(!is.na(condition)) %>% #remove any newly created missings
  mutate(race = case_when(
    race %in% c("White", "white", "W", "Caucasian", "1") ~ "white", #standardize race to {white, other, NA}
    is.na(race) ~ NA_character_, #NAs must be consistent with rest of vector type
    race == "Decline" ~ NA_character_,
    TRUE ~ "other"),

    condition = ifelse( #standardize condition to {treatment, control}
      condition %in% c("Experimental", "experimental", "E", "Description", "1", "exp", "EXPERIMENTAL", "Verbalization"), "treatment", "control"),

    lineup_choice = ifelse( #clarify outcome {correct, incorrect}
      lineup_choice == "6", "correct", "incorrect"),

    excluded_original = case_when( #standardize exclusions to {yes, no}
      excluded_original %in% c("1", "YES", "Yes", "Y") ~ "yes",
      is.na(excluded_original) ~ "no", #NAs for this variable means participants were not excluded
      TRUE ~ "no"),

    comprehension = ifelse(reason_for_exclusion %in% c("Did not understand that it was robbery", "did not understand the event (i.e., identify it as a bank robbery)",
                                                       "Claimed it was not robbery"), "no", "yes"), #see U1.

    age = case_when(`Age range` == "18-25" ~ 21.5,
                    TRUE~as.double(age)),

    fam_with_effect = case_when(fam_with_effect == "yes" |
                                  grepl(paste(c("hypothesis known", "overshadowing known", #see U2.
                                                "heard about study design", "May have had awareness of other condition",
                                                "Reported he/she heard from another participant that the first task consisted of describing a person",
                                                "Participant shared knowledge of true purpose of study"), collapse = "|"), reason_for_exclusion) ~ "yes",
                                is.na(fam_with_effect) ~ NA_character_,
                                fam_with_effect == "no" ~ "no"))


rrr2 <- rrr2 %>% #remove participants with experimenter error or who didn't follow instructions (as coded in reason_for_exclusion)
  filter(!reason_for_exclusion %in% c("Did not continue with describing the bank robber after the experiment leader provided a prompt after 3 minutes",
                                      "Had a very low motivation from the start of the experiment onwards. Also, did not continue with describing the bank robber after the experiment leader provided a prompt after 3 minutes",
                                      "Computer went to sleep and participant spent longer than 20 minutes on crossword",
                                      "participant repeatedly left room during session because to turn off phone alarm ",
                                      "participant opened door during video. Did not watch whole video",
                                      "Lineup did not appear",
                                      "tech. difficulty",
                                      "Experimenter Error",
                                      "Technical difficulties",
                                      "Technical difficulties (could not hear audio)",
                                      "Experimenter error",
                                      "Failure to follow instructions",
                                      "Earthquake interrupted session",
                                      "Race; No sound on video",
                                      "No sound on video",
                                      "Race; Did not follow procedure",
                                      "Error in procedure; Condition assigned between sessions",
                                      "Race; Condition assigned between sessions",
                                      "Condition assigned between sessions",
                                      "Did not attempt crossword",
                                      "Subject insisted on asking questions throughout the session and did not follow instructions to keep working on the crossword task.",
                                      "Subject described the actions rather than the visual characteristics of the robber.",
                                      "Listed states and capitals rather than countries and capitals.",
                                      "computer error",
                                      "no sound",
                                      "Timer not started for the listing task",
                                      "Took off headphones during the video; see replacement number 891",
                                      "Technical issues",
                                      "Study disruption",
                                      "Particpant noncompliance",
                                      "Computer error"))

#exchange lab names for codes
rrr2$lab <- factor(rrr2$lab, levels = c(labs2, "leite"),
                   labels = paste0("L", formatC(1:length(c(labs2, "leite")), width = 2, format = "d", flag = "0")))
lab_id2 <- data.frame(lab = c(labs2, "leite"), id = unique(rrr2$lab)) #for keeping track of which lab is which in an appendix


rrr2 <- rrr2 %>% #select only relevant variables
  select(lab, #{L1:L26}
         subject_number,
         condition, #{treatment, control}
         lineup_choice, #{incorrect, correct, NA}
         age, # {16:76, NA}
         race, #{white, other, NA}
         confidence, #{NA, 1:8)
         original_excl_reason = reason_for_exclusion, #Only information, not used further {32 unique reasons remaining}
         fam_with_effect, #{yes, no, NA}
         comprehension, #{yes, no}
         mturk_5_attempts, #{yes, no, NA}
         mturk_follow_instructions, #{yes, no, NA}
         mturk_attent_checks_passed, #{NA, 1.0, 0.5, 0.0}
         mturk_english_first_language, #{yes, no, NA}
         mturk_studied_psychology) #{yes, no, NA}


#save intermediate dataset
# write.csv(rrr2, "../data/03_cleaned-raw-data/rrr2.csv", row.names = FALSE)

#******************************************
#[3] RRR3 ----
#******************************************
##Raw data from Registered Replication Report 3
##Main OSF page: https://osf.io/d3mw4/
##Publication: https://doi.org/10.1177/1745691615605826
##Codebook https://osf.io/ntr2p/ [sheet 2 of excel]

##Direct links to [pre-exclusion] raw data files:
#Arnal: https://osf.io/ztvrp/ filename: "H & A Rep Data File"
#Berger: non-excluded participants: https://osf.io/jmigz/ and excluded participants: https://osf.io/5376y/
#Birt: missing, https://osf.io/gducj/, incorrect privacy configuration. [no data after 4 email requests, exclude]
#Erland lab: https://osf.io/9eg7y/
#Erland M-turk: https://osf.io/3bzf7/
#Ferreti: https://osf.io/7kizw/ #One dataset for each condition. But one of the two datasets missing. No response to emails: exclude.
#Knepp: https://osf.io/vfgp8/ filename: "Format_Excel_Data_Collection"
#kurby: https://osf.io/86zg9/
#Melcher: https://osf.io/r2a5f/ filename: "Hart_Albarracin_Data_Collection_raw_data"
#Michael: not on OSF https://osf.io/8y6bf/, incorrect privacy configuration? Acquired through private communcation (email) 2019-04-28
#Poirier: https://osf.io/r7hx4/ filename: "Format_Excel_Data_Collection_11-19-14"
#Prenoveau: https://osf.io/t4ng6/


##Additional comments:
##Files on main OSF repository are post-exclusions. No data available from 2 labs.


#*********************
#library(dplyr)
#library(haven)
#library(readxl)

#Load all data
files3_excel <- list.files("../data/02_raw-data/RRR3", pattern = ".xlsx", full.names = TRUE)
files3_sav <- list.files("../data/02_raw-data/RRR3", pattern = ".sav", full.names = TRUE)

rrr3_excel <- lapply(files3_excel, read_xlsx, col_types = "text")
rrr3_sav <- lapply(files3_sav, read_sav)

names(rrr3_excel) <- c("berger", "berger_excluded", "prenoveau", "eerland_lab", "mturk", "knepp", "poirier", "melcher", "kurby", "ferretti")
names(rrr3_sav) <- c("michael", "arnal")

rrr3 <- c(rrr3_excel, rrr3_sav) #combine into one list
rrr3$berger <- rbind(rrr3$berger, rrr3$berger_excluded) #combine berger data into one dataframe
rrr3$berger_excluded <- NULL #drop now unnecessary dataframe from list
rrr3$ferretti <- NULL #Drop Ferreti who only uploaded data from participants in one condition
labs3 <- names(rrr3) #get names, later used to exchanging names for codes

rrr3 <- lapply(rrr3, function(x) {mutate(x, subject_number = row_number())}) #add unique id within each dataset
rrr3 <- lapply(rrr3, mutate_all, as.character) #Convert all columns to character. .sav needs to be as characters for combining in next step.

#combine and standardize data across datasets
rrr3 <- bind_rows(rrr3, .id = "lab") #bind list of data frames into one dataframe with an identifier from list name. Non-matching columns get filled with NAs.

rrr3 <- rrr3 %>% #standardize variable names
  mutate(intent_item_1 = coalesce(A1,
                                  KnowHarm),
         intent_item_2 = coalesce(A2,
                                 IntentHarm),
         intent_item_3 = coalesce(A3,
                                  DelibHarm),
         process_item_1 = coalesce(A4,
                                   ImagineUnfold),
         process_item_2 = coalesce(A5,
                                   ImagineBehavior),
         process_item_3 = coalesce(A6,
                                   ImagineMovement),
         process_item_4 = coalesce(A7,
                                   ImagineCrime),
         attrib_item_1 = coalesce(A8,
                                  CapablePurpose),
         attrib_item_2 = coalesce(A9,
                                  CapablePlanned),
         attrib_item_3 = coalesce(A10,
                                  Goals),
         condition = coalesce(Cond,
                              Condition,
                              `Condition (1= perf, 2= imperf)`))

rrr3 <- rrr3 %>% #standardize within variables and cleanup
  filter(!is.na(condition)) %>% #drop some empty rows in michael dataset
  filter(!`Reason for exclusion` %in% c("Experimenter Error", #drop participants with experimenter error/not following instructions
                                        "Experimenter error",
                                        "Excluded for not following the instructions",
                                        "Second participation", #drop participants in m-turk study who particpated multiple times
                                        "Non native+second participation")) %>% #drop participants in m-turk study who particpated multiple times
  mutate(condition = recode(condition,
                            "1" = "perfect", #clarify
                            "2" = "imperfect"),
         L1 = case_when( #standardize L1 [native language] to {english, english_bilingual, other}
           L1 %in% c("English", "english", "engish","American English", "ENGLISH",
                     "american English", "american", "English (U.S)", "1") ~ "english", #note that Kurby coded "american" as non-English originally
           L1 %in% c("English/Spanish", "Spanish and English", "English and Spanish", "English and Malayalam",
                     "Greek/English", "Polish/English", "Spanish/English") ~ "english_bilingual",
           TRUE ~ "other"),
         L1 = ifelse(L1 == "english" & `Reason for exclusion` %in%
                       c("Bilingual", "English Not Primary"), "english_bilingual", L1)) %>%  #some participants only have 'english' as their native language (L1) but were originally excluded as bilingual
  mutate_at(vars(matches("intent|process|attrib")), as.numeric) %>% #convert DV items to numeric for computations
  mutate_at(vars(intent_item_1, intent_item_2, intent_item_3), #for these variables
            ~ifelse(lab != "kurby", .-6, .)) %>%  #for all labs except kurby (who already recoded), take the item score -6 (see https://osf.io/2z)
  mutate_at(vars(process_item_1, process_item_2, process_item_3, process_item_4), #for these variables
            ~case_when(
              lab == "kurby" ~ as.double(.),#for all labs except kurby, who already (seemingly) recoded values
              . == 1 ~ 7, #invert item scores, see https://osf.io/2z
              . == 2 ~ 6,
              . == 3 ~ 5,
              . == 5 ~ 3,
              . == 6 ~ 2,
              . == 7 ~ 1,
            TRUE ~ as.double(.))) %>%
  mutate(Age = case_when(Age %in% c("eighteen", "18 years old") ~ "18", #standardize age variable values
                         Age == "19 years old" ~ "19", #Note, one participant had age = "190", prenoveau coded this participant as "outside of age range", i.e, excluded and in line with other datasets we leave it as is
                         TRUE ~ Age))

#Exchange lab names for codes
rrr3$lab <- factor(rrr3$lab, levels = labs3,
                   labels = paste0("L", formatC(1:length(labs3), width = 2, format = "d", flag = "0")))

lab_id3 <- data.frame(lab = labs3, id = unique(rrr3$lab)) #for keeping track of which lab is which in an appendix

rrr3 <- rrr3 %>% #select only relevant variables
  select(lab, #lab code {L1:L10}
         subject_number,
         condition, #{imperfect, perfect}
         intent_item_1, #{-5:5}
         intent_item_2, #{-5:5}
         intent_item_3, #{-5:5}
         process_item_1, #{1:7, NA}
         process_item_2, #{1:7, NA}
         process_item_3, #{1:7, NA}
         process_item_4, #{1:7, NA}
         attrib_item_1, #{1:5}
         attrib_item_2, #{1:5}
         attrib_item_3, #{1:5}
         age = Age, #remove capitalization. {17:190}
         native_lang = L1, #{english, english_bilingual, other}
         education = Edu, #remove capitalization, variable only in mturk dataset {1:7, NA}
         original_excl_reason = `Reason for exclusion`) #improve variable name. Old exclusions, only used as info. {14 options remaining}


#save intermediate dataset
# write.csv(rrr3, "../data/03_cleaned-raw-data/rrr3.csv", row.names = FALSE)


#******************************************
#[4] RRR4 ----
#******************************************
##Raw data from Registered Replication Report 4
##Main OSF page: https://osf.io/jymhe/
##Publication: https://doi.org/10.1177/1745691616652873
##codebook (nonexhaustive): https://osf.io/nb8uj/  [file: readme.txt]

#Direct links to raw data files:
#Birt: https://osf.io/u5ec4/
#Brandt: https://osf.io/3squw/
#Brewer/Lau: https://osf.io/tpdfe/
#Calvillo: https://osf.io/ea6xd/
#Carruth: https://osf.io/vj4xe/
#Cheung: main https://osf.io/6ntbz/ and supplement https://osf.io/3xbds/ [contains age and language variables: filename 'SubjectStatus_complimentary.sav']
#Crowell: https://osf.io/udfqk/
#Elson: https://osf.io/2n6gv/
#Evans: https://osf.io/pwvmd/
#Francis: https://osf.io/w6u75/
#Hagger: https://osf.io/uz35s/
#Lange: https://osf.io/mkvyp/
#Otgaar: main https://osf.io/h8ag4/ and supplement  https://osf.io/yugw5/ [age variable, and where ID 155 == ID 150 in the primary dataset: file: 'questionnaire_ratings_Dutch Umaastricht']
#Phillip: https://osf.io/4zjsk/
#Rentzsch: https://osf.io/f7zve/
#Ringos: https://osf.io/gdnmf/
#Schlinkert: main https://osf.io/sn2b7/ and supplement [personal communcation (email) - age variable]
#Stamos: main https://osf.io/bv9tz/ and supplement https://osf.io/q73k6/ [age variable: filename 'Replication_Ego Depletion.sav']
#Tinghog: https://osf.io/8tyvz/ [note: link to lab page dataset rather than main OSF page, because lab page version contains age and language variables: filename 'EgodepletionResults151019.sav']
#Ullrich: https://osf.io/x52zw/
#VanDellen: https://osf.io/ju3p6/
#Wolff: https://osf.io/rq8hw/
#Yusainy: main https://osf.io/hwjga/ and supplement https://osf.io/pt69a/ [contains age variable: filename 'Participants Data']
#Zerhouni: main https://osf.io/kd6h7/ and supplement https://osf.io/eqabw/ [contains age variables: filename  'RRRData.xlsx']



##Additional comments:
##1) In the main OSF repository there is a 'merged data file' (see https://osf.io/t3mns/). However, this does not seem to
##correspond to simply merging the lab data files (linked above). For example, for the Brandt lab the merged file
##has neither age nor language variables, which the Brandt lab data file contains. We thus use the lab data
##files as they are apparently less edited
##
##2) Some labs excluded participants, e.g., due to not being native speakers, before uploading their data. Although two
##of these labs provided the raw data when contacted (VanDellen, Rentzsch) we were not able to retrieve the data for these
##participants due to the lack of an 'E-prime' license and the extensive work required to collate these participants' data.
##One other lab (Calvillo) lost the data for these excluded participants when moving computers [personal communication]
##and one lab (Carruth) did not respond to repeated contact attempts. We kept these labs' data despite these issues, and
##simply note that their multiverses will be smaller than they should be.


#*********************
#library(dplyr)
#library(haven)

##Load full datasets
files4 <- list.files("../data/02_raw-data/RRR4/", pattern = ".sav", full.names = TRUE)
rrr4 <- lapply(files4, read_sav)
labs4 <- tolower(unlist(lapply(strsplit(files4, split = "/|_"), function(x) x[[6]])))
labs4[match("egodepletionresults151019.sav", labs4)] <- "tinghog" #correct one name
names(rrr4) <- labs4

##supplemental data (add age/language from supplemental data to main dataset for 6 datasets)
#first load supplemental data
supp4_excel <- list.files("../data/02_raw-data/RRR4/supplemental-data/", full.names = TRUE, pattern = "xlsx") #warnings can be ignored, they reflect that one dataset doesn't have headers and warnings
supp4_sav <- list.files("../data/02_raw-data/RRR4/supplemental-data/", full.names = TRUE, pattern = "sav") # about an unexpected date format. The first is fixed below and the second doesn't matter for our purposes.
r4_excel <- lapply(supp4_excel, function(x) as.data.frame(read_excel(x))) #encoding error if don't use as.data.frame
r4_excel[[2]] <- read_excel(supp4_excel[2], col_names = FALSE, n_max = 100) #the otgar supplement does not have header names and summary data rows 101-103
r4_sav <- lapply(supp4_sav, read_sav)
rrr4_supp <- c(r4_excel, r4_sav)
names(rrr4_supp) <- c("yusainy", "otgaar", "zerhouni", "schlinkert", "stamos", "cheung")

#Next add age/language variables to the full dataset [warning messages about different attributes can be ignored]
rrr4$cheung <- rrr4_supp$cheung %>% #add age and language variable to Cheung dataset
  select(Subject, Age, Language) %>% #only keep relevant variables in supplemental (age) data
  left_join(rrr4$cheung, ., by = "Subject") #match to full dataset by Subject ID

rrr4$zerhouni <- rrr4_supp$zerhouni %>% #add age for zerhouni/muller
  select(Subject, age.RESP) %>%
  distinct() %>% #rows are repeating, so need to take the unique ones
  left_join(rrr4$zerhouni, ., by = "Subject")

rrr4$otgaar <- rrr4_supp$otgaar %>% #Otgaar
  select(Subject = ...1, age = ...3) %>%
  mutate(Subject = ifelse(Subject == 155, 150, Subject)) %>% #the same subject appears to have had different code in the two datasets, 155 -> 150
  left_join(rrr4$otgaar, ., by = "Subject")

rrr4$schlinkert <- rrr4_supp$schlinkert %>% #schlinkert
  mutate(Subject = as.double(row_number())) %>% #add subject ID for matching, convert to double for compatibility
  left_join(rrr4$schlinkert, ., by = "Subject")

rrr4$stamos <- rrr4_supp$stamos %>% #Stamos
  select(Subject = Participant, Age) %>% #rename 'participant' to 'Subject' for matching in next line
  left_join(rrr4$stamos, ., by = "Subject")

rrr4$yusainy <- rrr4_supp$yusainy %>% #Yusainy
  select(Subject, Age) %>%
  left_join(rrr4$yusainy, ., by = "Subject")


##Combine datasets and standardize
rrr4 <- lapply(rrr4, mutate_all, as.character) %>% #standardize variable types to prep for combining into one dataframe
  bind_rows(., .id = "lab") %>% #bind lists into one dataframe, with an identifier variable called 'lab' equal to the list names
  mutate(Subject = case_when( #drop cases with experimental error/computer crash/not following instructions
    lab == "elson" & Subject == "2015070776" ~ NA_character_, #Experimental error one(1) subject
    lab == "francis" & other_exclude == "0" ~ NA_character_, #1 subject 'knew hypothesis' and 1 excluded for 'no practice MAS' [not following instructions?]
    lab == "brewer" & is.na(condition) ~ NA_character_, #1 subject excluded for not following instructions
    lab == "evans" & is.na(Task) ~ NA_character_, #1 participant excluded for data not recorded properly by experimenter
    lab == "zerhouni" & Subject == "95" ~ NA_character_, #1 subject not following instructions
    lab == "rentzsch" & Subject %in% c("38", "122") ~ NA_character_, #Experimenter error (38) and non-native speaker (122) without retrievable data
    lab == "ringos" & Subject %in% c("50", "54") ~ NA_character_, #2 subjects for computer malfunctioning
    lab == "ullrich" & Subject %in% c("11", "13", "97") ~ NA_character_, #3 subjects did not follow instructions or computer crashed
    lab == "yusainy" & exclude_other == "0" ~ NA_character_, #2 subjects did not follow instructions
    TRUE ~ Subject)) %>% #other labs with computer/experimental error/not following instructions already deleted participants before uploading data
  filter(!is.na(Subject)) #drop the above cases


rrr4 <- rrr4 %>% #standardize variable names
  mutate(age = coalesce(Age, #no age variable for Carruth nor Ringos [missed emailing Ringos]
                        age,
                        age.RESP),
         native_speaker = coalesce(lang, #Brandt {1 = native, 2 = non-native}
                                   language, #hagger {1 = native, 2-15 = non-native}
                                   Language, #birt, Cheung {1 = native, 3, 5 = non-native}, tinghog {Swedish, other languages}
                                   native_language, #Elson {1 = native [all natives]}}
                                   EnglishAge, #francis {"" = native, unknown = NA, c(7, 9, 10, 11, 12, 13, 15) = bilingual [age learnt English]}
                                   EnglNat, #phillip {1 = native [all natives]}
                                   age_lang_excl, #Evans, {1 = native, 0 = non-native [after excluding experimenter/computer error etc above]}. Also has 'English' variable but does not appear to be used
                                   excl_langother)) #ullrich {1 = native, 0 = non-native [after excluding experimenter/computer error etc above]}


rrr4 <- rrr4 %>% #standarize variable values
  mutate(native_speaker = case_when(lab %in% c("brewer", "calvillo", "carruth", "crowell", "lange", "vandellen", "zerhouni", "otgaar",
                                               "rentzsch", "ringos", "schlinkert", "stamos", "wolff", "yusainy") ~ "yes", #these labs have only native speakers
                                    native_speaker %in% c("1", "") ~ "yes", #a 1 or an empty space "" corresponds to native speaker
                                    native_speaker == "unknown" | is.na(native_speaker) ~ NA_character_,
                                    native_speaker == "Swedish/" ~ "yes", #Tinghog is only lab where the the dominant category were not *English* native speakers, although they did the experiment in English (see, RRR4 p.549)
                                    TRUE ~ "no"), #any remainging values e.g., 2, 7, 3, 5 etc are non-native speakers
         Task = ifelse(Task %in% c("2", "E"), "easy", "hard"), #{2 == easy, 1 == hard [deduced from comparing https://osf.io/t3mns/ values with individual lab datasets]}
         Task = ifelse(Task == "easy", "control", "treatment"),
         age = ifelse(age == "0", NA_character_, age)) #zero age in Schlinkert == missing value [personal communication]


#Exchange lab names for codes
rrr4$lab <- factor(rrr4$lab, levels = labs4,
                   labels = paste0("L", formatC(1:length(labs4), width = 2, format = "d", flag = "0")))
lab_id4 <- data.frame(lab = labs4, id = unique(rrr4$lab)) #for keeping track of which lab is which in an appendix


rrr4 <- rrr4 %>%
  select(lab,
         subject_number = Subject, #improve some names
         condition = Task, #{easy, hard}
         age, #{17:48}
         native_speaker, #{yes, no, NA} - NB. For tinghog lab 'yes' is native Swedish speakers
         race = Race, #Evans lab only {NA, 1, 2, 3, 4, 6}
         acc_overall_LetE = Acc.Overall.LetE, #letter E accuracy {0.47:1}
         acc_overall_MSIT = Acc.Overall.MSIT, #MSIT accuracy {0.005: 1, NA}
         RTV_MSIT_mean_incongruent = ExGauss.I.RTVar.MSIT, #primary DV, {0.018:1.271} mean RTV on incongruent trials, see p. 551 of RRR4, and readme.txt in https://osf.io/nb8uj/
         RT_MSIT_mean_incongruent = I_1_MeanRT.MSIT) #secondary DV, {0.288:1.879} mean RT on incongruent trials, used for outlier excluesions, see appendix of RRR4 and https://osf.io/v79xp/


#save intermediate dataset
# write.csv(rrr4, "../data/03_cleaned-raw-data/rrr4.csv", row.names = FALSE)

#******************************************
#[5] RRR5 ----
#******************************************
##Raw data from Registered Replication Report 5
##Main OSF page: https://osf.io/s3hfr/
##Publication: https://doi.org/10.1177/1745691616664694
##Direct link to raw data files https://osf.io/dvaz7/
##Codebook: No codebook. The Qualtrics survey script (https://osf.io/rxwqv/) and can be used to determine which variables are which

##Additional comments: The exclusion indicator variable used by RRR5 does not separate the reasons for why someone was excluded,
#even though such and explantion was part of their pre-registered protocol (https://osf.io/2h6tf/)

#*********************
#library(dplyr)

#Load data
files5 <- list.files("../data/02_raw-data/RRR5", full.name = TRUE, pattern = ".csv") #folder with all raw data files for rrr5
labs5 <- unlist(lapply(strsplit(files5, split = "_|/"), function(x) x[[6]])) #list of labs to index data
rrr5 <- lapply(files5, read.csv, stringsAsFactors = FALSE, colClasses = "character") #read raw data, as characters for robustness
names(rrr5) <- labs5

#Dependent variable items
exit_items <- quos(Q2.2_1,Q2.3_3,Q2.4_1,Q2.5_2,Q2.6_2,Q2.7_4,Q2.8_2,Q2.9_4,Q2.10_1,Q2.11_4,Q2.12_3,Q2.13_4) #quote items for later use in dplyr verbs, list from Finkel_Analysis.R https://osf.io/mp3s7/
neglect_items <- quos(Q2.2_4,Q2.3_2,Q2.4_2,Q2.5_4,Q2.6_1,Q2.7_2,Q2.8_4,Q2.9_2,Q2.10_4,Q2.11_1,Q2.12_1,Q2.13_1) #quote items for later use dplyr verbs, list from Finkel_Analysis.R https://osf.io/mp3s7/
voice_items <- quos(Q2.2_2,Q2.3_10,Q2.4_3,Q2.5_1,Q2.6_4,Q2.7_1,Q2.8_3,Q2.9_3,Q2.10_2,Q2.11_2,Q2.12_4,Q2.13_3) #note that these are now a list, to convert to string vector use sapply(vector, quo_name)
loyalty_items <- quos(Q2.2_3,Q2.3_1,Q2.4_4,Q2.5_3,Q2.6_3,Q2.7_3,Q2.8_1,Q2.9_1,Q2.10_3,Q2.11_3, Q2.12_2,Q2.13_2)


#Formatting function (select and rename variables, minor cleaning)
format_rrr5 <- function(loaded_csv){ #function for selection of raw data
  loaded_csv %>%
    slice(-1) %>% #Remove first row which are alternative column names. Note that RRR5 drops the two first rows (https://osf.io/mp3s7/), but this is because they do not read first row as variable names
    mutate(subject_number = row_number()) %>% #first add participant ID to each dataset
    select(subject_number,
           condition = Q7.1, #condition item, {1 = high, 2 = low}
           years_dating = Q5.6,#U2. Number of years participant had been dating their partner, see qualtrix code https://osf.io/rxwqv/
           months_dating = Q5.7, #U2. Months the participant had been dating their partner, see qualtrix code https://osf.io/rxwqv/
           age = Q5.2, #age item, see Finkel_Analysis.R https://osf.io/mp3s7/
           year_in_school = Q5.3, #E4. 'your year in school'{1-4, 5 = "other"}, see qualtrix code https://osf.io/rxwqv/
           in_rom_rel = Q5.4, #U1. in a romantic relationship {1 = current, 2 = past}
           status_of_rel = Q5.5, #U6. {1 = friendship, 2= dating casually, 3=dating regularly, 4 = dating steadily, 5= engaged or married, 6 = other}
           rel_exclusive = Q5.8, #U7. Relationship exclusivity, 4 categories [cumbersome and long names]
           days_per_month = Q5.9, #U8, how many days per month do you see your partner on average
           partner_within_60m = Q5.10, #U9. live within 60 miles? {1 = yes, 2 = no}
           !!! exit_items, #DV items, unquoted (and spliced into list) by !!! (see item list above)
           !!! neglect_items, #For all items: [0 = Not at all likely to react this way, 8 = extremely likely to react this way]
           !!! voice_items,
           !!! loyalty_items) %>%
    mutate_all(as.numeric) %>%  #transform qualtrix data
    filter(!is.na(condition)) #remove participants with computer error, or rows with error in loading data
}


rrr5 <- lapply(rrr5, format_rrr5)
rrr5 <- bind_rows(rrr5, .id = "lab") #combine into one dataframe

#clarify variable names and content
exit_new_names <- paste0("exit_item_", 1:length(exit_items))
neglect_new_names <- paste0("neglect_item_", 1:length(neglect_items))
voice_new_names <- paste0("voice_item_", 1:length(voice_items))
loyalty_new_names <- paste0("loyalty_item_", 1:length(loyalty_items))

rrr5 <- rrr5 %>% #update DV variable names and clarify content in variables
  rename_at(vars(!!!exit_items), ~exit_new_names) %>% #rename all the DV items to more informative names
  rename_at(vars(!!!neglect_items), ~neglect_new_names) %>%
  rename_at(vars(!!!voice_items), ~voice_new_names) %>%
  rename_at(vars(!!!loyalty_items), ~loyalty_new_names) %>%
  mutate(condition = ifelse(condition == 1, "treatment", "control"), #clarifications
         in_rom_rel = ifelse(in_rom_rel == 1, "current", "past"),
         status_of_rel = case_when(status_of_rel == 1 ~ "friendship",
                                   status_of_rel == 2 ~ "dating_causally",
                                   status_of_rel == 3 ~ "dating_regularly",
                                   status_of_rel == 4 ~ "dating_steadily",
                                   status_of_rel == 5 ~ "engaged_or_married",
                                   status_of_rel == 6 ~ "other"),
         partner_within_60m = ifelse(partner_within_60m == 1, "yes", "no"),
         months_dating = years_dating*12 + months_dating) %>%  #convert dating time into one variable instead of two
  select(-years_dating)

#add indicators of % items missing of each DV, used for u3 later
rrr5$NA_exit_percent <- rowSums(is.na(rrr5[exit_new_names])) / length(exit_new_names) * 100
rrr5$NA_neglect_percent <- rowSums(is.na(rrr5[neglect_new_names])) / length(neglect_new_names) * 100
rrr5$NA_voice_percent <- rowSums(is.na(rrr5[voice_new_names])) / length(voice_new_names) * 100
rrr5$NA_loyalty_percent <- rowSums(is.na(rrr5[loyalty_new_names])) / length(loyalty_new_names) * 100

#Exchange lab names for codes
rrr5$lab <- factor(rrr5$lab, levels = labs5,
                   labels = paste0("L", formatC(1:length(labs5), width = 2, format = "d", flag = "0")))
lab_id5 <- data.frame(lab = labs5, id = unique(rrr5$lab)) #for keeping track of which lab is which in an appendix

#save intermediate dataset
# write.csv(rrr5, "../data/03_cleaned-raw-data/rrr5.csv", row.names = FALSE)

# #Final variables
# [1] "lab"               {L1:L16}
# [2] "subject_number"
# [3] "condition"         {control, treatment}
# [4] "months_dating"     {0:326}
# [5] "age"               {17:62}
# [6] "year_in_school"    {1:5}
# [7] "in_rom_rel"        {current, past, NA}
# [8] "status_of_rel"     {dating_steadily, engaged_or_married, dating_regularly, dating_casually, other, friendship}
# [9] "rel_exclusive"     {1:4, NA}
# [10] "days_per_month"    {0:31}
# [11] "partner_within_60m" {no, yes}
#DV1
# [12] "exit_item_1"       {0:8, NA}
# [13] "exit_item_2"       {0:8}
# [14] "exit_item_3"       {0:8}
# [15] "exit_item_4"       {0:8}
# [16] "exit_item_5"       {0:8}
# [17] "exit_item_6"       {0:8}
# [18] "exit_item_7"       {0:8}
# [19] "exit_item_8"       {0:8}
# [20] "exit_item_9"       {0:8}
# [21] "exit_item_10"      {0:8}
# [22] "exit_item_11"      {0:8}
# [23] "exit_item_12"      {0:8}
#DV2
# [24] "neglect_item_1"    {0:8, NA}
# [25] "neglect_item_2"    {0:8}
# [26] "neglect_item_3"    {0:8}
# [27] "neglect_item_4"    {0:8, NA}
# [28] "neglect_item_5"    {0:8}
# [29] "neglect_item_6"    {0:8}
# [30] "neglect_item_7"    {0:8}
# [31] "neglect_item_8"    {0:8}
# [32] "neglect_item_9"    {0:8}
# [33] "neglect_item_10"   {0:8}
# [34] "neglect_item_11"   {0:8}
# [35] "neglect_item_12"   {0:8}
#DV3
# [36] "voice_item_1"      {0:8, NA}
# [37] "voice_item_2"      {0:8}
# [38] "voice_item_3"      {0:8}
# [39] "voice_item_4"      {0:8}
# [40] "voice_item_5"      {0:8}
# [41] "voice_item_6"      {0:8}
# [42] "voice_item_7"      {0:8}
# [43] "voice_item_8"      {0:8, NA}
# [44] "voice_item_9"      {0:8}
# [45] "voice_item_10"     {0:8}
# [46] "voice_item_11"     {0:8}
# [47] "voice_item_12"     {0:8}
#DV4
# [48] "loyalty_item_1"    {0:8, NA}
# [49] "loyalty_item_2"    {0:8}
# [50] "loyalty_item_3"    {0:8}
# [51] "loyalty_item_4"    {0:8}
# [52] "loyalty_item_5"    {0:8}
# [53] "loyalty_item_6"    {0:8}
# [54] "loyalty_item_7"    {0:8}
# [55] "loyalty_item_8"    {0:8}
# [56] "loyalty_item_9"    {0:8}
# [57] "loyalty_item_10"   {0:8}
# [58] "loyalty_item_11"   {0:8}
# [59] "loyalty_item_12"   {0:8, NA}
#missingness indicators
# [60] "NA_exit_percent"   {0.00000000, 0.08333333}
# [61] "NA_neglect_percent" {0.00000000, 0.08333333}
# [62] "NA_voice_percent"  {0.00000000, 0.08333333}
# [63] "NA_loyalty_percent" {0.00000000, 0.08333333}


#******************************************
#[6] RRR6 ----
#******************************************
##Raw data from Registered Replication Report 6
##Main OSF page: https://osf.io/hgi2y/
##Publication: https://doi.org/10.1177/1745691616674458
##Direct link to raw data files https://osf.io/9j72u/
##Codebook: No codebook. Headers in data explain variables (double header row)

#Additional comment:

#*********************
#library(dplyr)

#load data
files6 <- list.files("../data/02_raw-data/RRR6", full.name = TRUE, pattern = ".csv")
labs6 <- unlist(lapply(strsplit(files6, split = "_|/"), function(x) x[[6]])) #list of labs to index data
rrr6 <- lapply(files6, read.csv, stringsAsFactors = FALSE, colClasses = "character") #colclasses set to character else SubjectId class differs between datasets
names(rrr6) <- labs6 #add lab indicators

#Initial cleaning of datasets (name issues due to double header row + remove excess columns in some datasets)
rrr6 <- lapply(rrr6, function(x) {
  names(x)[4:8] <- c("pen_correct_cartoon1", "pen_correct_cartoon2", "pen_correct_cartoon3", "pen_correct_cartoon4", "pen_correct_total") #correct headers with bad names due to double header row
  names(x)[9:14] <- c("rating_task1", "rating_task2", "rating_cartoon1", "rating_cartoon2", "rating_cartoon3", "rating_cartoon4") #correct headers with bad names due to double header row
  x <- x[-1, 1:22] #drop subheader row (1) and redundant columns in some datasets (#2 had explanations of exclusion, others are just from excel formatting)
}
)

#correct those column names that vary slightly between datasets (space or capitalization)
rrr6 <- lapply(rrr6, function(x) {
  names(x)[c(1, 2, 16)] <- c("subjectNo", "participantID", "Comprehension.of.cartoons.1...YES...0...NO")
  x
}
)

#Combine data
rrr6 <- bind_rows(rrr6, .id = "lab")

#select relevant variables
rrr6 <- rrr6 %>%
  select(lab,
         subject_number = subjectNo,
         condition = Condition.1..SMILE.0...POUT,
         pen_correct_cartoon1, #
         pen_correct_cartoon2,
         pen_correct_cartoon3,
         pen_correct_cartoon4,
         pen_correct_total,
         rating_cartoon1,
         rating_cartoon2,
         rating_cartoon3,
         rating_cartoon4,
         pen_correct_self_report = Self.reported.task.performance,
         comprehension_cartoons = Comprehension.of.cartoons.1...YES...0...NO,
         guessed_study_goal = Was.the.participant.aware.of.the.goal..1..YES..0...NO,
         age = Age,
         student = Student.1...YES...0...NO,
         study_field = Occupation...Field.of.study)

#clarify values in some variables
rrr6 <- rrr6 %>%
  na_if("") %>% #set empty values to NA
  mutate_at(vars(matches("correct|rating")), as.numeric) %>% #make numeric to be able to use in computations later
  mutate(condition = ifelse(condition == "1", "smile", "pout"), #clarifications
         comprehension_cartoons = ifelse(comprehension_cartoons == "1", "yes", "no"),
         guessed_study_goal = ifelse(guessed_study_goal == "1", "yes", "no"),
         student = ifelse(student == "1", "yes", "no")) %>%
  filter(!is.na(condition)) %>% #drop erranously read rows or subjects with computer error
  mutate_at(vars(matches("correct")), ~ifelse(is.na(.), 0, .)) #based on correct_cartoon_total, a few rows were coded as "" (NA) when pen not held correct instead of zero (0)
#warning about NAs introduced by coercion can be ignored since this is intentional from row above

#study_field has 786 unique responses in different languages. Too much to standardize fully. Below some general standardization.
rrr6 <- rrr6 %>% #to start, 786 unique values
  mutate(study_field = trimws(tolower(study_field)), #make all lower case and remove whitespace before and after text ->  679 unique
         study_field = ifelse(study_field %in% c(".", "-", "99", "(none given)", "unknown"), NA, study_field), #recode all missings to NA -> 674 unique
         study_field = gsub("/|&", " and ", study_field), #replace '/' or '&' by ' and ' (with spaces) -> 670 unique
         study_field = gsub("\\s+", " ", study_field)) #replace all spaces or multiple spaces  by a single space -> 665 unique


#Exchange lab names for codes
rrr6$lab <- factor(rrr6$lab, levels = labs6,
                   labels = paste0("L", formatC(1:length(labs6), width = 2, format = "d", flag = "0")))
lab_id6 <- data.frame(lab = labs6, id = unique(rrr6$lab)) #for keeping track of which lab is which in an appendix

#save intermediate dataset
# write.csv(rrr6, "../data/03_cleaned-raw-data/rrr6.csv", row.names = FALSE)

#Final variables
# [1] "lab"                    {L1:L17}
# [2] "subject_number"
# [3] "condition"              {smile, pout}
# [4] "pen_correct_cartoon1"   {0, 1}
# [5] "pen_correct_cartoon2"   {0, 1}
# [6] "pen_correct_cartoon3"   {0, 1}
# [7] "pen_correct_cartoon4"   {0, 1}
# [8] "pen_correct_total"      {0:4}
# [9] "rating_cartoon1"        {0:9, NA}
# [10] "rating_cartoon2"       {0:9, NA}
# [11] "rating_cartoon3"        {0:9, NA}
# [12] "rating_cartoon4"        {0:9, NA}
# [13] "pen_correct_self_report" {0.0:9.0}
# [14] "comprehension_cartoons"  {no, yes}
# [15] "guessed_study_goal"      {no, yes}
# [16] "age"                    {17:89}
# [17] "student"                {no, yes}
# [18] "study_field"            {665 options}

#******************************************
#[7] RRR7 ----
#******************************************
##Raw data from Registered Replication Report 7
##Main OSF repository: https://osf.io/scu2f/
##Publication: https://doi.org/10.1177/174569161769362
##Direct link to raw data files https://osf.io/6wvxa/
##Codebook: https://osf.io/4p8b8/


#Additional comments: On windows 7, using the default windows program to open the
#.zip file with data leads to incorrectly encoded names for non-English letters
#a better option is to use a program like 7zip

#*********************
#library(dplyr)

#load data
files7 <- Sys.glob("../data/02_raw-data/RRR7/*") #list.files cannot handle non-English letters on windows. Sys.glob permits UTF-8 file paths
files7 <- files7[grepl(".csv", files7)] #keep only .csv files
labs7 <- unlist(lapply(strsplit(files7, split = "_|/"), function(x) x[[6]])) #lab names for indexing
rrr7 <- lapply(files7, read.csv, stringsAsFactors = FALSE, colClasses = "character") #list with data per lab, read all vars. as characters for robustness
names(rrr7) <- labs7

#Formatting function
#note that this function refers to indices instead of variable names
#this is because at times there are differences in variable names between labs,
#but indices for vars stay the same [based on "Rand_Analysis.R" https://osf.io/m4b9n/]
#Compared to the codebook (https://osf.io/4p8b8/), indices are row-1 because the first row in the codebook is a header row

format_rrr7 <- function(loaded_csv){ #codebook RRR7 https://osf.io/4p8b8/
  loaded_csv %>% #
    slice(-1) %>% #remove first row, either NA or contains the question itself
    na_if("") %>% #set empty rows to NA
    mutate(missing_non_DV = rowSums(is.na(select(., c(31:33, 35:88))))) %>%  #U5. create var. indicating if missing value on any non-DV task item (cols 31:33, 35:88), see codebook for column numbers https://osf.io/4p8b8/
    mutate_all(as.numeric) %>% #convert characters to numeric ('convert qualtrix data'according to 'Rand_Analysis.R' https://osf.io/m4b9n/)
    mutate(subject_number = row_number()) %>% #add participant ID
    select(subject_number,
           contribution_TP = 18, #contribution in local currency time pressure condition [euro/dollar cents etc]
           contribution_FD = 24, #contribution in local currency forced delay condition [euro/dollar cents etc]
           year_born_qualtrix = 85, #needs to be recoded, 1 = 1920; 80 = 1999
           decision_time_TP = 22, #[seconds] 0 coded as NA in replication,  <10 = 'compliant', see "Rand_Analysis.R", line 292 https://osf.io/m4b9n/
           decision_time_FD = 28, #[seconds] 0 coded as NA in replication,  >=10 = 'compliant', see "Rand_Analysis.R", line 294 https://osf.io/m4b9n/
           comprehend_1 = 31, #correct answer = 9, see codebook
           comprehend_2 = 32, #correct answer = 1, see codebook
           participated_identical_exp = 67, #U3. "to what extent have you participated in studies like this one before"  {1 = nothing like this scenario, 5 = exactly this scenario}
           participated_any_exp = 68, #U7
           participated_paid_exp = 69, #U8
           participated_online_exp = 70, #U9
           participated_deceitful_exp = 71, #U10
           missing_non_DV, #U4 {number of missings on non-DV items}
           heard_about_study = 78, #U5 'did you hear anything abouth the study from previous participants?' {1 = yes, 2 = no}
           know_other_part = 88) %>%  #U6. 'how many participants in the room do you know?'
    filter(!(is.na(contribution_FD) & is.na(contribution_TP))) %>% #remove ss. with missing on DV. Additional vars. [19/25] in lines 7/8 in "Rand_Analysis.R" are superfluous. NB! must be done to be able to create condition variable (two lines down)
    mutate(condition = ifelse(is.na(contribution_FD), "time pressure", "forced delay"), #after filter, ss. with missing on one DV belong to other group
           contribution = coalesce(contribution_TP, contribution_FD), #coalesce(a, b) = ifelse(is.na(a), b, a)
           decision_time = coalesce(decision_time_TP, decision_time_FD)) %>% #combine into one variable
    mutate(contribution = contribution/max(contribution, na.rm = TRUE)*100) %>% #recode contribution as a percentage of max contribution in a lab, see RRR7 paper and Rand_Analysis.R line 310
    select(-c(contribution_TP, contribution_FD, decision_time_TP, decision_time_FD)) #drop redundant variables
}

#before_removing_NA_DV <- sum(unlist(lapply(rrr7, nrow))) #how many rows total across labs before removing ss with missing DV

#Format raw data
rrr7 <- lapply(rrr7, format_rrr7)
#warnings can be ignored, they are about unused character columns converted to numeric NAs

#after_removing_NA_DV <- before_removing_NA_DV - sum(unlist(lapply(rrr7, nrow))) #3771 - 3669 = 102 subjects had missing on DV

#combine into one dataframe and some clarifications
rrr7 <- bind_rows(rrr7, .id = "lab") %>% #combine into one dataframe, with indicator variable with lab names
  mutate(missing_non_DV = ifelse(missing_non_DV > 0, "yes", "no"),
         age = 2015 - (year_born_qualtrix + 1919), #conversion formula from lines 16/17 in "Rand_Analysis.R" https://osf.io/m4b9n/
         comprehend = ifelse(comprehend_1 == 9 & comprehend_2 == 1, "yes", "no"),
         heard_about_study = ifelse(heard_about_study == 1, "yes", "no"),
         decision_time = case_when(decision_time == 0 ~ NA_character_,
                                   is.na(decision_time) ~ NA_character_,
                                   condition == "time pressure" & decision_time < 10 ~ "compliant",
                                   condition == "forced delay" & decision_time >= 10 ~ "compliant",
                                   TRUE ~ "noncompliant")) %>%
  select(lab, subject_number, condition, everything(), -c(year_born_qualtrix, comprehend_1, comprehend_2)) #drop redundant variables and order others


#Exchange lab names for codes
rrr7$lab <- factor(rrr7$lab, levels = labs7,
                   labels = paste0("L", formatC(1:length(labs7), width = 2, format = "d", flag = "0")))
lab_id7 <- data.frame(lab = labs7, id = unique(rrr7$lab)) #for keeping track of which lab is which in an appendix

#save intermediate dataset
# write.csv(rrr7, "../data/03_cleaned-raw-data/rrr7.csv", row.names = FALSE)

#NB! note that missing on DV have already been excluded by necessity (to be able to code condition variable)
#All other datasets this will take place when computing the multiverse (but shouldn't make a difference since such
#values must always be removed). 102 subjects were removed in total for this reason.

# #Final variables
# [1] "lab"                     {L1:L21}
# [2] "subject_number"
# [3] "condition"               {time pressure, forced delay}
# [4] "participated_identical_exp" {1:5}, 1 = nothing like this scenario, 5 = exactly this scenario
# [5] "participated_any_exp"      {0:150, NA}
# [6] "participated_paid_exp"     {0:150, NA}
# [7] "participated_online_exp"   {0:50}
# [8] "participated_deceitful_exp" {0:80, NA}
# [9] "missing_non_DV"          {no, yes}
# [10] "heard_about_study"       {no, yes}
# [11] "know_other_part"         {0:50}
# [12] "contribution"            {0:100} %contribution of maximum possible
# [13] "decision_time"           {compliant, noncompliant}
# [14] "age"                     {16:90}
# [15] "comprehend"              {no, yes}


#******************************************
#[8] RRR8 ----
#******************************************
##Raw data from Registered Replication Report 8
##Main OSF repository: https://osf.io/k27hm/
##Publication: https://doi.org/10.1177/1745691618755704
##Direct link to raw data files included in RRR8  primary analysis: https://osf.io/p2myk/
##For direct links to RRR8 excluded data see below

##Additional comments:
##file named 'complete data' in each lab is after exclusions. In
##windows 10 using the inbuilt tool to extract the .zip with data
##files leads to some being incorrectly encoded (a tool like 7-zip
##is better). Each dataset is encoded in their local language
##which leads to some encoding issues (see code). I download both the
##data included and excluded by RRR8 and combine the two in the code below.
##RRR8 excludes several labs for too small sample size (see comments on OSF
##links below) which we include.

#Direct links to excluded data (to be combined with included from link above):
#O'Donnel: https://osf.io/vg7ss/ [all ss. in main dataset]
#Rentzelas: https://osf.io/cwgkp/ [all ss. in main dataset, only excluded on age] [not included original primary analysis]
#Boot: https://osf.io/z4cpd/ [link in paper incorrect, only to pre-reg. this link is correct] Download folder Boot_exclusions and file Boot_excel_exclusions
#Aveyard: https://osf.io/z38mb/ [file renamed manually, otherwise identical name to others]
#Azcel: https://osf.io/z2vjc/
#Braithwwaite: https://osf.io/84gdn/ [no excluded ss.]
#Baskin: https://osf.io/2fpev/
#Birt: https://osf.io/3pg28/ [not included original primary analysis]
#Karpinski: https://osf.io/wfn3s/ [file manually renamed, otherwise identical to Aveyard]
#Wood: https://osf.io/vznyt/ [no exlusion except cpu crashes] [not included original primary analysis]
#Cramwinckel: https://osf.io/a9edq/ [not included original primary analysis]
#chartier: https://osf.io/n3g84/ [not included original primary analysis]
#DiDonato: https://osf.io/hk9pg/ [not included original primary analysis, paper link to pre-registration rather than repository. Link here correct.]
#klein: https://osf.io/gejq5/
#Vazire/finnigan: https://osf.io/wgx4r/
#bialobrzeska: https://osf.io/r547x/
#Keller: https://osf.io/8dmyu/
#Koppel: https://osf.io/xa7h5/
#Legal: https://osf.io/s24nz/ [not included original primary analysis]
#Massar: https://osf.io/h9f4y/ [not included original primary analysis]
#Mcbee/Chambers/Coulthard: https://osf.io/hva2k/ [not included original primary analysis. Private OSF, no access]
#McLatchie: https://osf.io/y3uht/
#Newell: https://osf.io/t6nqa/
#Ozdogru: https://osf.io/ctkup/ [sent excluded data per email] [not included original primary analysis]
#Phillip: https://osf.io/7yqgt/
#Krahmer: https://osf.io/u85ah/ [all ss. in main dataset] [not included original primary analysis.]
#roer: https://osf.io/npa7h/ [not included original primary analysis]
#ropovik: https://osf.io/gzy3a/
#sackett: https://osf.io/n2974/ [not included original primary analysis. No data on OSF page. Can't get excluded data. However, excluded data appears to be in main dataset]
#Saunders: https://osf.io/wpyg4/
#schulte-Mecklenbeck: https://osf.io/5pg6r/
#Shanks: https://osf.io/cz2dq/ [folder 'shanks_excluded'] [not included original primary analysis]
#Steele: https://osf.io/ubj78/
#Steffens: https://osf.io/xns26/
#Susa: https://osf.io/aj5aa/
#Tamayo: https://osf.io/ry7nw/
#tong: https://osf.io/x8mg9/ #[not included original primary analysis]
#Harreveld: https://osf.io/zx928/ [only automatic exclusions]
#Willis: https://osf.io/enajs/ [not included original primary analysis]
#Zheng: https://osf.io/wujkd/ [No uploaded data on OSF page, no email response. Excluded subjects not in main dataset.] [not included original primary analysis]

#*********************
#library(dplyr)
#library(readxl)

#Included data
folders <- Sys.glob("../data/02_raw-data/RRR8/included/*") #list_files cannot handle non-English letters. Sys.glob permits UTF-8 file paths
labs8 <- unlist(lapply(strsplit(folders, split = "/|_"), function(x) x[[7]])) #save lab names for indexing
labs8[c(4, 17, 23, 27)] <- c("Bialobrzeska","Legal", "Ozdogru","Roer") #change these names to only use English letters for matching with excluded dataset later on
files <- lapply(folders, function(x) Sys.glob(paste0(x, "/*"))) #load data file names for each lab as list
files <- lapply(files, function(x) x[grepl("*.csv", x) & !grepl("complete", x)]) #remove non .csv files and the 'complete' data .csv file

rrr8 <- lapply(files, function(x) lapply(x, read.csv, stringsAsFactors = FALSE, #set all columns to character since R sometimes guesses different class on same column
                                          encoding = "UTF-8", colClasses = "character")) #UTF encoding to parse non-English content better
#Ignore warnings. Fixing them must be done manually and leads to switched columns in data, and e.g., McLatchie had no corrections made by RRR8 (see .R-file in McLatchie's https://osf.io/y3uht/)

rrr8[[1]] <- lapply(files[[1]], read.csv, stringsAsFactors = FALSE, colClasses = "character") #[[1]] does not load correctly with UTF-8
rrr8[[32]] <- lapply(files[[32]], read.csv, stringsAsFactors = FALSE, colClasses = "character") #[[1]] does not load correctly with UTF-8


rrr8 <- lapply(rrr8, bind_rows) #dplyr::bind_rows fills any missing columns in a df with NAs, instead of throwing an error like do.call(rbind, list) would
names(rrr8) <- labs8


#Excluded data indicators [i.e., excel sheets showing who was excluded and for what reason: some of them received through personal communcation (email)]
##NB! Excel reader cannot handle non-English characters -> Had to rename R"oer file manually to Roer
#The following labs lack an excel_exclusion sheet: Mcbee, krahmer, o'donnel, rentzelas, wood. However, they all either a) didn't do exclusions, or excluded ss are in main dataset

excluded_list_files <- list.files("../data/02_raw-data/RRR8/exclusion_sheets", pattern = ".xlsx", full.names = TRUE)
ex_sheet_names <- unlist(lapply(strsplit(excluded_list_files, split = "/|_"), function(x) x[[8]]))
ex_data <- lapply(excluded_list_files, read_excel, col_types = "text", skip = 1) #load data. skip double header row
names(ex_data) <- ex_sheet_names #set lab names
names(ex_data$Willis)[7] <- "Aware of the other priming condition"  #fix column name in one dataset where this was missing

excluded_and_reason <- bind_rows(ex_data, .id = "lab") #combine into one dataframe, with an indicator for lab (=list-names). Missing columns between datasets filled with NAs.

excluded_and_reason <- excluded_and_reason %>%
  mutate_at(vars(`Did not follow instructions`, `Did not complete all tasks`, `Aware of the other priming condition`, #convert to numeric variables
              `Computer or experimenter error`, `Other (please explain in the notes/explanations column)`), as.numeric) %>% #for next line computation
  mutate(non_age = rowSums(select(., 6:10), na.rm = TRUE)) #Create indicator that shows if ss were excluded for any reason other than age
#these reasons are: 'did not follow instructions', 'did not complete all tasks', 'aware of other priming condition', 'computer of experimenter error', 'other'


excluded_and_reason <- excluded_and_reason %>%
  filter(non_age == 0) %>% #subjects who were excluded because of age. Match to excluded data (check subject_numbers and see what they look like)
  filter(!is.na(Subject_Number)) %>% #drop Braitwathe who made no exclusions, but has an exclusion sheet
  filter(lab != "Zheng") %>% #drop Zheng lab, who has an exclusion sheet, but did not upload the excluded data
  mutate(Subject_Number = ifelse(lab == "Legal", #this lab uses the file names instead of subject ID in the excel exclusions sheet
                                 unlist(lapply(strsplit(.$Subject_Number, split = "_"), function(x) x[[1]])), #extract the first value from filenames == ID
                                 Subject_Number)) #do nothing with other lab subject IDs


#The data above is a dataframe with lab, subject_number, and reason for exclusion
#this can be matched to the data in the excluded folder to know which subjects should be included again

#excluded raw data
excluded_folders <- Sys.glob("../data/02_raw-data/RRR8/excluded/*")
ex_folder_names <- unlist(lapply(strsplit(excluded_folders, split = "/|_"), function(x) x[[7]])) #NB! For convenience I manually renamed the r"oer folder to roer
excluded_folders <- excluded_folders[which(ex_folder_names %in% unique(excluded_and_reason$lab))] #only keep folders from which we have subject excluded due to age

excluded_data_files <- lapply(excluded_folders, list.files, pattern = ".csv", full.name = TRUE) #list all files/folder
excluded_data <- lapply(excluded_data_files, function(x) lapply(x, read.csv, stringsAsFactors = FALSE, #set all columns to character since R sometimes guesses different class on same column
                                                               encoding = "UTF-8", colClasses = "character")) #UTF encoding to parse non-English content better
#Incomplete final line warning, see response 4 lines down.

excluded_data <- lapply(excluded_data, bind_rows) #dplyr::bind_rows fills any missing columns in a df with NAs, instead of throwing an error like do.call(rbind, list) would
names(excluded_data) <- ex_folder_names[which(ex_folder_names %in% unique(excluded_and_reason$lab))] #set list names, these correspond to the indices of the folder_names in unique...
excluded_data$Bialobrzeska <- excluded_data$Bialobrzeska[-17,]  #incomplete final line warning,  unable to fix this error.. deleting subject.
excluded_data$Newell <- NULL #Newell has no data, this was all 'deleted following our [Newell's] IRB guidelines' (participants < 18), so we can drop this lab

#we now have  a list of dataframes with excluded subjects for those labs where participants were excluded due to age

excluded_data <- bind_rows(excluded_data, .id = "lab") #combine data into one dataframe for matching

excluded_data <- excluded_data %>% #combine, keep only subjects excluded based on age
  semi_join(., excluded_and_reason, by = c("lab", "subjID" = "Subject_Number"))  #semi_join(x, y) matches by = c() and keeps rows in x with matches in y, and all x-columns. Drops duplicates.

#some discrepancies in reported excluded and uploaded excluded data. e.g., Cramwinckel ID = 72 excluded for age > 24, but data for ss not uploaded
#Ozdogru also does not include ID 1078 (excluded for age > 24) in the uploaded excluded data. These thus cannot be included (since the data doesn't exist)

excluded_data$lab <- tolower(excluded_data$lab) #make lowercase to facilitate matching between datasets
names(rrr8) <- tolower(names(rrr8)) #make lowercase to facilitate matching between datasets

#Combine excluded and included data into one list
excluded_data <- excluded_data %>%
  split(.$lab) %>% #first split excluded data into list again, by lab
  lapply(., select, -lab) #drop the lab variable inside of the datesets, this otherwise creates problems when we wish to combine all data into one dataset later on

rrr8_matching_excluded <- rrr8[names(rrr8) %in% names(excluded_data)] #extract all labs to add excluded data to
rrr8_nonmatching_excluded <- rrr8[!names(rrr8) %in% names(excluded_data)] #keep also the ones without any added excluded data
rrr8 <- mapply(bind_rows, rrr8_matching_excluded, excluded_data, SIMPLIFY = FALSE, USE.NAMES = TRUE) #match each list element so that the correct lab gets its excluded data appended at the end of its dataframe. bind_rows keeps all columns
rrr8 <- c(rrr8, rrr8_nonmatching_excluded) #recombine with the datasets that didn't have any excluded data (by age)

#At this point we have a list of 40 datasets, having combined the excluded data (on age) where available with the corresponding dataset's included data


##Initial cleaning: switched variables
#Only variables which we will use are corrected
#Information on which variables need to be switched found in the R-files in each lab's data folder https://osf.io/p2myk/)

for(i in seq_along(rrr8)){ #correct swapped columns in the different datasets
  if(names(rrr8)[i] == "aczel"){ #For Aczel
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(sex_r = language, #Swap misnamed columns, see Meta-analysis/Azcel_data/Azcel_univ.R in https://osf.io/p2myk/
             language = sex_r,
             year = major,
             major = year)
  } else if(names(rrr8)[i] == "klein"){ #These do not seem to have been corrected in the RRR? See Meta-analysis/klein_data/Klein_univ.R in https://osf.io/p2myk/
    rrr8[[i]][!is.na(as.numeric(rrr8[[i]]$language)), c("age", "year", "language", "major", "sex_r")] <- rrr8[[i]][!is.na(as.numeric(rrr8[[i]]$language)), c("language", "age", "year", "sex_r", "major")] #switch columns only for these rows 71, 80 etc
  } else if(names(rrr8)[i] == "legal"){ #for legal
      eng_corr <- filter(rrr8[[i]], subjID == 24) #for Legal one subject was English and had non-switched variables
      rrr8[[i]] <- filter(rrr8[[i]], !subjID == 24) #temporarily remove before correcting rest
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(major = sex_r, #Swap misnamed columns, see Meta-analysis/legal_data/legal_univ.R
             sex_r = major,
             year = age,
             age = language,
             language = year)
      rrr8[[i]] <- rbind(rrr8[[i]], eng_corr) #re-add temporarily removed subject
  } else if(names(rrr8)[i] %in% c("cramwinckel", "harreveld", "krahmer")){ #For Cramwinckel, Herreveld, and Krahmer
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(major = year, #Swap misnamed columns, see Meta-analysis/krahmer_data/krahmer_univ.R and corresponding files for other labs in https://osf.io/p2myk/
             sex_r = major,
             year = age,
             age = language,
             language = sex_r)
    } else if(names(rrr8)[i] == "koppel"){ #for koppel
      rrr8[[i]] <- rrr8[[i]] %>%
        rename(major = language,
               language = sex_r,
               sex_r = major)
  } else if(names(rrr8)[i] %in% c("bialobrzeska", "ropovik")){ #For Bialobrzeska, Roer, Ropovik and Schulte
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(age = sex_r, #Swap misnamed columns, see Meta-analysis/ropovik_data/ropovik_univ.R and corresponding files for other labs in https://osf.io/p2myk/
             sex_r = age,
             language = major,
             major = language)
  } else if(names(rrr8)[i] == "schulte-mecklenbeck"){
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(age = sex_r, #Swap misnamed columns, see Meta-analysis/roer_data/roer_univ.R and corresponding files for other labs in https://osf.io/p2myk/
             sex_r = age)
  } else if (names(rrr8)[i] == "roer"){
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(age = sex_r, #Swap misnamed columns, see Meta-analysis/roer_data/roer_univ.R and corresponding files for other labs in https://osf.io/p2myk/
             sex_r = age)
    rrr8[[i]][c(192:200), c("major", "language")] <- rrr8[[i]][c(192:200), c("language", "major")]
  } else if(names(rrr8)[i] == "ozdogru"){ #For Ozdugru
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(sex_r = language, #Swap misnamed columns, see Meta-analysis/ozdogru_data/ozdogru_univ.R and corresponding files for other labs in https://osf.io/p2myk/
             language = age,
             age = year,
             year = sex_r)
  } else if(names(rrr8)[i] %in% c("tamayo", "willis")){ #for Tamayo and Willis
    rrr8[[i]] <- rrr8[[i]] %>%
      rename(age = year,
             major = sex_r,
             language = age,
             sex_r = major,
             year = language)
  } else if(names(rrr8)[i] == "massar"){ #for Massar
    rrr8[[i]][rrr8[[i]]$major %in% c("man", "vrouw"), c("age", "year", "language", "sex_r", "major")] <- rrr8[[i]][rrr8[[i]]$major %in% c("man", "vrouw"), c("language", "age", "sex_r", "major", "year")] #swap some columns for subjects with "man" or "vrouw" in 'major' column
    rrr8[[i]][!is.na(as.numeric(rrr8[[i]]$sex_r)), c("age", "sex_r")] <- rrr8[[i]][!is.na(as.numeric(rrr8[[i]]$sex_r)), c("sex_r", "age")] #swap columns for some subjects with age in sex_r column
    rrr8[[i]] <- rrr8[[i]] %>%
      mutate(sex_r = ifelse(startsWith(sex_r, "m"), "male", "female")) #recode/translate gender to be consistent within variable
  }
  else if(names(rrr8)[i] %in% c("steele", "tong")){ #for steel and Tong, not fixed by RRR from what I can see?
    rrr8[[i]][!is.na(rrr8[[i]]$triv31_raw), c("age", "language", "major", "sex_r", "year", "link_yn", "thinking_yn")] <- rrr8[[i]][!is.na(rrr8[[i]]$triv31_raw), c("triv31_raw", "triv32_raw", "triv31_correct", "triv32_correct", "triv33_raw", "language", "year")]
    rrr8[[i]] <- rrr8[[i]] %>%
      mutate(link_yn = case_when(startsWith(link_yn, "yes") ~ "yes", #these variables are smushed together with the link_text and thinking_text variables
                                 startsWith(link_yn, "no") ~ "no",
                                 TRUE ~ as.character(link_yn)),
             thinking_yn = case_when(startsWith(thinking_yn, "yes") ~ "yes",
                                     startsWith(thinking_yn, "no") ~ "no",
                                     TRUE ~ as.character(thinking_yn)))
  }
}

#warning messages about 'NAs introduced by coercion' can be ignored, these are intentional

trivia_correct_cols <- syms(paste0(rep("triv", 30), seq(1:30), rep("_correct", 30))) #'syms' just allows vector to be read by dplyr
trivia_raw_cols <- syms(paste0(rep("triv", 30), seq(1:30), rep("_raw", 30))) #by transforming into a list of symbols

rrr8 <- rrr8 %>%
  bind_rows(rrr8, .id = "lab") %>% #combine into one dataset
  distinct() %>% #For some reason the above is duplicating each row, not sure why, so will just remove any duplicates..
  mutate(subject_number = coalesce(subjID, #standardize subject ID name, all other relevant variables names already standardized
                                   Subject.ID))

Encoding(rrr8$age) <- "latin1" #by changing the encoding to latin1 the '\xc3' character in Klein's data -> � which can be cleaned

rrr8 <- rrr8 %>% #standardize within variables
  na_if("") %>% #convert all empty cells to NA
  filter(!is.na(prime_code)) %>% #drop all empty rows, or with computer problesm, in the data
  mutate(age = ifelse(age == "dix-neuf ans", "19", age), #fix age written as letters for two subjects of Legal
         age = as.numeric(gsub("[^0-9]", "", age)), #replace any extra text in the age variable by nothing. [^0-9] finds any character that is NOT a digit
         link_yn = case_when(grepl("^[yes]", link_yn) ~ "yes", #EDIT! "igen" and "ano" give problems. these variables are smushed together with the link_text and thinking_text variables
                             grepl(".{6,}", link_yn) ~ NA_character_, #after converting values that start with "yes" into only "yes", all longer strings are mixed up values
                             grepl("^[n|h]", link_yn) ~ "no", #remining strings beginning with n or h are all words for "no"
                             link_yn == " " ~ NA_character_,
                             is.na(link_yn) ~ NA_character_, #case_when requires that you specify what happens with NAs
                             TRUE ~ "yes"), #any remaining are all words for "yes"
         thinking_yn = case_when(grepl("^[yes]", thinking_yn) ~ "yes", #same approach as above
                                 grepl(".{6,}", thinking_yn) ~ NA_character_, #I conver these to NA since it is not clear which column is the true value
                                 is.na(thinking_yn) ~ NA_character_,
                                 grepl("^[n|h]", thinking_yn) ~ "no",
                                 TRUE ~ "yes"),
         prior_hool = case_when(grepl("^[n|h]", prior_hool) ~ "no", #everything that begins with n or h is a word for  "no"
                                is.na(prior_hool) ~ NA_character_,
                                TRUE ~ "yes")) #reminder are all words for "yes"

#NB! Because it is not clear in a few cases which response {yes, no} belongs to thinking_yn, link_yn, prior_hool [because values have been swapped between columns]
#and it is not dealt with by RRR8 (at least according to the code available with each dataset https://osf.io/p2myk/) I code these as missing in the code above

Encoding(rrr8$major) <- "latin1" #change the encoding of the variables to latin1 to be able to clean them in code below
Encoding(rrr8$language) <- "latin1"
Encoding(rrr8$year) <- "latin1"

rrr8 <- rrr8 %>% #standardization of very mixed variables {major, lanugage, year} in multiple languages
  mutate(major = gsub("\n", "", major), #decreases unique from 1938 to 1754
         major = trimws(tolower(major)), #unique -> 1361
         major = ifelse(major == "n/a", NA, major), #1360
         major = ifelse(!is.na(suppressWarnings(as.numeric(major))), NA, major), #some numerical values -> 1352 unique
         language = trimws(tolower(language)),
         language = gsub("\n", "", language), #358 unique -> 261
         year = gsub("\n", "", year),
         year = trimws(tolower(year)))  %>% #76 unique -> 56
  mutate(year = case_when(grepl("1|erste|primer", year) ~ "1", #56 unique -> 6 + NA
                          grepl("2|tweede|segundo|zweiten", year) ~ "2",
                          grepl("3|derde|tercer|dritten", year) ~ "3",
                          grepl("4|vier|cuarto", year) ~ "4 or higher",
                          grepl("not|no|geen|nie|je ne|0|de��il", year) ~ "not a student",
                          year == "student" ~ "1 or more", #one participant just had "studnet" instead of a year
                          TRUE ~ as.character(year)))




for(i in which(names(rrr8) %in% trivia_correct_cols)){ #RRR8 code missing trivia responses as incorrect (triv#_raw was coded as 0 when missing)
  rrr8[,i] <- ifelse(rrr8[,i-1] == 0, NA, rrr8[,i]) #here I instead recode them as missing, taking advantage of the fact that each trivi#_correct in preceded by its corresponding triv#_raw
}

rrr8 <- rrr8 %>% #final clarifications of some variables
  mutate_at(vars(!!!trivia_correct_cols), ~case_when(. == "False" ~ "FALSE", #standardize values
                                                     . == "True" ~ "TRUE",
                                                     is.na(.) ~ NA_character_,
                                                     TRUE ~ .)) %>%
  mutate(prime_code = ifelse(prime_code == "0", "hooligan", "professor"),
         student = ifelse(grepl("not a student", year), "no", "yes")) #add a student variable

#Exchange lab names for codes
labs8 <- unique(rrr8$lab)
rrr8$lab <- factor(rrr8$lab, levels = labs8,
                   labels = paste0("L", formatC(1:length(labs8), width = 2, format = "d", flag = "0")))
lab_id8 <- data.frame(lab = labs8, id = unique(rrr8$lab)) #for keeping track of which lab is which in an appendix


#select final variables
rrr8 <- rrr8 %>%
  select(lab, #{L1:L40}
         subject_number,
         condition = prime_code, #{hooligan, professor}
         age, #{0:222000}
         year, #{1, 2, 3, 4 or more, 1 or more, not a student,NA} - year in school
         language, #{257 unique} - E4, native langguage, language-specific}
         major, #{1700 unique} - major in college
         link_yn, #{yes, no} - debriefing, link between conditions
         thinking_yn, # {yes, no} - debriefing, think connection between conditions
         student, #{yes, no}
         prior_hool, #{NA, yes, no}
         !!! trivia_correct_cols) #{FALSE, TRUE, NA} - correct on trivia questions?

#remove duplicates that were not removed as intended in line 1306
rrr8 <- rrr8[!duplicated(rrr8),] #L01 subject_numbers 49087, 49261, 49594, 49909

#save intermediate dataset
# write.csv(rrr8, "../data/03_cleaned-raw-data/rrr8.csv", row.names = FALSE)


#******************************************
#[9] RRR9 ----
#******************************************
##Raw data from Registered Replication Report 9
##Main OSF page: https://osf.io/vxz7q/
##Publication: https://doi.org/10.1177/2515245918777487
##Direct link to raw data files https://osf.io/qegfd/
##Codebook: See code info in "Srull & Wyer (RRR)-meta-analysis.Rmd" in data folder (above link)

##Additional comments:
##Data collected together with RRR10. RRR9 drops labs with less than 100
##participants per condition, we do not. RRR9 exclusion criteria
##clarifications are available at https://osf.io/9afwn/

#*********************
#library(dplyr)

files <- list.files("../data/02_raw-data/RRR9", pattern = ".csv", full.names = TRUE)
labs9 <- unlist(lapply(strsplit(files, "_|/"), function(x) x[[6]]))
rrr9 <- lapply(files, read.csv, stringsAsFactors = FALSE, colClasses = "character", #if not loaded as character vars get different class
               strip.white = TRUE, na.strings = "") #contains empty strings of unknown length, so must use both these options
names(rrr9) <- labs9 #set list names for later conversion. This way we don't have to correct faulty lab-names in the data.

hostility <- paste0("ron.", c("kind", "considerate", "thoughtful", "hostile", "unfriendly", "dislikable")) #hostility variables
behavior <- paste0("behavior", c(2, 5, 8, 10, 13)) #behavior variables, taken from "Srull & Wyer (RRR)-meta-analysis.Rmd" in https://osf.io/qegfd/



format_rrr9 <- function(loaded_csv){ #see "Srull & Wyer (RRR)-meta-analysis.Rmd" file in https://osf.io/qegfd/
  loaded_csv %>%
    filter(!is.na(id)) %>% #remove empty rows. see "Srull & Wyer (RRR)-meta-analysis.Rmd" file in https://osf.io/qegfd/
    filter(!is.na(sw.prime.complete)) %>% #drop participants without information (incompletion, or some from Huntjens who appears to have pre-filtered on age?)
    select(id,
           sw.prime.cond, #[hostile, neutral]
           sw.prime.complete, #[complete, NA]
           reason.excl, #[out of age range, not following instructions, session too small, misadministration, incompletion RRR, more?]
           !!hostility, #DV1 avg. of vars
           !!behavior, #DV2 avg. of vars
           age,
           major,
           language,
           gender) %>% #[male, female, NA], paper drops all NA
    mutate_at(vars(age, !!hostility, !!behavior), as.numeric) %>%
    mutate(ron.kind = 10 - ron.kind, #these were reverse coded, see "Srull & Wyer (RRR)-meta-analysis.Rmd"
           ron.considerate = 10 - ron.considerate,
           ron.thoughtful = 10 - ron.thoughtful,
           sw.prime.cond = ifelse(sw.prime.cond == "hostile", "treatment", "control")) #recode for more consistency across datasets
}


rrr9 <- lapply(rrr9, format_rrr9)
rrr9 <- bind_rows(rrr9, .id = "lab") #create lab indicator


#RRR9 drops people with any missing on one of the traits (but take average)


rrr9 <- rrr9 %>% #Standardize variable content
  filter(!reason.excl %in% c("not following instructions", "misadministration")) %>% #exclude ss not following instructions/misadministration. There are some other exclusion reasons like "sheet not torn" but these belong to rrr10
  mutate(reason.excl = ifelse(is.na(reason.excl), "not excluded", reason.excl), #just so the next two lines will work
         student = ifelse(reason.excl == "non-student", "no", "yes"), #create separate variables for ease of use
         below_50_participants = ifelse(reason.excl == "session too small", "yes", "no"),
         language = tolower(language), #some inconsistencies in capital or non-capital letters
         major = tolower(trimws(major))) %>% #multiple languages and free text = many unique. 1043 unique become -> 926
  rename(subject_number = id, #rename a few variables for consistency with outher dataset and clarity
         condition = sw.prime.cond,
         original_excl_reason = reason.excl,
         sentence_completed = sw.prime.complete)

#ready and can be saved as an intermediate dataset


#Exchange lab names for codes
rrr9$lab <- factor(rrr9$lab, levels = labs9,
                   labels = paste0("L", formatC(1:length(labs9), width = 2, format = "d", flag = "0")))
lab_id9 <- data.frame(lab = labs9, id = unique(rrr9$lab)) #for keeping track of which lab is which in an appendix

#remove duplicate in L05, subject_number 159
rrr9 <- rrr9[!duplicated(rrr9),] #Different values in raven10 and hex60 of original data, but all other values match, probably data entry mistake in original data.

#save intermediate dataset
# write.csv(rrr9, "../data/03_cleaned-raw-data/rrr9.csv", row.names = FALSE)

#Final variables
# [1] "lab"                  {L1:L26}
# [2] "subject_number"
# [3] "condition"            {treatment, control}
# [4] "sentence_completed"   {complete, incomplete}
# [5] "original_excl_reason" {9 categories} - only information, not used further
# [6] "ron.kind"             {0:10, NA}
# [7] "ron.considerate"      {0:10, NA}
# [8] "ron.thoughtful"       {0:10, NA}
# [9] "ron.hostile"          {0:10, NA}
# [10] "ron.unfriendly"       {0:10, NA}
# [11] "ron.dislikable"       {0.0:10.0, NA}
# [12] "behavior2"            {0:10, NA}
# [13] "behavior5"            {0:10, NA}
# [14] "behavior8"            {0.0:10.0, NA}
# [15] "behavior10"           {0:10, NA}
# [16] "behavior13"           {0:10, NA}
# [17] "age"                  {12:61, NA}
# [18] "major"                {925 categories} - multple languages
# [19] "language"             {dutch, english, french, german, hebrew, hungarian, portuguese, swedish, turkish}
# [20] "gender"               {female, male, NA}
# [21] "student"              {no, yes}
# [22] "below_50_participants" {no, yes}




#******************************************
#[10] RRR10 ----
#******************************************
##Raw data from Registered Replication Report 10
##Main OSF page: https://osf.io/vxz7q/
##Publication: https://doi.org/10.1177/2515245918781032
##Direct link to raw data files: https://osf.io/fwnc2/ [file path: Meta-Analysis/Results/raw_data_corrected_MAAA.csv]
##codebook: In RRR data folder, see Meta-Analysis/mazar_srull_meta_analysis.R

##Additional comments: Data collected together with RRR9, same OSF repository.
##In RRR data folder, see Meta-Analysis/mazar_srull_meta_analysis.R lines 150-192
##for clarification on how RRR corrected the raw data. RRR10 exclusion criteria
##clarifications are available at https://osf.io/9afwn/

#*********************
#library(dplyr)

rrr10 <- read.csv("../data/02_raw-data/RRR10/Meta-Analysis/Results/raw_data_corrected_MAA.csv", stringsAsFactors = FALSE, encoding = "UTF-8") #read as UTF-8 because of some non-English letters

#NB. One lab "Verschuere" does not have ID for their participants in this dataset (but do for rrr9, strange)
rrr10[rrr10$lab.name == "Verschuere",] <- rrr10[rrr10$lab.name == "Verschuere",] %>% mutate(id = row_number()) #add subject id to the lab missing it


rrr10 <- rrr10 %>%
  filter(maz.cheat.cond == "cheat") %>% ##[cheat, no cheat], comparison of interest is Cheat-books vs. cheat-commandments, so only keep participants in 'cheat' condition. This also drops participants with NA on this condition.
  mutate(time_spent_IV_seconds = maz.prime.minutes * 60 + maz.prime.seconds, #participants were excluded if no books/commandments + reported spending no time on it
         listed_books_or_commandments = coalesce(maz.num.books, #combine these two into one variable. Note, variables not coded for some participants (NA)
                                                 num.commandments),
         intervention_completion = case_when(time_spent_IV_seconds == 0 & listed_books_or_commandments == 0 ~ "no list and no time spent",
                                             listed_books_or_commandments == 0 ~ "no list", #applies to cases where the above is false
                                             listed_books_or_commandments > 0 ~ "completed",
                                             TRUE ~ NA_character_),

         reason.excl = ifelse(is.na(reason.excl), "not excluded", reason.excl), #just so the next two lines will work
         below_50_participants = ifelse(reason.excl == "session too small", "yes", "no"),
         religiousness = rowMeans(.[c("religious1", "religious2", "religious3")], na.rm = TRUE), #RRR10 takes average of religious vars and removes NAs, see row 145 in Meta-Analysis/mazar_srull_meta_analysis.R
         religiousness = ifelse(is.nan(religiousness), NA, religiousness), #when someone has NA on all 3 religion var they get NaN on average, change to NA
         student = ifelse(reason.excl == "non-student", "no", "yes")) %>%
  select(lab = lab.name, #rename some variables for consistency across datasets
         subject_number = id,
         condition = maz.prime.cond, #{books, commandments}
         matrices_solved = num.boxes, #DV [0-20]
         intervention_completion,
         age,
         gender,
         original_excl_reason = reason.excl,
         language,
         religiousness,
         major,
         below_50_participants,
         student) %>%
  filter(!original_excl_reason %in% c("not following instructions", "misadministration", "Sheet not torn out", "more than 20 matrices", #participants who were excluded for not
                            "empty boxes field,sheet not torn out")) %>%  #following instructions or misadminstration
  filter(!grepl("torn out", original_excl_reason)) %>% #one coding contains a special letter (invisible with UTF-8 encoding) which can't be matched directly  [" Not specified if sheet was torn out or not"]
  mutate(major = tolower(major), #634 unique -> 565
         condition = ifelse(condition == "commandments", "treatment", "control")) #recode for increased consistency across datasets



#Exchange lab names for codes
labs10 <- unique(rrr10$lab)
rrr10$lab <- factor(rrr10$lab, levels = labs10,
                    labels = paste0("L", formatC(1:length(labs10), width = 2, format = "d", flag = "0")))
lab_id10 <- data.frame(lab = labs10, id = unique(rrr10$lab)) #for keeping track of which lab is which in an appendix

#save intermediate dataset
# write.csv(rrr10, "../data/03_cleaned-raw-data/rrr10.csv", row.names = FALSE)

#Final variables
# [1] "lab"                  {L1:L25}
# [2] "subject_number"
# [3] "condition"            {treatment, control}
# [4] "matrices_solved"      {0:20, NA}
# [5] "intervention_completion {completed, no list, no list and no time spent, NA}
# [6] "age"                  {12:61, NA}
# [7] "gender"               {male, female, NA}
# [8] "original_excl_reason" {7 categories remaining} - only for info, not used
# [9] "language"             {English, Hungarian, German, Portuguese, Hebrew, Swedish, French, Turkish, Dutch}
# [10] "religiousness"       {NA, 1-5}
# [11] "major"                {565 categories} - multple language
# [12] "below_50_participants" {no, yes}
# [13] "student"             {no, yes}




#******************************************
#[11] Lab indicators ----
#******************************************
#library(dplyr)

dat_lab <- list(lab_id1, lab_id2, lab_id3, lab_id4, lab_id5, lab_id6, lab_id7, lab_id8, lab_id9, lab_id10)
names(dat_lab) <- paste0("RRR", 1:10)
dat_lab <- dplyr::bind_rows(dat_lab, .id = "Project")

# write.csv(dat_lab, "../data/03_cleaned-raw-data/lab-keys.csv", row.names = FALSE)
