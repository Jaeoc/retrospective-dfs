#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Create plots
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)

#********************************************************************************

##Script content:----

#[0] Packages and sourced functions
#[1] Meta-analytic plot (figure 5)
#[2] Create funnel plots
#**[2.1] RRR04 16/24 labs (Figure 2)
#**[2.2] Supplemental funnel plots
#[3] DF standard deviation plots
#**[3.1] Plot DF SD across all RRRs (figure 4)
#**[3.2] DF SD bar plots for all RRRs (supplemental)
#**[3.3] RRR04 16/24 labs (Figure 3)
#[4] Publication bias correction plots (supplemental)
#**[4.1] Most significant selection
#**[4.2] Below alpha selection
#**[4.3] Pre-registered results
#**[4.4] Random sample results
#**[4.5]Bounded random selection
#**[4.6] Random significant selection
#**[4.7] Combine into one dataset
#**[4.8]Plot publication bias corrected

#********************************************************************************
#[0] Packages and sourced functions----
#********************************************************************************
#install.packages("weightr")

if(!require(ggplot2)){install.packages("ggplot2")}
if(!require(weightr)){install.packages("weightr")}
if(!require(puniform)){install.packages("puniform")}
if(!require(hexbin)){install.packages("hexbin")} #required for geom_hex in funnel plots

library(ggplot2) #for plotting
library(weightr) #for fitting 3PSM publication bias correction for supplement
library(puniform) #for fitting puniform* publication bias correction for supplement

source("4.0_functions-meta-analytic-multiverse.r") #source data summary functions
source("4.1_PETPEESE-function.r") #for fitting PET-PEESE publication bias correction for supplement
source("5.0_functions-plots-and-tables.r") #source plot functions


#********************************************************************************
#[1] Meta-analytic plot (figure 5) ----
#********************************************************************************

meta_analytic_plotter(readRDS("../data/meta_plot_data_n24.RDS"),
                      name = "")
# ggsave("../figures/manuscript/meta-summary_n24.png", dpi = 600, width = 7, height = 7*1.2)
#ggsave("../figures/manuscript/meta-summary_n24.tiff", dpi = 600, width = 7, height = 7*1.2, device = "tiff")

#Supplemental Figure 5 (S3)
meta_analytic_plotter(readRDS("../data/meta_plot_data_n2.RDS"),
                      name = "")
# ggsave("../figures/supplement/meta-summary_n2.png", dpi = 600, width = 7, height = 7*1.2)

#********************************************************************************
#[2] Create funnel plots ----
#********************************************************************************

#Get file paths
files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")

#**[2.1] RRR04 16/24 labs (Figure 2)----
#RRR4 showing 16/24 labs for a nicer (square)plot

rrr4_cut_data <- files$RRR04[1:16]

#First compute SD in effect size for each lab
sd_rrr4 <- lapply(rrr4_cut_data, read_memory_saving, n_equal_or_more = 24) #load data

sd_rrr4 <- lapply(sd_rrr4, function(x){ #compute SD in effect size for each lab
  data.frame(lab = x[["lab"]][1],
             lab_sd = sd(x[["effect_size"]]))
})

sd_rrr4 <- do.call(rbind, sd_rrr4)
sd_rrr4$label <- format(round(sd_rrr4$lab_sd, 2), nsmall = 2) #to ensure 2 significant decimals

#Next create plot, add the above dataframe to the annotation argument, see 5.0_functions-plots-tables
rrr4_cut <- funnel_plotter(rrr4_cut_data, n_equal_or_more = 24, plot_title = "", annotation = sd_rrr4)
# ggsave("../figures/manuscript/RRR04_funnel_n24_cut.png", plot = rrr4_cut, dpi = 600, width = 6, height = 6*1.2)
#ggsave("../figures/manuscript/RRR04_funnel_n24_cut.tiff", plot = rrr4_cut, dpi = 600, width = 6, height = 6*1.2, device = "tiff")

rrr4_cut_complete <- funnel_plotter(rrr4_cut_data, n_equal_or_more = 2, plot_title = "", annotation = sd_rrr4) #minimum possible ss = 2/group to compute means etc
# ggsave("../figures/supplement/RRR04_funnel_n2_cut.png", plot = rrr4_cut_complete, dpi = 600, width = 6, height = 6*1.2)

#**[2.2] Supplemental funnel plots----
#Create lab funnel plots for all RRRs

#First, get effect size SD to annotate plots with
rrr_sd <- lapply(files, function(file_paths){

  a <- lapply(file_paths, function(x){ #get ES SD for each lab in an RRR (list)
    lab_data <- read_memory_saving(x, n_equal_or_more = 24)
    if(nrow(lab_data) == 0){ #if lab contains no multiverse with ns larger than the set value

      warning_message <- get_warning_message(x, "")
      warning(warning_message)
      return() #print warning and return NULL value for that lab
      #with sample size 24, expect warnings for RRR8 L24; RRR2 L09, L17, L26
    }

    data.frame(lab = lab_data$lab[1], lab_sd = sd(lab_data$effect_size)) #SD per lab
  })

  b <- do.call(rbind, a) #combine list of lab SDs into dataframe
  b$label <- format(round(b$lab_sd, 2), nsmall = 2) #add label variable with 2 significant decimals

  b
}) #output is a list of dataframes with annotation data


#loop: load data, plot, and save for all RRRs, n >= 24
for(rrr in seq_along(files)){
  out <- funnel_plotter(files[[rrr]], n_equal_or_more = 24, plot_title = names(files)[rrr], annotation = rrr_sd[[rrr]])
  save_name <- paste0("../figures/supplement/", names(files)[rrr], "_funnel_n24.png")
  ggsave(save_name, plot = out, dpi = 600, width = 6, height = 6*1.2)
}

#To create "unfiltered" funnel plots for all RRRs, set n_equal_or_more = 2
#also change the save_name to not oversave the other plots
#********************************************************************************
#[3] DF standard deviation plots ----
#********************************************************************************


#In this section we
#1) create Figure 3: SD in labs disaggregated across RRRs
#2) create DF SD histograms for all RRRs (supplemental)
#3) Create Figure 2: SD in 16/24 labs in RRR04

#Load file paths
files <- files_by_meta_analysis(directory = "../data/05_study_multiverses_cleaned")

#extract ES SD for each DF
# es_sd <- lapply(files, list_extract_df_sd, n_equal_more_than = 24)
# saveRDS(es_sd, "../data/es_sd.RDS") #takes a bit of time, so create intermediate dataset

es_sd <- readRDS("../data/es_sd.RDS")


#**[3.1] Plot DF SD across all RRRs (figure 4)----


#Prep plot for median across RRRs  (warnings can be ignoreD)
es_sd_all <- dplyr::bind_rows(es_sd, .id = "meta_analysis")

#Compbine all unique into one category and all e4 into one category
es_sd_all$DF <- gsub("u.*", "u", es_sd_all$DF)
es_sd_all$DF <- gsub("e4_1|e4_2", "e4", es_sd_all$DF) #we will average across these

#Separate between log odds ratios and SMD effect types
es_sd_smd <- es_sd_all[!es_sd_all$meta_analysis %in% c("RRR01", "RRR02"),]
es_sd_logor <- es_sd_all[es_sd_all$meta_analysis %in% c("RRR01", "RRR02"),]

es_split <- list("Log Odds Ratios (RRR01 - RRR02)" = es_sd_logor,
                 "Standardized Mean Differences (RRR03 - RRR10)" = es_sd_smd)

#Create function to compute median ES across DF in labs
#disaggregated across RRRs
median_per_DF <- function(es_type){
  a <- split(es_type, es_type$DF)
  b <- lapply(a, function(x){
    data.frame(DF = x[1, "DF"],
               median_sd_complete = median(x$sd_complete),
               median_sd_filtered = median(x$sd_filtered, na.rm = TRUE), #na.rm necessary because filtering in some labs (RRR8) -> NA because no DF choice variation left
               max_DF_sd_complete = max(x$sd_complete), #used for ordering for individual RRR plots
               max_DF_sd_filtered = max(x$sd_filtered, na.rm = TRUE)) #used for ordering
  })
  out <- do.call(rbind, b)

  #because DF is a factor, splitting by it creates Na results where an RRR lacks a DF, get rid of these
  #This applies only to the use of this function in section 2)
  out[!is.na(out$median_sd_complete),]
}


es_split <- lapply(es_split, function(x){
  es_medians <- median_per_DF(x) #compute medians for each DF
  merge(x, es_medians) #merge with full SD dataset
})

##prep plot
#First, create a variable across lists that goes from 1:length(categories DF)
#to make boxplot in descending order across facets
es_split <- lapply(es_split, function(x){ #order by median sd and max
 x <- x[order(x$median_sd_filtered, x$max_DF_sd_filtered),]
 x$order <- as.numeric(factor(x$DF, levels = unique(x$DF))) #create order variable per effect type
 x
})

#add the values of the logor to the smd order variable to make them all appear in descending order
es_split$`Standardized Mean Differences (RRR03 - RRR10)`$order <-
  es_split$`Standardized Mean Differences (RRR03 - RRR10)`$order + length(unique(es_split$`Log Odds Ratios (RRR01 - RRR02)`$order))

#Recombine to be able to do a facet plot   (warnings can be ignoreD)
es_sd_all <- dplyr::bind_rows(es_split, .id = "effect_type")

#Factor order variable
es_sd_all$order <- factor(es_sd_all$order)

#Rename to be able to change order in plot, not have extra DF showing up on axis, and nice output
es_sd_all$DF <- factor(es_sd_all$DF,
                       levels = c("e1", "e2", "e3", "e4", "e5", "e6", "s1", "s2", "u"),
                       labels = c("Missingness DV (E1)",
                                  "Miss. age/demog. (E2)",
                                  "Age (E3)",
                                  "Demographics (E4)",
                                  "Attention check (E5)",
                                  "Univariate outliers (E6)",
                                  "Scale length (S1)",
                                  "Composite score (S2)",
                                  "Unique (U)"))
#plot for >=24 participants
#Warnings can be ignored: due to some DF not resulting in any results for some labs
ggplot(data = es_sd_all, aes(x = order, y = sd_filtered)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(width = 0.2, alpha = 0.1) +
  geom_hline(yintercept = 0, lty = 2, alpha = 0.6) +
  scale_x_discrete(name = "DF",
                   labels =  setNames(as.character(es_sd_all$DF),
                                      as.numeric(es_sd_all$order))) +
  scale_y_continuous(name = "Standard Deviation") +
  coord_flip() +
  theme_classic() +
  facet_wrap(~effect_type, scales = "free", ncol = 1, drop = TRUE)

# ggsave("../figures/manuscript/median_multi_sd_n24.png", dpi = 600, width = 6, height = 6*1.2)
#ggsave("../figures/manuscript/median_multi_sd_n24.tiff", dpi = 600, width = 6, height = 6*1.2, device = "tiff")

#Plot for all participants
ggplot(data = es_sd_all, aes(x = order, y = sd_complete)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(width = 0.2, alpha = 0.1) +
  geom_hline(yintercept = 0, lty = 2, alpha = 0.6) +
  scale_x_discrete(name = "DF",
                   labels =  setNames(as.character(es_sd_all$DF),
                                      as.numeric(es_sd_all$order))) +
  scale_y_continuous(name = "Effect size SD") +
  coord_flip() +
  theme_classic() +
  facet_wrap(~effect_type, scales = "free", ncol = 1, drop = TRUE)

# ggsave("../figures/supplement/median_multi_sd_n2.png", dpi = 600, width = 6, height = 6*1.2)



#**[3.2] DF SD bar plots for all RRRs (supplemental)----

es_sd <- readRDS("../data/es_sd.RDS")

es_median_sd <- lapply(es_sd, function(x){
  es_medians <- median_per_DF(x) #compute medians for each DF
  merge(x, es_medians) #merge with full SD dataset
})

#Add unique titles for plotting (the list names)
for(rrr in seq_along(es_median_sd)){
 es_median_sd[[rrr]]$meta_analysis <- names(es_median_sd)[rrr]
}

#add labels to the factors for nicer plots
common_df_identified <- c("Missingness DV (E1)",
                          "Miss. age/demog. (E2)",
                          "Age (E3)",
                          "Demographics (E4)",
                          "Attention check (E5)",
                          "Univariate outliers (E6)",
                          "Scale length (S1)",
                          "Composite score (S2)")


e4_and_unique <- list(rrr01 = c("Ethnicity (E4.1)",
                                "Language (E4.2)",
                                "Comprehension check (U1)",
                                "Familiarity with effect (U2)",
                                "Confidence Rating (U3)",
                                "Studied psychology (U4)",
                                "Control task tries (U5)"),
                      rrr02 = c("Ethnicity (E4.1)",
                                "Language (E4.2)",
                                "Comprehension check (U1)",
                                "Familiarity with effect (U2)",
                                "Confidence Rating (U3)",
                                "Studied psychology (U4)",
                                "Control task tries (U5)"),
                      `rrr03 attribution` = c("Native Language (E4)",
                                              "Bilingualism (U1)",
                                              "Education level (U2)"),
                      `rrr03 intention` = c("Native Language (E4)",
                                            "Bilingualism (U1)",
                                            "Education level (U2)"),
                      `rrr03 process` = c("Native Language (E4)",
                                          "Bilingualism (U1)",
                                          "Education level (U2)"),
                      rrr04 = c("Language (E4.1)",
                                "Ethnicity (E4.2)",
                                "% correct MSIT (U1)",
                                "% correct letter E (U2)",
                                "Outliers RT (U3)"),
                      `rrr05 exit` = c("In rom. rel. (U1)",
                                       "Dating duration (U2)",
                                       "Miss. across DVs (U3)",
                                       "Complete all tasks (U4)",
                                       "Status of rel. (U5)",
                                       "Rel. exclusivity (U6)",
                                       "Rel. interaction (U7)",
                                       "Distance partner (U8)",
                                       "Year in school (U9)"),
                      `rrr05 neglect`= c("In rom. rel. (U1)",
                                         "Dating duration (U2)",
                                         "Missing across DVs (U3)",
                                         "Complete all tasks (U4)",
                                         "Status of rel. (U5)",
                                         "Rel. exclusivity (U6)",
                                         "Rel. interaction (U7)",
                                         "Distance partner (U8)",
                                         "Year in school (U9)"),
                      rrr06 = c("Student (E4)",
                                "Guessed goal (U1)",
                                "Understood cartoons (U2)",
                                "Held pen incorrectly \n video (U3)",
                                "Held pen incorrectly \n self-report (U4)",
                                "Field of study (U5)"),
                      rrr07 = c("Comprehension check (U1)", #u1 then the unique
                                "Particip. deceitful exp. (U10)", #u10 comes after u1 because of ssorting
                                "Time limit compliance (U2)", #u2
                                "Particip. identical exp. (U3)", #u3
                                "Complete all tasks (U4)", #u4
                                "Heard about study (U5)",  #u5
                                "Know other participants (U6)", #u6.
                                "Particip. any exp. (U7)", #u7
                                "Particip. paid exp. (U8)", #u8
                                "Particip. online exp. (U9)"),
                      rrr08 = c("Student (E4.1)",
                                "Language (E4.2)",
                                "Connection tasks (U1)",
                                "Prior exp. hooligan (U2)",
                                "Year in school (U3)",
                                "Major (U4)"),
                      `rrr09 behavior` = c("Language (E4.1)",
                                           "Student (E4.2)",
                                           "Gender (U1)",
                                           "Sentence completion (U2)",
                                           "Session size (U4)",
                                           "Major (U5)"),
                      `rrr09 hostility` = c("Language (E4.1)",
                                            "Student (E4.2)",
                                            "Gender (U1)",
                                            "Sentence completion (U2)",
                                            "Session size (U3)",
                                            "Major (U4)"),
                      rrr10 = c("Language (E4.1)",
                                "Student (E4.2)",
                                "Gender (U1)",
                                "Intervention completion (U2)",
                                "Session size (U3)",
                                "Religiousness (U4)",
                                "Major (U5)")
)


#apply the labels
for(rrr in seq_along(es_median_sd)){
  es_median_sd[[rrr]]$DF <- factor(es_median_sd[[rrr]]$DF,
                                level = c("e1", "e2", "e3", "e5", "e6", "s1", "s2", #first common minus e4
                                          unique(grep("e4|u", es_median_sd[[rrr]]$DF, value = TRUE))), #then e4, then all unique
                                labels = c(common_df_identified[-4], #first all the common labels
                                           e4_and_unique[[rrr]])) #then e4 and unique labels

}


#Convert es_median_sd from wide to long format to prepare for facet plot
es_median_sd <- lapply(es_median_sd, function(x){
  data.frame(x[, c("meta_analysis", "DF", "lab")],
             df_sd = c(x$sd_complete, x$sd_filtered),
             df_sd_median = c(x$median_sd_complete, x$median_sd_filtered),
             df_sd_max = c(x$max_DF_sd_complete, x$max_DF_sd_filtered),
             id = c(rep("Complete dataset", length(x$median_sd_complete)),
                    rep("Excluding multiverses with group size < 24", length(x$median_sd_filtered))),
             row.names = NULL)
})

#plottting function for loop below
plot_sd <- function(dat, plot_title){

  #Create a variable to order bars by the median SD across labs
  dat_split <- split(dat, dat$id) #split by complete/filtered
  dat_split <- lapply(dat_split, function(x){
    x <- x[order(x$df_sd_median, x$df_sd_max),] #first order by median sd, then by max
    x$order <- as.numeric(factor(x$DF, levels = unique(x$DF))) #create order variable per complete/filtered should match #Df
    x
  })
  dat <- do.call(rbind, dat_split)
  dat$order <- factor(dat$order) #as factor to facilitate plotting

  ggplot(dat) +
    geom_bar(aes(y = order, x = df_sd), stat = "identity") + #Ggplot 3.0 allows interchangeable x and y
    theme_bw() + #keep gridlines but use white background
    theme(strip.background =element_rect(fill="transparent")) + #change background color strip labels
    scale_x_continuous(name = "Standard Deviation", breaks = scales::pretty_breaks(n = 3)) + #set the number of breaks
    scale_y_discrete(name = "DF",
                     labels =  setNames(as.character(dat$DF),
                                        as.numeric(dat$order))) +
    ggtitle(plot_title) +
    facet_wrap(~lab)
}

#Primary output for supplement
sd_plotting_data <- lapply(es_median_sd, function(x) x[x$id == "Excluding multiverses with group size < 24",])
#If you want to print the plots without filtering, exchange the id above for "Complete dataset"

#these labs do not have n >= 24, so are NA (can't just remove NA, because some other RRR labs miss certain variables but not others)
sd_plotting_data$RRR02 <- sd_plotting_data$RRR02[!sd_plotting_data$RRR02$lab %in% c("L09", "L17", "L26"),]
sd_plotting_data$RRR08 <- sd_plotting_data$RRR08[!sd_plotting_data$RRR08$lab == "L24",]

#loop to create all plots
for(rrr in seq_along(sd_plotting_data)){
  out <- plot_sd(sd_plotting_data[[rrr]], plot_title = names(sd_plotting_data)[rrr])
  save_name <- paste0("../figures/supplement/", names(sd_plotting_data)[rrr], "_sd")
  save_name <- paste0(save_name, "_n24.png")
  ggsave(filename = save_name, plot = out, dpi = 600, width = 6, height = 6*1.2)
}
#Warnings can be ignored: due to some DF not being applicable for some labs within an RRR


#**[3.3] RRR04 16/24 labs (Figure 3)----

#NB! First run the above section to get the polished es_median_sd object needed below

#RRR04 cut variant
#Get only first 16 labs for a nicer plot
labs1_16 <- sort(unique(es_median_sd$RRR04$lab))[1:16] #get lab indicators for first 16 labs
es_sd_rrr4_cut <- es_median_sd$RRR04[es_median_sd$RRR04$lab %in% labs1_16,] #get matching observations



#Plot (Filtered and unfiltered)
es_sd_rrr4_plot <- plot_sd(es_sd_rrr4_cut[es_sd_rrr4_cut$id == "Excluding multiverses with group size < 24",], plot_title = "")
# ggsave("../figures/manuscript/RRR04_sd_n24_cut.png", plot = es_sd_rrr4_plot, dpi = 600, width = 6, height = 6*1.2)
#ggsave("../figures/manuscript/RRR04_sd_n24_cut.tiff", plot = es_sd_rrr4_plot, dpi = 600, width = 6, height = 6*1.2, device = "tiff")

es_sd_rrr4_plot <- plot_sd(es_sd_rrr4_cut[es_sd_rrr4_cut$id == "Complete dataset",], plot_title = "")
# ggsave("../figures/supplement/RRR04_sd_n2_cut.png", plot = es_sd_rrr4_plot, dpi = 600, width = 6, height = 6*1.2)


#********************************************************************************
#[4] Publication bias correction plots (supplemental)----
#********************************************************************************

#Load data
single_results <- readRDS("../data/single_MA_study_level.RDS") #The single-MA study-level results
plot_data <- readRDS("../data/meta_plot_data_n24.RDS") #The random sample and original estimates

#The above data is loaded from the 5.1_prep-meta-plot.r script


#**[4.1] Most significant selection----
most_sig_split <- split(single_results$most_sig,
                        single_results$most_sig$meta_analysis) #split by

most_sig_split <- lapply(most_sig_split,
                         function(x) x[x$lab != "Summary",]) #drop rma results since will be re-estimated

most_sig_res <- lapply(most_sig_split, pub_bias_summarizer) #"file_path" just needs to be a var that contains RRR number (e.g., RRR03) [to get direction of hypothesis]

#bind into dataframe
most_sig <- dplyr::bind_rows(most_sig_res, .id = "meta_analysis")

#**[4.2] Below alpha selection----
below_alpha_split <- split(single_results$below_alpha,
                           single_results$below_alpha$meta_analysis) #split by

below_alpha_split <- lapply(below_alpha_split,
                            function(x) x[x$lab != "Summary",]) #drop rma results since will be re-estimated

below_alpha_res <- lapply(below_alpha_split, pub_bias_summarizer) #"file_path" just needs to be a var that contains RRR number (e.g., RRR03) [to get direction of hypothesis]

#bind into dataframe
below_alpha <- dplyr::bind_rows(below_alpha_res, .id  = "meta_analysis")

#**[4.3] Pre-registered results----
prereg <- plot_data$estimates[plot_data$estimates$Selection == "Pre-registered",]
prereg$Selection <- NULL
prereg$Correction <- "None"

#**[4.4] Random sample results----

#NB! The below line is different between making plot and table

random <- plot_data$estimates[plot_data$estimates$Selection == "Random draw",]
random$Selection <- NULL #drop since having it makes it more complicated when combining all results in this script for plotting later
random$Correction <- "None" #add for easier plotting later

#**[4.5]Bounded random selection----
files_all <- list.files("../data/06_meta-analytic-multiverse-samples", full.names = TRUE)
files_bounded_draws <- grep("bound", files_all, value = TRUE) #If we have any significance sample files  pick these here

bounded_draws <- load_summarize_pub_bias_methods(files_bounded_draws)

#**[4.6] Random significant selection----
files_sig_draws <- grep("sig", files_all, value = TRUE) #If we have any significance sample files  pick these here
files_sig_draws <- setdiff(files_sig_draws, files_bounded_draws) #drop the sig_bound draws

sig_draws <- load_summarize_pub_bias_methods(files_sig_draws)



#**[4.7] Combine into one dataset----


res <- list("Random draw" = random,
            "Random significant" = sig_draws,
            "Bounded significant" = bounded_draws,
            "Pre-registered" = prereg,
            "Most significant" = most_sig,
            "Below alpha" = below_alpha)


#combine estimates into one dataframe
res <- dplyr::bind_rows(res, .id = "Selection")

#change the estimates in the list with plotting data to the publication bias estimates
plot_data$estimates <- res

#**[4.8]Plot publication bias corrected----


#We will create three separate plots by correction method, so separate these
plot_data_3psm <- list("estimates" =
                         plot_data$estimates[plot_data$estimates$Correction %in%
                                               c("None", "3PSM"),],
                       "distribution" = plot_data$distribution,
                       "multiverse_sizes" = plot_data$multiverse_sizes,
                       "median_N" = plot_data$median_N,
                       "multiverse_sd" = plot_data$multiverse_sd)

plot_data_petpeese <- list("estimates" =
                             plot_data$estimates[plot_data$estimates$Correction %in%
                                                   c("None", "PETPEESE"),],
                           "distribution" = plot_data$distribution,
                           "multiverse_sizes" = plot_data$multiverse_sizes,
                           "median_N" = plot_data$median_N,
                           "multiverse_sd" = plot_data$multiverse_sd)

plot_data_puniform <- list("estimates" =
                             plot_data$estimates[plot_data$estimates$Correction %in%
                                                   c("None", "Puniform*"),],
                           "distribution" = plot_data$distribution,
                           "multiverse_sizes" = plot_data$multiverse_sizes,
                           "median_N" = plot_data$median_N,
                           "multiverse_sd" = plot_data$multiverse_sd)




meta_analytic_plotter(plot_data_3psm, name = "Corrected with 3PSM")
# ggsave("../figures/supplement/3psm.png", dpi = 600, width = 7, height = 7*1.2)

meta_analytic_plotter(plot_data_petpeese, name = "Corrected with PETPEESE")
# ggsave("../figures/supplement/PETPEESE.png", dpi = 600, width = 7, height = 7*1.2)

meta_analytic_plotter(plot_data_puniform, name = "Corrected with Puniform*")
# ggsave("../figures/supplement/puniform.png", dpi = 600, width = 7, height = 7*1.2)
