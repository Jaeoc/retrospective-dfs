#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: Function for the PET-PEESE estimator, to be sourced into the
#                 4.2_create-meta-analytic-multiverses.r script                  
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)

#********************************************************************************
##Script content:----

#[0] Code origin and summary of changes
#[1] Helper function
#[2] Pet-PEESE, copied and modified code

#********************************************************************************
#[0] Code origin and summary of changes----
#********************************************************************************
#Pet-PEESE function, copied from https://osf.io/w8qcn/ 
#Carter, E. C., Schönbrodt, F. D., Gervais, W. M., & Hilgard, J. (2019). Correcting for
#Bias in Psychology: A Comparison of Meta-Analytic Methods. Advances in Methods
#and Practices in Psychological Science, 2. doi:10.1177/2515245919847196

#I have copied the full code of this function from Carter et al. with the 
#following minor changes to load fewer packages (plyr, dplyr and reshape2)

#1) do filtering with base R instead of dplyr
# when creating the objects lm.p.value, lm.est, rma.p.value and rma.est

#2) changed rbind.fill to rbind just before output of 'res'
#(and added a few lines of code to make this work)

#3) drop the returnRes function and remove the long = TRUE input argument
#(since I want wide output: this avoids the use of the reshape2 package)

#4) Also added a conditional on which direction hypothesis was expected in
#with additional argument file_path
#see line: if(grepl(pattern = "RRR3|RRR4|RRR6|RRR7|RRR8|RRR9", file_path)){ 

#********************************************************************************
#[1] Helper function----
#********************************************************************************

#NB! the PETPEESE.est function requires the below helper from https://osf.io/phjxq/

tidyLM <- function(...) {
  res <- broom::tidy(..., conf.int=TRUE)
  res$term[res$term == "(Intercept)"] <- "b0"
  res$term[res$term == "sqrt(v)"] <- "b1"
  res$term[res$term == "v"] <- "b1"
  return(res)
}

#NB! the Tidy function above comes from the package broom! I added the broom::
#in the code above to not have to load the full package

#********************************************************************************
#[2] Pet-PEESE, copied and modified code----
#********************************************************************************


# estimator_type: 1 = WAAP; 2 = WLS; 3 = PET; 4 = PEESE

PETPEESE.est <- function(d, v, PP.test = "one-sided", runRMA=FALSE, file_path) {
  
  PP.test <- match.arg(PP.test, PP.test)
  
  PET.lm <- lm(d~sqrt(v), weights=1/v)
  PEESE.lm <- lm(d~v, weights=1/v)
  
  res <- rbind(
    data.frame(method="PET.lm", tidyLM(PET.lm)),
    data.frame(method="PEESE.lm", tidyLM(PEESE.lm))
  )  
  
  # conditional PET/PEESE estimator	
  lm.p.value  <- res[res$method == "PET.lm" & res$term == "b0", "p.value"]
  lm.est  <- res[res$method == "PET.lm" & res$term == "b0", "estimate"]
  
  
  # "For the purpose of deciding which meta-regression accommodation for selective reporting bias to employ, we recommend testing H0:b0 < 0 at the 10% significance level."
  # From: Stanley, T. D. (2016). Limitations of PET-PEESE and other meta-analysis methods. Abgerufen von https://www.hendrix.edu/uploadedFiles/Departments_and_Programs/Business_and_Economics/AMAES/Limitations%20of%20PET-PEESE.pdf
  
  
  if (PP.test == "one-sided") {
    p.crit <- .10
  } else if (PP.test == "two-sided") {
    p.crit <- .05
  }
  
  #the grepl:ed RRRs predicted a positive effect size, given treatment - control, rest negative (see direction of {<>}
  if(grepl(pattern = "RRR3|RRR4|RRR6|RRR7|RRR8|RRR9", file_path)){ 
    
    usePEESE.lm <- ifelse(lm.p.value < p.crit & lm.est > 0, TRUE, FALSE) #positive >
  }else{
    usePEESE.lm <- ifelse(lm.p.value < p.crit & lm.est < 0, TRUE, FALSE) #negative <
  }
  
  res <- rbind(res,
               data.frame(method="PETPEESE.lm", if (usePEESE.lm == TRUE) {tidyLM(PEESE.lm)} else {tidyLM(PET.lm)})
  )
  
  
  if (runRMA == TRUE) {
    PET.rma <- tryCatch(
      rma(yi = d, vi = v, mods=sqrt(v), method="REML", control = list(stepadj = .5, maxiter=500)),
      error = function(e) rma(yi = d, vi = v, mods=sqrt(v), method="DL")
    )
    
    PEESE.rma <- tryCatch(
      rma(yi = d, vi = v, mods=v, method="REML", control = list(stepadj = .5, maxiter=500)),
      error = function(e) rma(yi = d, vi = v, mods=v, method="DL")
    )  
    
    res <- rbind(
      res,
      data.frame(method="PET.rma", tidyRMA(PET.rma)),
      data.frame(method="PEESE.rma", tidyRMA(PEESE.rma))
    )
    
    rma.p.value <- res[res$method == "PET.rma" & res$term == "b0", "p.value"]
    rma.est <- res[res$method == "PET.rma" & res$term == "b0", "estimate"]
    usePEESE.rma <- ifelse(rma.p.value < p.crit & rma.est > 0, TRUE, FALSE)
    res <- rbind(res,
                 data.frame(method="PETPEESE.rma", if (usePEESE.rma == TRUE) {tidyRMA(PEESE.rma)} else {tidyRMA(PET.rma)})
    )
  }
  
  
  estimator <- data.frame(term="estimator", 
                          estimate = NA, #all NAs added to be able to combine with res
                          std.error = NA,
                          statistic = NA,
                          p.value = NA,
                          conf.low = NA,
                          conf.high = NA)
  res$type <- NA #added to match the added dataframe below
  
  res <- rbind(res, data.frame(method="PETPEESE.lm",estimator, type=ifelse(usePEESE.lm == TRUE, 4, 3)))
  
  if (runRMA == TRUE) {
    res <- rbind(res, data.frame(method="PETPEESE.rma", estimator, type=ifelse(usePEESE.rma == TRUE, 4, 3)))	
  } else {
    res$method <- gsub(".lm", "", res$method, fixed=TRUE)
  }
  
  res
}

