#********************************************************************************

##Project: Meta-analyzing the multiverse
##Script purpose: This script creates the multiverses to be analyzed
##Code: Anton Olsson Collentine (anton@olssoncollentine.com)


#********************************************************************************

##Script content:----

#[0] Packages
#[1] RRR1 multiverse
#[2] RRR2 multiverse
#[3] RRR3 multiverse
#**[3.1] Intentionality
#**[3.2] Intention attribution
#**[3.3] Detailed processing
#[4] RRR4 multiverse
#[5] RRR5 multiverse
#**[5.1] Exit effect
#**[5.2] Neglect effect
#[6] RRR6 multiverse
#[7] RRR7 multiverse
#[8] RRR8 multiverse
#[9] RRR9 multiverse
#**[9.1] Hostility
#**[9.2] Behavior
#[10] RRR10 multiverse

#********************************************************************************
##[0] Packages----
#********************************************************************************
if(!require("psych")){install.packages("psych")}

library(psych) #needed for PCA  (s2 of common_df)
library(parallel) #for parallelizing all functions, part of R

source("2.0_functions-study-level-multiverses.r") #load the functions

#NB! To run the parallel::parLapply() loops in the code below the save folders
#must already exist or there will be an error. The save folders should have the
#structure (all these are folders):
#data/04_study_multiverses/RRR1
#data/04_study_multiverses/RRR2
#...
#data/04_study_multiverses/RRR10


#********************************************************************************
##[1] RRR1 multiverse----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr1.csv", stringsAsFactors = FALSE)

#L19 is mturk lab. Code this variable as " yes' for all other labs, otherwise they just drop all participants when e2 = 1, which doesn't make sense because these labs didn't actually measure this variable
rrr$mturk_english_first_language <- ifelse(rrr$lab == "L19", rrr$mturk_english_first_language, "yes")

rrr_split <- split(rrr, rrr$lab) #to facilitate looping

#unique DF (number of choices)
u1 <- u2 <- u3 <- u4 <- u5 <- 0:1
unique_rrr1 <- list(u1, u2, u3, u4, u5) #unique must be input as named list in create_multiverse function
names(unique_rrr1) <- paste0("u", 1:length(unique_rrr1))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr1, e2 = 0:1, e3 = 0:4, e4 = 2, e5 = 0:2) #rest of common == 0, nb e4 takes number of vars as input
N_multiverse <- nrow(multiverse_rrr)

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR1",  apply_df_rrr = apply_df_rrr1_2, es = "OR") #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#********************************************************************************
##[2] RRR2 multiverse----
#********************************************************************************

#only change here compared to RRR1 is using different dataset, all other vectors
#(e.g., multiverse_rrr) reused from section above. [Different save-name also]

rrr <- read.csv("../data/03_cleaned-raw-data/rrr2.csv", stringsAsFactors = FALSE)

#Here L15 is mturk lab. Code this variable as " yes' for all other labs, otherwise they just drop all participants when e2 = 1, which doesn't make sense because these labs didn't actually measure this variable
rrr$mturk_english_first_language <- ifelse(rrr$lab == "L15", rrr$mturk_english_first_language, "yes")


rrr_split <- split(rrr, rrr$lab) #to facilitate looping

#unique DF (number of choices) [NB! same unique DF and multiverse as RRR1)
u1 <- u2 <- u3 <- u4 <- u5 <- 0:1
unique_rrr1 <- list(u1, u2, u3, u4, u5) #unique must be input as named list in create_multiverse function
names(unique_rrr1) <- paste0("u", 1:length(unique_rrr1))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr1, e2 = 0:1, e3 = 0:4, e4 = 2, e5 = 0:2) #rest of common == 0, nb e4 takes number of vars as input
N_multiverse <- nrow(multiverse_rrr)


#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR2",  apply_df_rrr = apply_df_rrr1_2, es = "OR") #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start


#********************************************************************************
##[3] RRR3 multiverse----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr3.csv", stringsAsFactors = FALSE)

intent <- grep("intent_item", names(rrr), value = TRUE)
process <- grep("process_item", names(rrr), value = TRUE)
attrib <- grep("attrib_item", names(rrr), value = TRUE)

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- 0:2
u2 <- 0:1
unique_rrr3 <- list(u1, u2) #unique must be input as named list in create_multiverse function
names(unique_rrr3) <- paste0("u", 1:length(unique_rrr3))

#note that processing is the only of the 3 DVs that has e1 = 0:1 instead of e1 = 0, see DF per project list
multiverse_rrr <- create_multiverse(unique_rrr3, s1 = 0:2, s2 = 0:2, e2 = 0:1, e3 = 0:4, e4 = 1, e6 = 0:3) #rest of common == 0. Needed as input for dataverser function
multiverse_rrr <- multiverse_rrr[!(multiverse_rrr[,"s1"] == 2 & multiverse_rrr[, "s2"] == 2),] #these must be dropped because when < 5 items s1 == 2 leads to < 3 items for PCA
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for the dataverser function

#create the dataverses

#**[3.1] Intentionality----

#parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment
#create the dataverse

#loop over each lab

parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR3", appendix = "intent", apply_df_rrr = apply_df_rrr3,
                    dv_input = intent) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start


#**[3.2] Intention attribution----

start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment
#create the dataverse

#loop over each lab

parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR3", appendix = "attrib", apply_df_rrr = apply_df_rrr3,
                    dv_input = attrib) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start



#**[3.3] detailed processing----
#note that processing is the only of the 3 DVs that has e1 = 0:1 instead of e1 = 0, and thus has a different multiverse

multiverse_rrr <- create_multiverse(unique_rrr3, s1 = 0:2, s2 = 0:2, e1 = 0:1, e2 = 0:1, e3 = 0:4, e4 = 1, e6 = 0:3) #e1 = 0 for this DV, not 0:1
multiverse_rrr <- multiverse_rrr[!(multiverse_rrr[,"s1"] == 2 & multiverse_rrr[, "s2"] == 2),] #these must be dropped because when < 5 items s1 == 2 leads to < 3 items for PCA
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for loop below

start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment
#create the dataverse

#loop over each lab

parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR3", appendix = "process", apply_df_rrr = apply_df_rrr3,
                    dv_input = process) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#********************************************************************************
##[4] RRR4 multiverse----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr4.csv", stringsAsFactors = FALSE)

rrr$race <- ifelse(rrr$lab == "L10", rrr$race, "not measured") #only one lab measured the variable "race", change NA in other labs to something, otherwise all ss in those labs excluded for no reason
rrr$age <- ifelse(rrr$lab %in% c("L5", "L17"), 18, rrr$age) #Two labs (Carruth, Ringos) did not have age variables, recode to avoid excluding all subjects for these labs [ie.e., for these labs no exclusions based on age]

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- u2 <-  0:5
u3 <- 0:3
unique_rrr4 <- list(u1, u2, u3) #unique must be input as named list in create_multiverse function
names(unique_rrr4) <- paste0("u", 1:length(unique_rrr4))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr4, e2 = 0:1, e3 = 0:4, e4 = 2, e6 = 0:3) #rest of common == 0
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for loop below

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR4", apply_df_rrr = apply_df_rrr4) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start


#********************************************************************************
##[5] RRR5 multiverse----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr5.csv", stringsAsFactors = FALSE)

exit_items <- grep("exit_item", names(rrr), value = TRUE) #value = TRUE gives us the value instead of the index
neglect_items <- grep("neglect_item", names(rrr), value = TRUE)

#EDIT: NB! Exit and neglect are the "primary outcome variables" in RRR5 -> update in DF list as well

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- u3 <-  u4 <- u6 <-  u8 <- 0:1
u5 <- u7 <- u9 <- 0:2
u2 <- 0:3
unique_rrr5 <- list(u1, u2, u3, u4, u5, u6, u7, u8 ,u9) #unique must be input as named list in create_multiverse function
names(unique_rrr5) <- paste0("u", 1:length(unique_rrr5))

multiverse_rrr <- create_multiverse(unique_rrr5, s1 = 0:2, s2 = 0:2, e1 = 0:1, e2 = 0:1, e3 = 0:4, e6 = 0:3) #rest of common == 0
N_multiverse <- nrow(multiverse_rrr)

#create the data multiverses

#NB! this function is currently about 10 times as slow as the RRR7 function, this appears to be mostly due to the psych::alpha function
#I might choose to write my own more specialized function and see if things improve [item-rest correlations, and pca]
#EDIT: by writing my own item-rest correlation function it became twice as fast (i.e, now only 5 times as slow as rrr7, but also runs more options)
#still somewhat slowed down by the psych::alpha for pca, but not sure how much that can be improved. E1 is also equally slow.



#**[5.1] Exit effect----

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR5", appendix = "exit",  apply_df_rrr = apply_df_rrr5, #parallel function
                    dv_input = exit_items) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#**[5.2] neglect effect----

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR5", appendix = "neglect",  apply_df_rrr = apply_df_rrr5, #parallel function
                    dv_input = neglect_items) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start


#********************************************************************************
##[6] RRR6 multiverse----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr6.csv", stringsAsFactors = FALSE)

rating_cartoon_items <- paste0("rating_cartoon", 1:4)

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- u2 <- u5 <- 0:1
u3 <- 0:3
u4 <- 0:2
unique_rrr6 <- list(u1, u2, u3, u4, u5) #unique must be input as named list in create_multiverse function
names(unique_rrr6) <- paste0("u", 1:length(unique_rrr6))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr6, s1 = 0:2, s2 = 0:2, e1 = 0:1, e2 = 0:1, e3 = 0:4, e4 = 1, e6 = 0:3) #rest of common == 0
multiverse_rrr <- multiverse_rrr[!(multiverse_rrr[,"s1"] == 2 & multiverse_rrr[, "s2"] == 2),] #these must be dropped because when < 5 items s1 == 2 leads to < 3 items for PCA. PCA requires 3+ items, rrr6 has 4 DV items..
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for loop below

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR6",  apply_df_rrr = apply_df_rrr6, dv_input = rating_cartoon_items) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start


#********************************************************************************
##[7] RRR7 multiverse----
#********************************************************************************

rrr <- read.csv("../data/03_cleaned-raw-data/rrr7.csv", stringsAsFactors = FALSE)

rrr_split <- split(rrr, rrr$lab) #to facilitate looping



#unique DF
u3 <- u6 <- u7 <- u8 <- u9 <- u10 <- 0:3
u1 <- u2 <- u4 <- u5 <- 0:1
unique_rrr7 <- list(u1, u2, u3, u4, u5, u6, u7, u8 ,u9, u10) #unique must be input as named list in create_multiverse function
names(unique_rrr7) <- paste0("u", 1:length(unique_rrr7))



multiverse_rrr <- create_multiverse(unique_rrr7, e2 = 0:1, e3 = 0:4, e6 = 0:3) #rest of common == 0




#EDIT 2019-10-17: should now be possible to apply all the common DF to RRR7
#still need to double-check so that the completed apply_common_DF function works as expected
#need to write the functions for unique exclusions (not necessary to have as a single function)
#then a summary function
#Still need to think about what happens when there are multiple e4 variables and account for that (later)

#RRR7 ran t-test using lm line 27


#1) loop over each row in the multiverse
#2) extract s1:e7 + uX values
#3) apply e1:e7 + u1:uX
#4) apply s1:s2 (must do all exclusions first)
#4.5) compute % excluded
#5) compute summary statistics
#6) save output + multiverse row input

#EDIT 2019-10-21: gave up on the lapply, because it seems to limit me to computing multiverse 1 for all labs, then 2 for all labs etc
#option would be to use a combination of lapply and mapply (like below the for loop), but the multiverse input must be given as a list
#and is then too large (several gb)
#EDIT: just tried running the below code with 5 multiverses, very very slow...
##output also needs to be double-checked, seems to repeat every two rows for some reason...
#EDIT 2019-10-22: seems to be coincidence based on how the expand.grid function works
#every second row adds the 1 to e2 but e2 does not exclude any data for L1 because sum(is.na(age)) = 0 and there are no e4 variables
#1000 instances took about 6.9 minutes -> 1e4 = 69 min, 1e6 6900 min, 2.6e6 ~ 17940 min ~ 299 hours ~ 12 days..
#running profvis on the function below (from 'efficient r-programming') it seems like my functions take barely any of the total time
#rather it is loading the huge dataframe that takes time. What if we loaded it in chunks? 2621440 -> 1e4
#indeed seems to be much faster. 1e4 now takes 60 seconds -> 1e6 = 100 min -> 2.6e6 = 260 min = 4.3 hours, which would be acceptable
#so: make sure code doesn't have to read full data.frame all the time + look into parallellization
#If I can get it down to just above one hour with paralellization I'm good
#Current setup still takes about 59sec for 1e4, so it is usable: now just look into parallellization
#might slow down if it has to read the full multiverse though?
#increasing the number of rows of the lab_multiverse to the full increases time to 1.5 min, so by about 30 seconds or 50%
#Adding the full multiverse really slowed down the function, now we're back to +6 min

#Apparently reading and assigning values in R is known to be slow
#simplest way to speed things up is by converting dataframes to matrices (if not possible, use data.table and .subset2)
#some options to speed things up https://stackoverflow.com/questions/48703802/most-efficient-way-fastest-to-modify-a-data-frame-using-indexing
#test1 -> changed output of apply_df_rrr7 to matrix, changed lab_multiverse to a matrix, changed multiverse_rrr7 to matrix
#NB, since dropped RRR and lab variables from apply_df_rrr7 output, also removed two columns in lab_multiverse (and dropped two names), and changed names to colnames
#for the rest, the same assignments work also for matrices

#results: even with the full variables running, the time taken is now 28.8 seconds for 1e4 rows, easily best so far!
#with this approach 1e6 ~ 2880 seconds = 48 min and 2.6e6 would then be about 125 min, so two hours for a full run
#running all of rrr7 would then take 20*2 so about 40 hours. Still a lot. But maybe this is where parallellization comes in?
#tried using the DoParallel package but couldn't get it to work, there are also only two cores on this computer..
#Below is my currently best approach, I am concerned I might run out of memory though, because the matrices are quite big
#Tried converting them into sparse matrices with Matrix, but these run much much slower
#According to profvis, what takes most of the time is still the by far the subsetting and replacing the row in lab_multiverse
#tried saving rresults by indexing as list as suggested in link earlier, but was extremely slow, because takes up all memory.
#I think this is as fast as I get without involving Rcpp

#just tried running it with 1e5 iterations -> took 3.26 minutes, which -> 2.6e6 will take ~ 85 min

N_multiverse <- nrow(multiverse_rrr)

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR7",  apply_df_rrr = apply_df_rrr7) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#********************************************************************************
##[8] RRR8 multiverse----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr8.csv", stringsAsFactors = FALSE)

triv_correct <- grep("triv", names(rrr), value = TRUE) #outcome variable, 30 binary items

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- 0:4
u2 <- u3 <- u4 <- 0:1
unique_rrr8 <- list(u1, u2, u3, u4) #unique must be input as named list in create_multiverse function
names(unique_rrr8) <- paste0("u", 1:length(unique_rrr8))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr8, s1 = 0:2, s2 = 0:2, e1 = 0:1,  e2 = 0:1, e3 = 0:4, e4 = 2, e6 = 0:3) #rest of common == 0
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for loop below

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR8",  apply_df_rrr = apply_df_rrr8, dv_input = triv_correct) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#********************************************************************************
##[9] RRR9 multiverse ----
#********************************************************************************
rrr <- read.csv("../data/03_cleaned-raw-data/rrr9.csv", stringsAsFactors = FALSE)

hostility <- grep("ron", names(rrr), value = TRUE) #DV1 scored 0-10
behavior <- grep("behavior", names(rrr), value = TRUE) #dv2 scored 0-10

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- u2 <- u3 <- u4 <-  0:1
unique_rrr9 <- list(u1, u2, u3, u4) #unique must be input as named list in create_multiverse function
names(unique_rrr9) <- paste0("u", 1:length(unique_rrr9))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr9, s1 = 0:2, s2 = 0:2, e1 = 0:1, e2 = 0:1, e3 = 0:4, e4 = 2, e6 = 0:3) #rest of common == 0
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for loop below

#**[9.1] Hostility----

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR9", appendix = "hostility", apply_df_rrr = apply_df_rrr9, dv_input = hostility) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#**[9.2] Behavior----

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR9", appendix = "behavior", apply_df_rrr = apply_df_rrr9, dv_input = behavior) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start

#********************************************************************************
##[10] RRR10 multiverse ----
#********************************************************************************
#library(dplyr)
rrr <- read.csv("../data/03_cleaned-raw-data/rrr10.csv", stringsAsFactors = FALSE)

rrr_split <- split(rrr, rrr$lab) #to facilitate looping


#unique DF (number of choices)
u1 <- u3 <- u5 <- 0:1
u2 <- u4 <- 0:2
unique_rrr10 <- list(u1, u2, u3, u4, u5) #unique must be input as named list in create_multiverse function
names(unique_rrr10) <- paste0("u", 1:length(unique_rrr10))

#create multiverse
multiverse_rrr <- create_multiverse(unique_rrr10, e2 = 0:1, e3 = 0:4, e4 = 2, e6 = 0:3) #rest of common == 0
N_multiverse <- nrow(multiverse_rrr) #needed as input in the for loop below

#create dataverse
#Parallel setup
start <- Sys.time()
ncores <- detectCores()
cl <- makePSOCKcluster(ncores) # Create cluster based on nworkers. This because mclappyl doesn't work on windows
clusterExport(cl, ls(.GlobalEnv)) #exported everything in environment to each node, otherwise they don't have access to all functions in the master global environment


#create the dataverse (parallelize over each lab)
parallel::parLapply(cl = cl,rrr_split, dataverser, RRR_name = "RRR10",  apply_df_rrr = apply_df_rrr10) #Note dataverser function is sourced and all following arguments are fed to the dataverser
stopCluster(cl) # Shut down the nodes
end <- Sys.time()
end - start
