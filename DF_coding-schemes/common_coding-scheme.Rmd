---
title: "Retrospective degrees of freedom"
author: "Anton Olsson Collentine"
bibliography: ../bibliography/Retrospective-DF.bib
nocite: |
  @10.1017/S0140525X0999152X, @10.1177/1948550617693063, @10.1177/2515245919838781, @10.1027/1614-2241/a000003, @10.1097/EDE.0b013e3182576cdb, @10.1007/s11136-015-1206-1, @10.1038/sj.bjc.6601907, @DOI:10.17026/dans-z5p-zedj, @10.1016/j.jesp.2009.03.009, @10.1016/j.jesp.2015.07.006, @tabachnick2007
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE)
```

### Degrees of freedom
Here are lists of degrees of freedom and corresponding choices we believe are common amongst many research projects in psychology _and relevant at the meta-analytic level_. To these will be added degrees of freedom that are unique to a particular project. The tables below constitute a coding scheme in order to maintain as much consistency as possible when coding DF across projects.

**Scale adjustments**

Scale length must be adjusted before exclusions based on missingness of DV is made.

Table 1. Scale adjustment. 
```{r scale-adjustment}
library(kableExtra)

scales <- data.frame(Variable = c(rep("S1. _Post hoc_ scale length", 3),
                                  rep("S2. Composite score", 3)),
                     Comment = c(rep("It is unclear how common it is to _post hoc_ drop items 'that don't work' from a scale, but dropping more than a few seems unlikely. In the research we are looking at (experimental) there are rarely long scales. Excluding 1-item scales, the average scale length in JPSPS in 2014 was 6.87 (SD = 7.18) (Flake et al., 2017)",3),
                                    rep("For Likert-type scales with multiple items. Other DVs e.g., reaction time variability (RRR3), dichotomous correct/incorrect (RRR1&2), continuous measures (RRR7), single item DVs (RRR10) may need more unique choice options. For PCA", 3)),
                     Choices = c("a) No adjustment",
                                "b) Drop the item with the lowest item-rest correlation",
                                "c) Drop the two items with the lowest item-rest correlations",
                                "a) Unweighted average score",
                                "b) Sum score",
                                "c) PCA: Varimax rotation, force two components and pick the first, requires at least 3 items.")
                     )


knitr::kable(scales, booktabs = TRUE) %>% 
  kable_styling(latex_options = "scale_down") %>%
  column_spec(2, width = "25em") %>%
  collapse_rows(1:2, valign = "top")
```


**Exclusions**

In about a quarter of psychology research one or more participants are excluded from analysis [@molenaar2005]. About 40% of researchers admit to ever excluding data after looking at their results [@10.1177/0956797611430953]. Even so, it appears common to not report data exclusions in psychological research [@10.1037/met0000014]. 

Table 2.
```{r common-vars-table}

common <- data.frame(Variable = c(rep("E1. Missingness DV", 2),
                                  rep("E2. Missingness E3-E4 variables", 2),
                                  rep("E3. Age", 5),
                                  rep("E4. Language / Student / Ethnicity", 2),
                                  rep("E5. Attention check", 3),
                                  rep("E6. Univariate outliers", 4),
                                  rep("E7. Multivariate outliers", 2)),
                     
                     Comment = c(rep("List-wise deletion appears to be by far the most common approach to missing data. In van Ginkel et al.'s (2010) review of personality psychology 97% used list-wise deletion for missing data and several reviews in medicine have also found it to be an extremely common method (Eekhout et al., 2012; Rombach et al., 2016; Burton & Altman, 2004). Nonetheless, we see that for example RRR6 used pair-wise deletion (option b) which may seem reasonable to some researchers, in particular with a longer scale.", 2),
                                rep("RRRs that excluded data based on a E3-E4 variable (e.g., age) did list-wise deletion when data was missing. For other non-DV variables we do no exclusions based on missingness, unless this was explicitly done by the project (e.g., 'task completion' RRR10).", 2),
                                rep("Used by 9/10 RRRs for exclusions. Across 25 cohorts of Dutch bachelor psychology students about 95% (96.7%) of students were below 25, 90% (92.7%) below 24, 85% (86.6%) below 23 and 80% (77.8%) below 22. The oldest students in this dataset were 25. Bachelor psychology students are common samples for psychological research (e.g., Heinrich et al., 2010), but may be becoming less common with an increase in internet samples (Sassenberg & Ditrich, 2019)", 5),
                                rep("Used by 3/10 RRRs. Demographic variables which are sometimes used for exclusions. Language includes variables such as 'native speaker' which may have a yes/no response. Ethnicity includes similar variables such as 'country of birth' or 'race'. If multiple of these demographic variables are available they are treated as separate exclusion criteria. ", 2),
                                rep("Attention checks are common in psychology, as evidenced by the more than 1500 citations of (Oppenheimer et al., 2009) who introduced 'instructional manipulation checks'. (Curran et al., 2016) suggest 'conservative' exclusions based on 50% failed attention checks when multiple items are used. This category does not include manipulation checks which are more varying", 3),
                                rep("Used by 1/10 RRRs. Commonly recommended cutoffs (Bakker & Wicherts, 2014). Test for outliers across groups.", 4),
                                rep("If the outcome variable is a correlation. Tabachnik, Fidell and Ullman (2007) recommend using Mahalanobi's distance with a cutoff of p < .001 for detecting multivariate outliers. Outliers tested within groups as recommended by Tabachnik et al.", 2)),
                     Choices = c("a) Any missing items -> list-wise deletion",
                                 "b) If <=25% items missing then pair-wise deletion of missing items. Otherwise list-wise.",
                                 "a) No exclusion",
                                 "b) Missing on any variable -> list-wise deletion",
                                 "a) No exclusion",
                                 "b) Not 18-24",
                                 "c) Not 18-23",
                                 "d) Not 18-22",
                                 "e) Not 18-21",
                                 "a) No exclusion",
                                 "b) Exclude participants not belonging to the dominant category",
                                 "a) No exclusion",
                                 "b) Exclude if failed >50% of attention check items [i.e., with two items, must fail both, e.g., RRR7]",
                                 "c) Exclude if failed any attention check item",
                                 "a) No exclusion",
                                 "b) DV score > 2 SD from mean",
                                 "c) > 3 SD from mean",
                                 "d) > 1.5 times the interquartile range",
                                 "a) No exclusion",
                                 "b) Mahalanobi's distance with p < .001")
)



knitr::kable(common, booktabs = TRUE) %>% 
  kable_styling(latex_options = "scale_down") %>% 
  column_spec(2, width = "25em") %>% #This leads to problems together with collapse_rows for pdf
  collapse_rows(columns =  1:2, valign = "top") 


```




# References


